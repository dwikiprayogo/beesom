package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.Reg;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.List;

/**
 * Created by basofi on 13/07/16.
 */
public class RegDao implements DaoCrud<Reg, String> {

    private static RegDao singleton;

    public static RegDao getInstance(){
        if(singleton == null){
            singleton = new RegDao();
        }
        return singleton;
    }

    @Override
    public void create(Reg model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Reg model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Reg model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Reg model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<Reg> read() throws Exception {
        return getDao().queryForAll();
    }

    @Override
    public Dao<Reg, String> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Reg.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Reg.class);
    }
}
