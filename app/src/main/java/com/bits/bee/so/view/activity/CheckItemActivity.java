package com.bits.bee.so.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.ItemDao;
import com.bits.bee.so.bl.db.dao.PriceDao;
import com.bits.bee.so.bl.db.dao.StockDao;
import com.bits.bee.so.bl.db.model.Item;
import com.bits.bee.so.bl.db.model.Price;
import com.bits.bee.so.bl.db.model.Stock;
import com.bits.bee.so.util.BUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckItemActivity extends AppCompatActivity {

    @BindView(R.id.checkitem_etSearch)
    EditText mEtSearch;
    @BindView(R.id.checkitem_rvItem)
    RecyclerView mRvItem;
    @BindView(R.id.checkitem_toolbar)
    Toolbar mToolbar;

    private RvItemAdapter mRvItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkitem);
        ButterKnife.bind(this);

        initViews();
        initListeners();
    }

    private void initViews(){
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        mEtSearch.setHint("Fitur masih belum bisa digunakan");
//        mEtSearch.setEnabled(false);

        mRvItem.setLayoutManager(new LinearLayoutManager(this));
        mRvItem.setHasFixedSize(false);

        mRvItemAdapter = new RvItemAdapter();
        mRvItem.setAdapter(mRvItemAdapter);

        try {
            mRvItemAdapter.generate(ItemDao.getInstance().read());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initListeners(){
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                mRvItemAdapter.filter(str);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class RvItemAdapter extends RecyclerView.Adapter<RvItemAdapter.ViewHolder> {
        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.checkitem_rvitem_tvItem)
            TextView tvItem;

            @BindView(R.id.checkitem_rvitem_tvPrice)
            TextView tvPrice;

            @BindView(R.id.checkitem_rvitem_tvStock)
            TextView tvStock;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        private List<Item> mItemList = new ArrayList<>();
        private List<Item> mItemListCopy = new ArrayList<>();

        public void generate(List<Item> itemList) {
            this.mItemList = itemList;
            this.mItemListCopy.addAll(mItemList);
            notifyDataSetChanged();
        }

        public void filter(String text){
            if(text.isEmpty()){
                mItemList.clear();
                mItemList.addAll(mItemListCopy);
            }else{
                List<Item> filterResult = new ArrayList<>();
                text = text.toLowerCase();
                for(Item item : mItemListCopy){
                    if(item.getName().toLowerCase().contains(text)){
                        filterResult.add(item);
                    }
                }
                mItemList.clear();
                mItemList.addAll(filterResult);
            }
            notifyDataSetChanged();
        }

        @Override
        public RvItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(CheckItemActivity.this).inflate(
                    R.layout.row_checkitem_rvitem, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RvItemAdapter.ViewHolder holder, int position) {
            Item item = mItemList.get(position);
            Price price = null;
            try {
                price = PriceDao.getInstance().readByItem(item);
                holder.tvPrice.setText(BUtil.addCurr(price.getPrice1().toPlainString()));
            } catch (Exception e) {
                holder.tvPrice.setText("Data not available");
                e.printStackTrace();
            }

            Stock stock = null;
            try {
                stock = StockDao.getInstance().readByItemId(item);
                holder.tvStock.setText(stock.getQtySo().toPlainString());
            } catch (Exception e) {
                holder.tvStock.setText("Data not available");
                e.printStackTrace();
            }

            holder.tvItem.setText(item.getName());
        }

        @Override
        public int getItemCount() {
            return mItemList.size();
        }
    }
}