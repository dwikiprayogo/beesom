package com.bits.bee.so.bl.db.model;

import com.bits.bee.so.bl.db.dao.ItemDao;
import com.bits.bee.so.bl.net.model.StockGet;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by basofi on 25/05/16.
 */
@DatabaseTable(tableName = Stock.TBL_NAME)
public class Stock implements Serializable {
    public static final String TBL_NAME = "stock";

    public static final String ID = "id";
    public static final String ITEM_ID = "item_id";
    public static final String WH_ID = "wh_id";
    public static final String PID = "pid";
    public static final String ITEMNAME = "itemname";
    public static final String QTYONHAND = "qtyonhand";
    public static final String QTYPO = "qtypo";
    public static final String QTYSO = "qtyso";
    public static final String QTYAVAILABLE = "qtyavailable";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = ITEM_ID, foreign = true, foreignAutoRefresh = true)
    private Item itemId;
    @DatabaseField(columnName = WH_ID)
    private String whId;
    @DatabaseField(columnName = PID)
    private String pid;
    @DatabaseField(columnName = ITEMNAME)
    private String itemName;
    @DatabaseField(columnName = QTYONHAND)
    private BigDecimal qtyOnHand = new BigDecimal(0);
    @DatabaseField(columnName = QTYPO)
    private BigDecimal qtyPo = new BigDecimal(0);
    @DatabaseField(columnName = QTYSO)
    private BigDecimal qtySo = new BigDecimal(0);
    @DatabaseField(columnName = QTYAVAILABLE)
    private BigDecimal qtyAvailable = new BigDecimal(0);

    public Stock(){

    }

    public Stock(StockGet.Datum stockGetDatum){
        try {
            Item item = ItemDao.getInstance().readByOlId(stockGetDatum.getItemId());
            setItemId(item);
            setItemName(item.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        setWhId(stockGetDatum.getWhId());
        setPid(stockGetDatum.getPid());
        setQtyAvailable(new BigDecimal(stockGetDatum.getQty()));
        setQtyOnHand(new BigDecimal(stockGetDatum.getQty()));
        setQtyPo(new BigDecimal(stockGetDatum.getQtypo()));
        setQtySo(new BigDecimal(stockGetDatum.getQtyso()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Item getItemId() {
        return itemId;
    }

    public void setItemId(Item itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getQtyOnHand() {
        return qtyOnHand;
    }

    public void setQtyOnHand(BigDecimal qtyOnHand) {
        this.qtyOnHand = qtyOnHand;
    }

    public BigDecimal getQtyPo() {
        return qtyPo;
    }

    public void setQtyPo(BigDecimal qtyPo) {
        this.qtyPo = qtyPo;
    }

    public BigDecimal getQtySo() {
        return qtySo;
    }

    public void setQtySo(BigDecimal qtySo) {
        this.qtySo = qtySo;
    }

    public BigDecimal getQtyAvailable() {
        return qtyAvailable;
    }

    public void setQtyAvailable(BigDecimal qtyAvailable) {
        this.qtyAvailable = qtyAvailable;
    }

    public String getWhId() {
        return whId;
    }

    public void setWhId(String whId) {
        this.whId = whId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}