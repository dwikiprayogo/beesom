package com.bits.bee.so.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.view.abstraction.WidgetAbstract;

/**
 * Created by basofi on 15/07/16.
 */
public class WidgetTargetCheckIn extends WidgetAbstract {

    private View mTargetCheckIn;
    private TextView mTvTargetCheckIn;

    public WidgetTargetCheckIn(Context context) {
        super(context);
        initViews();
    }

    public WidgetTargetCheckIn(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    private void initViews(){
        mTargetCheckIn = LayoutInflater.from(getContext())
                .inflate(R.layout.widget_targetcheckin, this, false);
        mTvTargetCheckIn = (TextView) mTargetCheckIn.findViewById(R.id.targetcheckin_tvTargetCheckIn);

        setContent(mTargetCheckIn);
    }

    @Override
    public void render() {
        loadData();
    }

    @Override
    protected void loadData() {
        mTvTargetCheckIn.setText("0");
    }
}
