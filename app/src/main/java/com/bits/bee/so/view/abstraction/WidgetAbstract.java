package com.bits.bee.so.view.abstraction;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.bits.bee.so.R;

/**
 * Created by basofi on 15/07/16.
 */
public abstract class WidgetAbstract extends CardView{

    public static final int TYPE_TOTAL_PENJUALAN = 0;
    public static final int TYPE_ORDERSALES_HARINI = 1;
    public static final int TYPE_TARGET_CHECKIN = 2;
    public static final int TYPE_CHECKIN_HARINI = 3;
    public static final int TYPE_CHECKIN_SELANJUTNYA = 4;
    public static final int TYPE_CUSTOMER_BARU = 5;

    private int mType = TYPE_TOTAL_PENJUALAN;

    private LinearLayout mLlContent;

    public WidgetAbstract(Context context) {
        super(context);
        initLayout();
    }

    public WidgetAbstract(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout();
    }

    public abstract void render();

    protected abstract void loadData();

    public int getType(){
        return mType;
    }

    private void initLayout(){
        CardView.LayoutParams widgetAbstractParams =
                new CardView.LayoutParams(
                        LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT);
//        widgetAbstractParams.gravity = Gravity.CENTER_HORIZONTAL;
        setLayoutParams(widgetAbstractParams);

        mLlContent = new LinearLayout(getContext());

        LinearLayout.LayoutParams llContentParams =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
//        llContentParams.gravity = Gravity.CENTER_HORIZONTAL;

        mLlContent.setOrientation(LinearLayout.VERTICAL);
        mLlContent.setLayoutParams(llContentParams);

        addView(mLlContent);

        setForeground(getSelectedItemDrawable());
        setClickable(true);
        setCardElevation(10);
    }

    public Drawable getSelectedItemDrawable() {
        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray ta = getContext().obtainStyledAttributes(attrs);
        Drawable selectedItemDrawable = ta.getDrawable(0);
        ta.recycle();
        return selectedItemDrawable;
    }

    public void setContent(View view){
        mLlContent.addView(view);
    }
}
