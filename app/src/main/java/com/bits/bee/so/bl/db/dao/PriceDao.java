package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.Item;
import com.bits.bee.so.bl.db.model.Price;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 27/05/16.
 */
public class PriceDao implements DaoCrud<Price, Integer> {
    private static PriceDao singleton;

    public static PriceDao getInstance() {
        if (singleton == null) {
            singleton = new PriceDao();
        }
        return singleton;
    }

    @Override
    public void create(Price model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Price model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Price model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Price model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<Price> read() throws Exception {
        return getDao().queryForAll();
    }

    public Price readByItem(Item item) throws Exception{
        List<Price> itemList = new ArrayList<>();
        itemList = getDao().queryForEq(Price.ITEM_ID, item);
        if(itemList.size() > 0){
            return itemList.get(0);
        }else {
            throw new Exception("No price found");
        }
    }

    @Override
    public Dao<Price, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Price.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Price.class);
    }
}
