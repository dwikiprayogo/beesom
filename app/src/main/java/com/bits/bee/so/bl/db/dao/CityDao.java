package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.City;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 30/06/16.
 */
public class CityDao implements DaoCrud<City, Integer> {

    private static CityDao singleton;

    public static CityDao getInstance(){
        if(singleton == null){
            singleton = new CityDao();
        }
        return singleton;
    }

    @Override
    public void create(City model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(City model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(City model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(City model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<City> read() throws Exception {
        return getDao().queryForAll();
    }

    public City readByCode(String code) throws Exception{
        List<City> cityList = new ArrayList<>();
        cityList = getDao().queryForEq(City.CODE, code);
        if(cityList.size() > 0){
            return cityList.get(0);
        }else {
            throw new Exception("Code not found");
        }
    }

    @Override
    public Dao<City, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(City.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, City.class);
    }
}
