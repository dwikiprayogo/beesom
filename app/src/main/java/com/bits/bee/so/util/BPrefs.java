package com.bits.bee.so.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by basofi on 30/07/16.
 */
public class BPrefs {

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mSharedPreferencesEditor;

    private static BPrefs singleton;
    private Context mContext;

    public static synchronized BPrefs getInstance(){
        if(singleton == null){
            singleton = new BPrefs();
        }
        return singleton;
    }

    public void init(Context context){
        mContext = context;
        if(mSharedPreferences == null){
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            mSharedPreferencesEditor = mSharedPreferences.edit();
        }
    }

    private boolean saveToPreferences(String key, Object value){

        if(value instanceof Boolean){
            mSharedPreferencesEditor.putBoolean(key, (Boolean)value);
        }else if(value instanceof String){
            mSharedPreferencesEditor.putString(key, (String)value);
        }else if(value instanceof Integer){
            mSharedPreferencesEditor.putInt(key, (Integer)value);
        }else if(value instanceof Float){
            mSharedPreferencesEditor.putFloat(key, (Float)value);
        }else if(value instanceof Long){
            mSharedPreferencesEditor.putLong(key, (Long)value);
        }else{
            return false;
        }

        mSharedPreferencesEditor.commit();
        return true;
    }

    private Object getDataFromPreferences(String key, Object defValue){

        if(defValue instanceof Boolean){
            return mSharedPreferences.getBoolean(key, (Boolean)defValue);
        }else if(defValue instanceof String){
            return mSharedPreferences.getString(key, (String)defValue);
        }else if(defValue instanceof Integer){
            return mSharedPreferences.getInt(key, (Integer)defValue);
        }else if(defValue instanceof Float){
            return mSharedPreferences.getFloat(key, (Float)defValue);
        }else if(defValue instanceof Long){
            return mSharedPreferences.getLong(key, (Long)defValue);
        }else{
            return null;
        }
    }

    public boolean setLastDownloadTime(String downloadTime){
        return saveToPreferences("dltime", downloadTime);
    }

    public String getLastDownloadTime(){
        return (String) getDataFromPreferences("dltime", "");
    }

    public boolean setLastUploadTime(String uploadTime){
        return saveToPreferences("ultime", uploadTime);
    }

    public String getLastUploadTime(){
        return (String) getDataFromPreferences("ultime", "");
    }
}