package com.bits.bee.so.view.activity;

import android.app.ProgressDialog;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.model.Sod;
import com.bits.bee.so.bl.db.model.Unit;
import com.bits.bee.so.bl.trans.SaleSoTrans;
import com.bits.bee.so.util.BUtil;
import com.bits.bee.so.view.dialog.BDialog;
import com.bits.bee.so.view.dialog.NewSoItemAddItemDialog;
import com.bits.bee.so.view.fragment.SalesSoFragment;
import com.bits.bee.so.view.listener.NewSoItemAddItemDialogCallback;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.solodovnikov.rxlocationmanager.LocationRequestBuilder;
import ru.solodovnikov.rxlocationmanager.LocationTime;
import rx.Subscriber;

public class NewSoItemActivity extends AppCompatActivity implements NewSoItemAddItemDialogCallback,
        AppBarLayout.OnOffsetChangedListener{

    @BindView(R.id.newsoitem_appbar)
    AppBarLayout mAppBar;
    @BindView(R.id.newsoitem_ctlToolbar)
    CollapsingToolbarLayout mCtlToolbar;
    @BindView(R.id.newsoitem_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.newsoitem_tvNamaPerusahaan)
    TextView mTvNamaPerusahaan;
    @BindView(R.id.newsoitem_tvTermin)
    TextView mTvTermin;
    @BindView(R.id.newsoitem_tvTgl)
    TextView mTvTgl;
    @BindView(R.id.newsoitem_tvItemTotal)
    TextView mTvItemTotal;
    @BindView(R.id.newsoitem_tvTaxTotal)
    TextView mTvTaxTotal;
    @BindView(R.id.newsoitem_tvOrderTotal)
    TextView mTvOrderTotal;

    @BindView(R.id.newsoitem_rvItemSalesOrder)
    RecyclerView mRvItemSalesOrder;
    @BindView(R.id.newsoitem_fabNewItem)
    FloatingActionButton mFabNewItem;

    private SaleSoTrans mSaleSoTrans;

    private NewSoItemAddItemDialog mNewSoItemAddItemDialog;

    private RvItemSalesOrderAdapter mRvItemSalesOrderAdapter;

    public static final int RESULTCODE_FINISH = 0;

    private double mLat = 0;
    private double mLong = 0;

    private ProgressDialog mPd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsoitem);
        ButterKnife.bind(this);

        initViews();
        initListeners();
        loadData();
    }


    private void initViews() {
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.setTitle("");

        mPd = new ProgressDialog(this);
        mPd.setMessage("Menyimpan Data...");
        mPd.setCancelable(false);
    }

    private void initListeners(){
        mAppBar.addOnOffsetChangedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_newsoitem, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_newsoitem_save:
                save();
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void save() {
        if(BUtil.gpsCheck(this)) {
            mPd.show();

            new LocationRequestBuilder(this)
                    .addRequestLocation(LocationManager.NETWORK_PROVIDER, new LocationTime(10, TimeUnit.SECONDS))
                    .addRequestLocation(LocationManager.GPS_PROVIDER, new LocationTime(10, TimeUnit.SECONDS))
                    .setDefaultLocation(new Location(LocationManager.PASSIVE_PROVIDER))
                    .create().subscribe(new Subscriber<Location>() {
                @Override
                public void onCompleted() {
                    mPd.dismiss();
                    if (mRvItemSalesOrderAdapter.getItemCount() != 0) {
                        mSaleSoTrans.save(BUtil.genCoordinate(mLat, mLong));
                        BDialog.showInfoDialog(NewSoItemActivity.this, "Data Saved", "Data has been successfully saved",
                                new MaterialDialog.ButtonCallback() {
                                    @Override
                                    public void onAny(MaterialDialog dialog) {
                                        super.onAny(dialog);
                                        setResult(RESULTCODE_FINISH);
                                        finish();
                                    }
                                });
                    } else {
                        BDialog.showInfoDialog(NewSoItemActivity.this,
                                "Tidak Bisa Simpan Data", "Item masih kosong");
                    }
                }

                @Override
                public void onError(Throwable e) {
                    mPd.dismiss();
                    BDialog.showErrorDialog(NewSoItemActivity.this, e.getMessage());
                }

                @Override
                public void onNext(Location location) {
                    mLat = location.getLatitude();
                    mLong = location.getLongitude();
                }
            });
        }else{
            BUtil.openGpsSetting(this);
        }
    }

    private void loadData() {
        Bundle bundle = getIntent().getExtras();
        mSaleSoTrans = (SaleSoTrans) bundle.getSerializable(SalesSoFragment.BUNDLEKEY_SOTRANS);

        mRvItemSalesOrder.setHasFixedSize(true);
        mRvItemSalesOrder.setLayoutManager(new LinearLayoutManager(this));

        mTvNamaPerusahaan.setText(mSaleSoTrans.getBp().getNamaPerusahaan());
        mTvTermin.setText(mSaleSoTrans.getTermType());
        mTvTaxTotal.setText(BUtil.addCurr(mSaleSoTrans.getSale().getTaxamt().toPlainString()));
        mTvItemTotal.setText(BUtil.addCurr(mSaleSoTrans.getSale().getSubtotal().toPlainString()));
        mTvOrderTotal.setText(BUtil.addCurr(mSaleSoTrans.getSale().getTotal().toPlainString()));
        mTvTgl.setText(BUtil.getCurrentDate());

        mRvItemSalesOrderAdapter = new RvItemSalesOrderAdapter();
        mRvItemSalesOrder.setAdapter(mRvItemSalesOrderAdapter);
    }

    private void initAddEditItemDialog() {
        mNewSoItemAddItemDialog = new NewSoItemAddItemDialog(this);
        mNewSoItemAddItemDialog.setNewSoItemAddItemDialogCallback(this);
    }

    private void showAddItemDialog() {
        initAddEditItemDialog();
        mNewSoItemAddItemDialog.show();
    }

    private void showEditItemDialog(Sod sod) {
        initAddEditItemDialog();
        mNewSoItemAddItemDialog.show(sod);
    }

    @OnClick(R.id.newsoitem_fabNewItem)
    void newItem() {
        showAddItemDialog();
    }

    @Override
    public void onNewSodSet(Sod sod) {
        mSaleSoTrans.putSodToSo(sod);
        mRvItemSalesOrderAdapter.generate(mSaleSoTrans.getSodList());

        mTvItemTotal.setText(BUtil.addCurr(mSaleSoTrans.getSale().getSubtotal().toPlainString()));
        mTvOrderTotal.setText(BUtil.addCurr(mSaleSoTrans.getSale().getTotal().toPlainString()));
    }

    @Override
    public void onSodDelete(Sod sod) {
        mSaleSoTrans.deleteSod(sod);
        mRvItemSalesOrderAdapter.generate(mSaleSoTrans.getSodList());

        mTvItemTotal.setText(BUtil.addCurr(mSaleSoTrans.getSale().getSubtotal().toPlainString()));
        mTvOrderTotal.setText(BUtil.addCurr(mSaleSoTrans.getSale().getTotal().toPlainString()));
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if(verticalOffset < 0){
            mToolbar.setTitle(BUtil.addCurr(mSaleSoTrans.getSale().getTotal().toPlainString()));
        }else{
            mToolbar.setTitle("");
        }
    }

    class RvItemSalesOrderAdapter
            extends RecyclerView.Adapter<RvItemSalesOrderAdapter.ViewHolder> {

        private List<Sod> mSodList = Collections.emptyList();
        private LinkedHashMap<Integer, Sod> mSelectedSodHash = new LinkedHashMap<>();
//        private List<Integer> mIndexSelectedList = new ArrayList<>();

        class ViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.newsoitem_rvitemsalesorder_tvItemName)
            TextView mTvItemName;
            @BindView(R.id.newsoitem_rvitemsalesorder_tvQtyUnit)
            TextView mTvQtyUnit;
            @BindView(R.id.newsoitem_rvitemsalesorder_tvPrice)
            TextView mTvPrice;
            @BindView(R.id.newsoitem_rvitemsalesorder_cv)
            CardView mCv;
            @BindView(R.id.newsoitem_rvitemsalesorder_btnDelete)
            Button mBtnDelete;
            @BindView(R.id.newsoitem_rvitemsalesorder_btnEdit)
            Button mBtnEdit;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        public void generate(List<Sod> listSod) {
            this.mSodList = listSod;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_newsoitem_rvitemsalesorder, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final Sod sod = mSodList.get(position);
            Unit unit = sod.getUnit();

            String price = sod.getSubtotal().toPlainString();
            String qtyUnit = String.format("%s %s", sod.getQty().toPlainString(), unit.getUnit());

            holder.mTvItemName.setText(sod.getItemname());
            holder.mTvQtyUnit.setText(qtyUnit);
            holder.mTvPrice.setText(BUtil.addCurr(price));

            holder.mBtnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BDialog.showChoiceDialog(NewSoItemActivity.this,
                            "Hapus Item",
                            "Yakin hapus item?",
                            new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    onSodDelete(sod);
                                }
                            });
                }
            });

            holder.mBtnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEditItemDialog(sod);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mSodList.size();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!BUtil.gpsCheck(this)) {
            BUtil.openGpsSetting(this);
        }
    }
}