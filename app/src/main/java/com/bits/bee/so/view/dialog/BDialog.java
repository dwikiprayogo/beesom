package com.bits.bee.so.view.dialog;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bits.bee.so.R;

/**
 * Created by basofi on 17/06/16.
 */
public class BDialog {


    private static MaterialDialog.Builder buildStandartDialog(Context context){
        return new MaterialDialog.Builder(context)
                .positiveColorRes(R.color.secondary_text)
                .negativeColorRes(R.color.secondary_text)
                .titleColorRes(R.color.secondary_text)
                .neutralColorRes(R.color.secondary_text);
    }

    public static void showCustomChoiceDialog(Context context,
                                              String title,
                                              String msg,
                                              String positiveButton,
                                              String neutralButton,
                                              MaterialDialog.ButtonCallback callback){

        buildStandartDialog(context)
                .title(title)
                .content(msg)
                .positiveText(positiveButton)
                .neutralText(neutralButton)
                .callback(callback)
                .show();
    }

    public static void showChoiceDialog(Context context, String title, String msg,
                                        MaterialDialog.ButtonCallback callback){
        buildStandartDialog(context)
                .title(title)
                .content(msg)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .callback(callback)
                .show();
    }

    public static void showChoiceDialog(Context context, int titleRes, int msgRes,
                                        MaterialDialog.ButtonCallback callback){
        String title = context.getResources().getString(titleRes);
        String msg = context.getResources().getString(msgRes);

        showChoiceDialog(context, title, msg, callback);
    }

    public static void showInfoDialog(Context context, String title, String msg){
        buildStandartDialog(context)
                .title(title)
                .content(msg)
                .iconRes(R.drawable.ic_information)
                .positiveText(context.getResources().getString(R.string.ok))
                .show();
    }

    public static void showInfoDialog(Context context, String title, String msg,
                                      MaterialDialog.ButtonCallback callback){
        buildStandartDialog(context)
                .title(title)
                .content(msg)
                .iconRes(R.drawable.ic_information)
                .positiveText(context.getResources().getString(R.string.ok))
                .callback(callback)
                .show();
    }

    public static void showErrorDialog(Context context, int msgId){
        String msg = context.getString(msgId);
        showErrorDialog(context, msg);
    }

    public static void showErrorDialog(Context context, String msg){
        buildStandartDialog(context)
                .title(context.getString(R.string.error))
                .content(msg)
                .iconRes(R.drawable.ic_close)
                .positiveText(context.getResources().getString(R.string.ok))
                .show();
    }

    public static void showErrorDialog(Context context, int msgId,
                                       MaterialDialog.ButtonCallback callback){
        String msg = context.getString(msgId);
        showErrorDialog(context, msgId, callback);
    }

    public static void showErrorDialog(Context context, String msg,
                                       MaterialDialog.ButtonCallback callback){
        buildStandartDialog(context)
                .title(context.getString(R.string.error))
                .content(msg)
                .positiveText(context.getResources().getString(R.string.ok))
                .callback(callback)
                .show();
    }
}
