package com.bits.bee.so.bl.net.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 25/05/16.
 */
public class ItemGet {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("name1")
        @Expose
        private String name1;
        @SerializedName("name2")
        @Expose
        private String name2;
        @SerializedName("barcode")
        @Expose
        private String barcode;
        @SerializedName("version")
        @Expose
        private String version;
        @SerializedName("brand_id")
        @Expose
        private Object brandId;
        @SerializedName("itemtype_code")
        @Expose
        private String itemtypeCode;
        @SerializedName("usepid")
        @Expose
        private Boolean usepid;
        @SerializedName("uniquepid")
        @Expose
        private Boolean uniquepid;
        @SerializedName("itemgrp1_id")
        @Expose
        private Object itemgrp1Id;
        @SerializedName("itemgrp2_id")
        @Expose
        private Object itemgrp2Id;
        @SerializedName("itemgrp3_id")
        @Expose
        private Object itemgrp3Id;
        @SerializedName("accinv")
        @Expose
        private String accinv;
        @SerializedName("acccost")
        @Expose
        private String acccost;
        @SerializedName("accsale")
        @Expose
        private String accsale;
        @SerializedName("accsret")
        @Expose
        private String accsret;
        @SerializedName("ispurc")
        @Expose
        private Boolean ispurc;
        @SerializedName("isstock")
        @Expose
        private Boolean isstock;
        @SerializedName("ismfg")
        @Expose
        private Boolean ismfg;
        @SerializedName("issale")
        @Expose
        private Boolean issale;
        @SerializedName("unitdesc")
        @Expose
        private String unitdesc;
        @SerializedName("purcqtymin")
        @Expose
        private String purcqtymin;
        @SerializedName("leadtime")
        @Expose
        private String leadtime;
        @SerializedName("qtymin")
        @Expose
        private String qtymin;
        @SerializedName("qtymax")
        @Expose
        private String qtymax;
        @SerializedName("qtyreorder")
        @Expose
        private String qtyreorder;
        @SerializedName("qtypo")
        @Expose
        private String qtypo;
        @SerializedName("qtyso")
        @Expose
        private String qtyso;
        @SerializedName("qtywip")
        @Expose
        private String qtywip;
        @SerializedName("note")
        @Expose
        private String note;
        @SerializedName("active")
        @Expose
        private Boolean active;
        @SerializedName("pousepid")
        @Expose
        private Boolean pousepid;
        @SerializedName("sousepid")
        @Expose
        private Boolean sousepid;
        @SerializedName("vendor_id")
        @Expose
        private Object vendorId;
        @SerializedName("vitemcode")
        @Expose
        private String vitemcode;
        @SerializedName("purcunit")
        @Expose
        private String purcunit;
        @SerializedName("saleunit")
        @Expose
        private String saleunit;
        @SerializedName("stockunit")
        @Expose
        private String stockunit;
        @SerializedName("created_at")
        @Expose
        private Object createdAt;
        @SerializedName("created_by")
        @Expose
        private Object createdBy;
        @SerializedName("updated_at")
        @Expose
        private Object updatedAt;
        @SerializedName("updated_by")
        @Expose
        private Object updatedBy;
        @SerializedName("costtype")
        @Expose
        private String costtype;
        @SerializedName("isimport")
        @Expose
        private Boolean isimport;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName1() {
            return name1;
        }

        public void setName1(String name1) {
            this.name1 = name1;
        }

        public String getName2() {
            return name2;
        }

        public void setName2(String name2) {
            this.name2 = name2;
        }

        public String getBarcode() {
            return barcode;
        }

        public void setBarcode(String barcode) {
            this.barcode = barcode;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public Object getBrandId() {
            return brandId;
        }

        public void setBrandId(Object brandId) {
            this.brandId = brandId;
        }

        public String getItemtypeCode() {
            return itemtypeCode;
        }

        public void setItemtypeCode(String itemtypeCode) {
            this.itemtypeCode = itemtypeCode;
        }

        public Boolean getUsepid() {
            return usepid;
        }

        public void setUsepid(Boolean usepid) {
            this.usepid = usepid;
        }

        public Boolean getUniquepid() {
            return uniquepid;
        }

        public void setUniquepid(Boolean uniquepid) {
            this.uniquepid = uniquepid;
        }

        public Object getItemgrp1Id() {
            return itemgrp1Id;
        }

        public void setItemgrp1Id(Object itemgrp1Id) {
            this.itemgrp1Id = itemgrp1Id;
        }

        public Object getItemgrp2Id() {
            return itemgrp2Id;
        }

        public void setItemgrp2Id(Object itemgrp2Id) {
            this.itemgrp2Id = itemgrp2Id;
        }

        public Object getItemgrp3Id() {
            return itemgrp3Id;
        }

        public void setItemgrp3Id(Object itemgrp3Id) {
            this.itemgrp3Id = itemgrp3Id;
        }

        public String getAccinv() {
            return accinv;
        }

        public void setAccinv(String accinv) {
            this.accinv = accinv;
        }

        public String getAcccost() {
            return acccost;
        }

        public void setAcccost(String acccost) {
            this.acccost = acccost;
        }

        public String getAccsale() {
            return accsale;
        }

        public void setAccsale(String accsale) {
            this.accsale = accsale;
        }

        public String getAccsret() {
            return accsret;
        }

        public void setAccsret(String accsret) {
            this.accsret = accsret;
        }

        public Boolean getIspurc() {
            return ispurc;
        }

        public void setIspurc(Boolean ispurc) {
            this.ispurc = ispurc;
        }

        public Boolean getIsstock() {
            return isstock;
        }

        public void setIsstock(Boolean isstock) {
            this.isstock = isstock;
        }

        public Boolean getIsmfg() {
            return ismfg;
        }

        public void setIsmfg(Boolean ismfg) {
            this.ismfg = ismfg;
        }

        public Boolean getIssale() {
            return issale;
        }

        public void setIssale(Boolean issale) {
            this.issale = issale;
        }

        public String getUnitdesc() {
            return unitdesc;
        }

        public void setUnitdesc(String unitdesc) {
            this.unitdesc = unitdesc;
        }

        public String getPurcqtymin() {
            return purcqtymin;
        }

        public void setPurcqtymin(String purcqtymin) {
            this.purcqtymin = purcqtymin;
        }

        public String getLeadtime() {
            return leadtime;
        }

        public void setLeadtime(String leadtime) {
            this.leadtime = leadtime;
        }

        public String getQtymin() {
            return qtymin;
        }

        public void setQtymin(String qtymin) {
            this.qtymin = qtymin;
        }

        public String getQtymax() {
            return qtymax;
        }

        public void setQtymax(String qtymax) {
            this.qtymax = qtymax;
        }

        public String getQtyreorder() {
            return qtyreorder;
        }

        public void setQtyreorder(String qtyreorder) {
            this.qtyreorder = qtyreorder;
        }

        public String getQtypo() {
            return qtypo;
        }

        public void setQtypo(String qtypo) {
            this.qtypo = qtypo;
        }

        public String getQtyso() {
            return qtyso;
        }

        public void setQtyso(String qtyso) {
            this.qtyso = qtyso;
        }

        public String getQtywip() {
            return qtywip;
        }

        public void setQtywip(String qtywip) {
            this.qtywip = qtywip;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public Boolean getPousepid() {
            return pousepid;
        }

        public void setPousepid(Boolean pousepid) {
            this.pousepid = pousepid;
        }

        public Boolean getSousepid() {
            return sousepid;
        }

        public void setSousepid(Boolean sousepid) {
            this.sousepid = sousepid;
        }

        public Object getVendorId() {
            return vendorId;
        }

        public void setVendorId(Object vendorId) {
            this.vendorId = vendorId;
        }

        public String getVitemcode() {
            return vitemcode;
        }

        public void setVitemcode(String vitemcode) {
            this.vitemcode = vitemcode;
        }

        public String getPurcunit() {
            return purcunit;
        }

        public void setPurcunit(String purcunit) {
            this.purcunit = purcunit;
        }

        public String getSaleunit() {
            return saleunit;
        }

        public void setSaleunit(String saleunit) {
            this.saleunit = saleunit;
        }

        public String getStockunit() {
            return stockunit;
        }

        public void setStockunit(String stockunit) {
            this.stockunit = stockunit;
        }

        public Object getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(Object createdAt) {
            this.createdAt = createdAt;
        }

        public Object getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Object createdBy) {
            this.createdBy = createdBy;
        }

        public Object getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(Object updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Object updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getCosttype() {
            return costtype;
        }

        public void setCosttype(String costtype) {
            this.costtype = costtype;
        }

        public Boolean getIsimport() {
            return isimport;
        }

        public void setIsimport(Boolean isimport) {
            this.isimport = isimport;
        }
    }
}
