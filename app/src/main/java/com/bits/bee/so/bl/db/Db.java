package com.bits.bee.so.bl.db;

import android.content.Context;

import com.bits.bee.so.bl.db.dao.BatchDao;
import com.bits.bee.so.bl.db.dao.BpDao;
import com.bits.bee.so.bl.db.dao.CityDao;
import com.bits.bee.so.bl.db.dao.ItemDao;
import com.bits.bee.so.bl.db.dao.PriceDao;
import com.bits.bee.so.bl.db.dao.RegDao;
import com.bits.bee.so.bl.db.dao.SaleDao;
import com.bits.bee.so.bl.db.dao.SaledDao;
import com.bits.bee.so.bl.db.dao.SoDao;
import com.bits.bee.so.bl.db.dao.SodDao;
import com.bits.bee.so.bl.db.dao.StockDao;
import com.bits.bee.so.bl.db.dao.UnitDao;
import com.bits.bee.so.bl.db.dao.UsrDao;

/**
 * Created by basofi on 24/05/16.
 */
public class Db {

    private static Db singleton;
    private DbHelper mDbHelper;

    public static Db getInstance(){
        if(singleton == null){
            singleton = new Db();
        }
        return  singleton;
    }

    public void init(Context context){
        mDbHelper = new DbHelper(context);
        try {
            mDbHelper.getWritableDatabase();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void clearAllDb(){
        try {
            BpDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            CityDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ItemDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PriceDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            RegDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            SaleDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            SaledDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            SoDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            SodDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            StockDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            UnitDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            UsrDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            BatchDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DbHelper getDbHelper(){
        return mDbHelper;
    }
}
