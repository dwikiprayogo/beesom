package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.Sale;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by basofi on 14/06/16.
 */
public class SaleDao implements DaoCrud<Sale, Integer> {
    private static SaleDao singleton;

    public static SaleDao getInstance() {
        if (singleton == null) {
            singleton = new SaleDao();
        }
        return singleton;
    }

    @Override
    public void create(Sale model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Sale model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Sale model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Sale model) throws Exception {
        getDao().createOrUpdate(model);
    }

    public Sale readById(String id) throws Exception{
        return getDao().queryForId(Integer.parseInt(id));
    }

    @Override
    public List<Sale> read() throws Exception {
        return getDao().queryForAll();
    }

    public List<Sale> readNotUploaded() throws Exception {
        return getDao().queryForEq(Sale.ISUPLOADED, false);
    }

    @Override
    public Dao<Sale, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Sale.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Sale.class);
    }

    public BigDecimal sumTotalByDate(long startDate, long endDate) throws Exception{
        BigDecimal retVal = new BigDecimal(0);

        String query = String.format("SELECT %s FROM %s WHERE %s >= %s AND %s <= %s",
                Sale.TOTAL,
                Sale.TBL_NAME,
                Sale.TRXDATELONG,
                startDate,
                Sale.TRXDATELONG,
                endDate);

        GenericRawResults<String[]> rawResults = getDao().queryRaw(query);
        for(String[] results : rawResults){
            retVal = retVal.add(new BigDecimal(results[0]));
        }

        rawResults.close();

        return retVal;
    }

    public long countSaleByDate(long startDate, long endDate) throws Exception{
        long count = getDao().queryBuilder()
                .where()
                .ge(Sale.TRXDATELONG, startDate)
                .and()
                .le(Sale.TRXDATELONG, endDate).countOf();

        return count;
    }

    public List<Sale> readSalesByDate(long startDate, long endDate) throws Exception{
        QueryBuilder queryBuilder = getDao().queryBuilder();
        queryBuilder.where()
                .ge(Sale.TRXDATELONG, startDate)
                .and()
                .le(Sale.TRXDATELONG, endDate);

        PreparedQuery preparedQuery = queryBuilder.prepare();

        return getDao().query(preparedQuery);
    }
}