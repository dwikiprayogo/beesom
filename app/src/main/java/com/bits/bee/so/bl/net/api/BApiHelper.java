package com.bits.bee.so.bl.net.api;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by basofi on 5/10/16.
 */
public class BApiHelper {

    private static OkHttpClient genHttpClient(Context context, String apiKey) {
        InputStream certStream = null;

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        try {
            certStream = context.getAssets().open("android_cacerts");

            KeyStore trustStore = KeyStore.getInstance("BKS");
            trustStore.load(certStream, "java1milBsnsBEEFR33".toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
            tmf.init(trustStore);

            X509TrustManager trustManager = (X509TrustManager) tmf.getTrustManagers()[0];

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);

            httpClient.sslSocketFactory(sslContext.getSocketFactory());
//            httpClient.sslSocketFactory(getSSLConfig(context).getSocketFactory());
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        final String apiBearer = String.format("Bearer %s", apiKey);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .addHeader("Authorization", apiBearer)
                        .addHeader("Content-Type", "application/json")
//                        .header("Authorization", "Bearer eyJ0eXAiOiJKV1MiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcHAuYmVlY2xvdWQuaWQiLCJqdGkiOiI4MjcwZWM0MGRlN2NmMmMwNjRkODgzOGEyZWIwMDk2NyIsImRibmFtZSI6IjQ3cHRfaGFkaV9rYXJ5YV9hYmFkaSIsImRiaG9zdCI6ImRiMDAxLmJlZWNsb3VkLmlkIn0.YPuyQynrS4OtnHF8GTuTWJplt1gaAZ_l2E3huypSdd4")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(logging);

        return httpClient.build();
    }

    public static BApi build(Context context, String baseUrl, String apiKey) {
        OkHttpClient client = genHttpClient(context, apiKey);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .client(client)
                .build();
        return retrofit.create(BApi.class);
    }
}
