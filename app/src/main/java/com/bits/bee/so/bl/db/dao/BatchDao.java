package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.Batch;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.List;

/**
 * Created by basofi on 27/07/16.
 */
public class BatchDao implements DaoCrud<Batch, Integer> {

    private static BatchDao singleton;

    public static BatchDao getInstance(){
        if(singleton == null){
            singleton = new BatchDao();
        }
        return singleton;
    }

    @Override
    public void create(Batch model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Batch model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Batch model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Batch model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<Batch> read() throws Exception {
        return getDao().queryForAll();
    }

    @Override
    public Dao<Batch, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Batch.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Batch.class);
    }

    public int lastBatchId() throws Exception {
        QueryBuilder queryBuilder = getDao().queryBuilder();
        queryBuilder.limit((long)1)
                .orderBy(Batch.BATCH_ID, false);
        PreparedQuery preparedQuery = queryBuilder.prepare();
        List<Batch> batchList = getDao().query(preparedQuery);
        return batchList.get(0).getBatchId();
    }
}