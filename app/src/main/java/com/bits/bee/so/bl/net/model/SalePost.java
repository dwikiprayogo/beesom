package com.bits.bee.so.bl.net.model;

import com.bits.bee.so.bl.db.dao.SaledDao;
import com.bits.bee.so.bl.db.model.Sale;
import com.bits.bee.so.bl.db.model.Saled;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 11/07/16.
 */
public class SalePost {
    @SerializedName("salearray")
    @Expose
    private List<SaleArray> saleArray = new ArrayList<SaleArray>();

    public List<SaleArray> getSaleArray() {
        return saleArray;
    }

    public void setSaleArray(List<SaleArray> saleArray) {
        this.saleArray = saleArray;
    }

    public void addSale(Sale sale){
        SaleArray saleArray = new SaleArray(sale);
        getSaleArray().add(saleArray);
    }

    public class SaleArray {
        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("trxdate")
        @Expose
        private String trxdate;
        @SerializedName("bp_id")
        @Expose
        private String bpId;
        @SerializedName("crc_id")
        @Expose
        private String crcId;
        @SerializedName("cash_id")
        @Expose
        private String cashId;
        @SerializedName("termtype")
        @Expose
        private String termtype;
        @SerializedName("duedays")
        @Expose
        private String duedays;
        @SerializedName("subtotal")
        @Expose
        private String subtotal;
        @SerializedName("basesubtotal")
        @Expose
        private String basesubtotal;
        @SerializedName("discexp")
        @Expose
        private String discexp;
        @SerializedName("taxamt")
        @Expose
        private String taxamt;
        @SerializedName("basetaxamt")
        @Expose
        private String basetaxamt;
        @SerializedName("basefistaxamt")
        @Expose
        private String basefistaxamt;
        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("basetotal")
        @Expose
        private String basetotal;
        @SerializedName("paidamt")
        @Expose
        private String paidamt;
        @SerializedName("coordinate")
        @Expose
        private String coordinate;
        @SerializedName("saleds")
        @Expose
        private List<SaledPost> saledPosts = new ArrayList<SaledPost>();

        public SaleArray(Sale sale){
            this.setId(sale.getId());
            this.setPaidamt(sale.getPaidamt().toPlainString());
            this.setDuedays(""+sale.getDuedays());
            this.setDiscexp(sale.getDiscexp());
            this.setCrcId("1");
            this.setBasefistaxamt(sale.getBasefistaxamt().toPlainString());
            this.setBasesubtotal(sale.getBasesubtotal().toPlainString());
            this.setBasetaxamt(sale.getBasetaxamt().toPlainString());
            this.setBasetotal(sale.getBasetotal().toPlainString());
            this.setBpId(sale.getBpolid());
            this.setCashId("1");
            this.setPaidamt(sale.getPaidamt().toPlainString());
            this.setTaxamt(sale.getTaxamt().toPlainString());
            this.setSubtotal(sale.getSubtotal().toPlainString());
            this.setTermtype(sale.getTermtype());
            this.setTotal(sale.getTotal().toString());
            this.setTrxdate(sale.getTrxdate());
            this.setCoordinate(sale.getTrxCoordinate());
            try {
                List<Saled> saledList = SaledDao.getInstance().readBySale(sale);
                for(int i = 0 ; i < saledList.size() ; i++){
                    Saled saled = saledList.get(i);
                    SaledPost saledPost = new SaledPost(saled);
                    getSaledPosts().add(saledPost);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTrxdate() {
            return trxdate;
        }

        public void setTrxdate(String trxdate) {
            this.trxdate = trxdate;
        }

        public String getBpId() {
            return bpId;
        }

        public void setBpId(String bpId) {
            this.bpId = bpId;
        }

        public String getCrcId() {
            return crcId;
        }

        public void setCrcId(String crcId) {
            this.crcId = crcId;
        }

        public String getCashId() {
            return cashId;
        }

        public void setCashId(String cashId) {
            this.cashId = cashId;
        }

        public String getTermtype() {
            return termtype;
        }

        public void setTermtype(String termtype) {
            this.termtype = termtype;
        }

        public String getDuedays() {
            return duedays;
        }

        public void setDuedays(String duedays) {
            this.duedays = duedays;
        }

        public String getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(String subtotal) {
            this.subtotal = subtotal;
        }

        public String getBasesubtotal() {
            return basesubtotal;
        }

        public void setBasesubtotal(String basesubtotal) {
            this.basesubtotal = basesubtotal;
        }

        public String getDiscexp() {
            return discexp;
        }

        public void setDiscexp(String discexp) {
            this.discexp = discexp;
        }

        public String getTaxamt() {
            return taxamt;
        }

        public void setTaxamt(String taxamt) {
            this.taxamt = taxamt;
        }

        public String getBasetaxamt() {
            return basetaxamt;
        }

        public void setBasetaxamt(String basetaxamt) {
            this.basetaxamt = basetaxamt;
        }

        public String getBasefistaxamt() {
            return basefistaxamt;
        }

        public void setBasefistaxamt(String basefistaxamt) {
            this.basefistaxamt = basefistaxamt;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getBasetotal() {
            return basetotal;
        }

        public void setBasetotal(String basetotal) {
            this.basetotal = basetotal;
        }

        public String getPaidamt() {
            return paidamt;
        }

        public void setPaidamt(String paidamt) {
            this.paidamt = paidamt;
        }

        public List<SaledPost> getSaledPosts() {
            return saledPosts;
        }

        public void setSaledPosts(List<SaledPost> saledPosts) {
            this.saledPosts = saledPosts;
        }

        public String getCoordinate() {
            return coordinate;
        }

        public void setCoordinate(String coordinate) {
            this.coordinate = coordinate;
        }

        public class SaledPost {

            @SerializedName("dno")
            @Expose
            private String dno;
            @SerializedName("item_id")
            @Expose
            private String itemId;
            @SerializedName("itemname")
            @Expose
            private String itemname;
            @SerializedName("qty")
            @Expose
            private String qty;
            @SerializedName("wh_id")
            @Expose
            private String whId;
            @SerializedName("listprice")
            @Expose
            private String listprice;
            @SerializedName("conv")
            @Expose
            private String conv;
            @SerializedName("unit")
            @Expose
            private String unit;
            @SerializedName("baseprice")
            @Expose
            private String baseprice;
            @SerializedName("discexp")
            @Expose
            private String discexp;
            @SerializedName("discamt")
            @Expose
            private String discamt;
            @SerializedName("totaldiscamt")
            @Expose
            private String totaldiscamt;
            @SerializedName("disc2amt")
            @Expose
            private String disc2amt;
            @SerializedName("totaldisc2amt")
            @Expose
            private String totaldisc2amt;
            @SerializedName("subtotal")
            @Expose
            private String subtotal;
            @SerializedName("basesubtotal")
            @Expose
            private String basesubtotal;
            @SerializedName("tax_code")
            @Expose
            private String taxCode;
            @SerializedName("taxableamt")
            @Expose
            private String taxableamt;
            @SerializedName("taxamt")
            @Expose
            private String taxamt;
            @SerializedName("totaltaxamt")
            @Expose
            private String totaltaxamt;
            @SerializedName("basetotaltaxamt")
            @Expose
            private String basetotaltaxamt;
            @SerializedName("basefistotaltaxamt")
            @Expose
            private String basefistotaltaxamt;

            public SaledPost(Saled saled){
                this.setSubtotal(saled.getSubtotal().toPlainString());
                this.setTaxamt(saled.getTaxamt().toPlainString());
                this.setBasefistotaltaxamt(saled.getBasefistotaltaxamt().toPlainString());
                this.setBaseprice(saled.getBaseprice().toPlainString());
                this.setBasesubtotal(saled.getBasesubtotal().toPlainString());
                this.setBasetotaltaxamt(saled.getBasetotaltaxamt().toPlainString());
                this.setConv("1");
//                this.setDisc2amt();
                this.setDiscamt(saled.getDiscamt().toPlainString());
                this.setDiscexp(saled.getDiscexp());
                this.setDno(""+saled.getDno());
                this.setItemId(saled.getItemolid());
                this.setItemname(saled.getItemname());
                this.setListprice(saled.getListprice().toPlainString());
                this.setQty(saled.getQty().toPlainString());
                this.setSubtotal(saled.getSubtotal().toPlainString());
                this.setTaxableamt(saled.getTaxableamt().toPlainString());
                this.setTaxamt(saled.getTaxamt().toPlainString());
                this.setTaxCode(saled.getTax_code());
//                this.setTotaldisc2amt();
                this.setTotaldiscamt(saled.getTotaldiscamt().toPlainString());
                this.setTotaltaxamt(saled.getTotaltaxamt().toPlainString());
                this.setUnit(saled.getUnit_olid());
                this.setWhId(saled.getWhId());
            }

            public String getDno() {
                return dno;
            }

            public void setDno(String dno) {
                this.dno = dno;
            }

            public String getItemId() {
                return itemId;
            }

            public void setItemId(String itemId) {
                this.itemId = itemId;
            }

            public String getItemname() {
                return itemname;
            }

            public void setItemname(String itemname) {
                this.itemname = itemname;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }

            public String getWhId() {
                return whId;
            }

            public void setWhId(String whId) {
                this.whId = whId;
            }

            public String getListprice() {
                return listprice;
            }

            public void setListprice(String listprice) {
                this.listprice = listprice;
            }

            public String getConv() {
                return conv;
            }

            public void setConv(String conv) {
                this.conv = conv;
            }

            public String getUnit() {
                return unit;
            }

            public void setUnit(String unit) {
                this.unit = unit;
            }

            public String getBaseprice() {
                return baseprice;
            }

            public void setBaseprice(String baseprice) {
                this.baseprice = baseprice;
            }

            public String getDiscexp() {
                return discexp;
            }

            public void setDiscexp(String discexp) {
                this.discexp = discexp;
            }

            public String getDiscamt() {
                return discamt;
            }

            public void setDiscamt(String discamt) {
                this.discamt = discamt;
            }

            public String getTotaldiscamt() {
                return totaldiscamt;
            }

            public void setTotaldiscamt(String totaldiscamt) {
                this.totaldiscamt = totaldiscamt;
            }

            public String getDisc2amt() {
                return disc2amt;
            }

            public void setDisc2amt(String disc2amt) {
                this.disc2amt = disc2amt;
            }

            public String getTotaldisc2amt() {
                return totaldisc2amt;
            }

            public void setTotaldisc2amt(String totaldisc2amt) {
                this.totaldisc2amt = totaldisc2amt;
            }

            public String getSubtotal() {
                return subtotal;
            }

            public void setSubtotal(String subtotal) {
                this.subtotal = subtotal;
            }

            public String getBasesubtotal() {
                return basesubtotal;
            }

            public void setBasesubtotal(String basesubtotal) {
                this.basesubtotal = basesubtotal;
            }

            public String getTaxCode() {
                return taxCode;
            }

            public void setTaxCode(String taxCode) {
                this.taxCode = taxCode;
            }

            public String getTaxableamt() {
                return taxableamt;
            }

            public void setTaxableamt(String taxableamt) {
                this.taxableamt = taxableamt;
            }

            public String getTaxamt() {
                return taxamt;
            }

            public void setTaxamt(String taxamt) {
                this.taxamt = taxamt;
            }

            public String getTotaltaxamt() {
                return totaltaxamt;
            }

            public void setTotaltaxamt(String totaltaxamt) {
                this.totaltaxamt = totaltaxamt;
            }

            public String getBasetotaltaxamt() {
                return basetotaltaxamt;
            }

            public void setBasetotaltaxamt(String basetotaltaxamt) {
                this.basetotaltaxamt = basetotaltaxamt;
            }

            public String getBasefistotaltaxamt() {
                return basefistotaltaxamt;
            }

            public void setBasefistotaltaxamt(String basefistotaltaxamt) {
                this.basefistotaltaxamt = basefistotaltaxamt;
            }
        }
    }
}