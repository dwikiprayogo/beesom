package com.bits.bee.so.bl.db.model;

import com.bits.bee.so.bl.net.model.ItemGet;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by basofi on 25/05/16.
 */
@DatabaseTable(tableName = Item.TBL_NAME)
public class Item implements Serializable{
    public static final String TBL_NAME = "item";

    public static final String ID = "id";
    public static final String OL_ID = "ol_id";
    public static final String CODE = "code";
    public static final String BARCODE = "barcode";
    public static final String NAME = "name";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = OL_ID)
    private String olId;
    @DatabaseField(columnName = CODE)
    private String code;
    @DatabaseField(columnName = BARCODE)
    private String barcode;
    @DatabaseField(columnName = NAME)
    private String name;

    public Item(){

    }

    public Item(ItemGet.Datum itemGetDatum){
        setOlId(itemGetDatum.getId());
        setBarcode(itemGetDatum.getBarcode());
        setCode(itemGetDatum.getCode());
        setName(itemGetDatum.getName1());
    }

    public String getOlId() {
        return olId;
    }

    public void setOlId(String olId) {
        this.olId = olId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}