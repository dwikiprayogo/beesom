package com.bits.bee.so.bl.trans;

import com.bits.bee.so.bl.db.dao.RegDao;
import com.bits.bee.so.bl.db.dao.SaledDao;
import com.bits.bee.so.bl.db.model.Bp;
import com.bits.bee.so.bl.db.model.Reg;
import com.bits.bee.so.bl.db.model.Sale;
import com.bits.bee.so.bl.db.model.Saled;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 02/08/16.
 */
public class SaleTrans implements Serializable {

    private Sale mSale;
    private List<Saled> mSaledList = new ArrayList<>();
    private Bp mBp;
    private Reg mReg;

    public SaleTrans(){
        mSale = new Sale();
        try {
            mReg = RegDao.getInstance().read().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SaleTrans(Sale sale){
        this.mSale = sale;
        try {
            mSaledList = SaledDao.getInstance().readBySale(mSale);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mReg = RegDao.getInstance().read().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Sale getSale() {
        return mSale;
    }

    public void setBp(Bp bp){
        this.mBp = bp;
        mSale.setBpId(bp);
        mSale.setBpolid(bp.getOlId());
    }

    public void setDueDays(int dueDays){
        mSale.setDuedays(dueDays);
    }

    public void setTermType(String termType){
        mSale.setTermtype(termType);
    }

    public String getTermType(){
        return mSale.getTermtype();
    }
}
