package com.bits.bee.so.bl.db.model;

import com.bits.bee.so.bl.net.model.RegGet;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by basofi on 13/07/16.
 */
@DatabaseTable(tableName = Reg.TBL_NAME)
public class Reg implements Serializable {
    public static final String TBL_NAME = "reg";

    public static final String CODE = "code";
    public static final String NAME = "name";
    public static final String VALUE = "value";
    public static final String ISVISIBLE = "isvisible";
    public static final String MODUL_CODE = "modul_code";
    public static final String VALEDITOR = "valeditor";

    @DatabaseField(columnName = CODE, id = true)
    private String code;
    @DatabaseField(columnName = NAME)
    private String name;
    @DatabaseField(columnName = VALUE)
    private String value;
    @DatabaseField(columnName = ISVISIBLE)
    private boolean isVisible;
    @DatabaseField(columnName = MODUL_CODE)
    private String modulCode;
    @DatabaseField(columnName = VALEDITOR)
    private String valeditor;

    public Reg(){

    }

    public Reg(RegGet.Data regGetData){
        setCode(regGetData.getCode());
        setIsVisible(regGetData.getIsvisible());
        setModulCode(regGetData.getModulCode());
        setName(regGetData.getName());
        setValeditor(regGetData.getValeditor());
        setValue(regGetData.getValue());
    }

    public String getValeditor() {
        return valeditor;
    }

    public void setValeditor(String valeditor) {
        this.valeditor = valeditor;
    }

    public String getModulCode() {
        return modulCode;
    }

    public void setModulCode(String modulCode) {
        this.modulCode = modulCode;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setIsVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
