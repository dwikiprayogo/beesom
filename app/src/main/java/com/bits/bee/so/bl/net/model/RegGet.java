package com.bits.bee.so.bl.net.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by basofi on 13/07/16.
 */
public class RegGet {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private Data data;

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The data
     */
    public Data getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("isvisible")
        @Expose
        private Boolean isvisible;
        @SerializedName("modul_code")
        @Expose
        private String modulCode;
        @SerializedName("valeditor")
        @Expose
        private String valeditor;

        /**
         * @return The code
         */
        public String getCode() {
            return code;
        }

        /**
         * @param code The code
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * @return The name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The value
         */
        public String getValue() {
            return value;
        }

        /**
         * @param value The value
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * @return The isvisible
         */
        public Boolean getIsvisible() {
            return isvisible;
        }

        /**
         * @param isvisible The isvisible
         */
        public void setIsvisible(Boolean isvisible) {
            this.isvisible = isvisible;
        }

        /**
         * @return The modulCode
         */
        public String getModulCode() {
            return modulCode;
        }

        /**
         * @param modulCode The modul_code
         */
        public void setModulCode(String modulCode) {
            this.modulCode = modulCode;
        }

        /**
         * @return The valeditor
         */
        public String getValeditor() {
            return valeditor;
        }

        /**
         * @param valeditor The valeditor
         */
        public void setValeditor(String valeditor) {
            this.valeditor = valeditor;
        }
    }
}
