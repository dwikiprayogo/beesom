package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.Item;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 27/05/16.
 */
public class ItemDao implements DaoCrud<Item, Integer> {
    private static ItemDao singleton;

    public static ItemDao getInstance(){
        if(singleton == null){
            singleton = new ItemDao();
        }
        return singleton;
    }

    @Override
    public void create(Item model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Item model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Item model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Item model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<Item> read() throws Exception {
        return getDao().queryForAll();
    }

    public Item readByOlId(String olId) throws Exception{
        List<Item> itemList = new ArrayList<>();
        itemList = getDao().queryForEq(Item.OL_ID, olId);
        if(itemList.size() > 0){
            return itemList.get(0);
        }else {
            throw new Exception("No item found");
        }
    }

    @Override
    public Dao<Item, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Item.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Item.class);
    }
}
