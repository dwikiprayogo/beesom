package com.bits.bee.so.bl.net.api;

import com.bits.bee.so.bl.net.model.BpGet;
import com.bits.bee.so.bl.net.model.BpPost;
import com.bits.bee.so.bl.net.model.CityGet;
import com.bits.bee.so.bl.net.model.ItemGet;
import com.bits.bee.so.bl.net.model.PostReturn;
import com.bits.bee.so.bl.net.model.PriceGet;
import com.bits.bee.so.bl.net.model.RegGet;
import com.bits.bee.so.bl.net.model.SalePost;
import com.bits.bee.so.bl.net.model.SaleReturn;
import com.bits.bee.so.bl.net.model.SoPost;
import com.bits.bee.so.bl.net.model.StockGet;
import com.bits.bee.so.bl.net.model.UnitGet;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by basofi on 5/10/16.
 */
public interface BApi {

    @POST("api/v1/so")
    Call<PostReturn> postSo(@Body SoPost soPost);

    @POST("api/v1/bp")
    Call<PostReturn> postBp(@Body BpPost bpPost);

    @POST("api/v1/sale")
    Call<SaleReturn> postSale(@Body SalePost salePost);

    @GET("api/v1/city")
    Call<CityGet> getCity();

    @GET("api/v1/bp?expand=bpaccount")
    Call<BpGet> getBp();

    @GET("api/v1/item")
    Call<ItemGet> getItem();

    @GET("api/v1/stock")
    Call<StockGet> getStock();

    @GET("api/v1/price")
    Call<PriceGet> getPrice();

    @GET("api/v1/unit")
    Call<UnitGet> getUnit();

    @GET("api/v1/reg?code=WHSALE")
    Call<RegGet> getReg();
}