package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.Bp;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 27/05/16.
 */
public class BpDao implements DaoCrud<Bp, Integer> {

    private static BpDao singleton;

    public static BpDao getInstance(){
        if(singleton == null){
            singleton = new BpDao();
        }
        return singleton;
    }

    @Override
    public void create(Bp model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Bp model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Bp model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Bp model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<Bp> read() throws Exception {
        return getDao().queryForAll();
    }

    public Bp readByOlId(String olId) throws Exception{
        List<Bp> bpList = new ArrayList<>();
        bpList = getDao().queryForEq(Bp.OL_ID, olId);
        if(bpList.size() > 0){
            return bpList.get(0);
        }else {
            throw new Exception("Ol_Id not found");
        }
    }

    public List<Bp> readNotUploaded() throws Exception{
        List<Bp> bpList = new ArrayList<>();
        bpList = getDao().queryForEq(Bp.ISUPLOADED, false);
        return bpList;
    }

    @Override
    public Dao<Bp, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Bp.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Bp.class);
    }

    public long countBpByDate(long startDate, long endDate) throws Exception{
        long count = getDao().queryBuilder()
                .where()
                .ge(Bp.CREATED_AT, startDate)
                .and()
                .le(Bp.CREATED_AT, endDate).countOf();

        return count;
    }

    public List<Bp> readBpByDate(long startDate, long endDate) throws Exception{
        QueryBuilder queryBuilder = getDao().queryBuilder();
        queryBuilder.where()
                .ge(Bp.CREATED_AT, startDate)
                .and()
                .le(Bp.CREATED_AT, endDate);
        PreparedQuery preparedQuery = queryBuilder.prepare();

        return getDao().query(preparedQuery);
    }
}
