package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.So;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.List;

/**
 * Created by basofi on 13/06/16.
 */
public class SoDao implements DaoCrud<So, Integer> {

    private static SoDao singleton;

    public static SoDao getInstance() {
        if (singleton == null) {
            singleton = new SoDao();
        }
        return singleton;
    }

    @Override
    public void create(So model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(So model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(So model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(So model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<So> read() throws Exception {
        return getDao().queryForAll();
    }

    public List<So> readNotUploaded() throws Exception{
        List<So> soList = getDao().queryForEq(So.ISUPLOADED, false);
        return soList;
    }

    @Override
    public Dao<So, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(So.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, So.class);
    }

    public long countSoByDate(long startDate, long endDate) throws Exception{
        long count = getDao().queryBuilder()
                .where()
                .ge(So.TRXDATELONG, startDate)
                .and()
                .le(So.TRXDATELONG, endDate).countOf();

        return count;
    }
}
