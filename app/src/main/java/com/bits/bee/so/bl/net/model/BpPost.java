package com.bits.bee.so.bl.net.model;

import com.bits.bee.so.bl.db.model.Bp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 30/06/16.
 */
public class BpPost {

    @SerializedName("bparray")
    @Expose
    private List<BpArray> bpArray = new ArrayList<BpArray>();

    /**
     * @return The bparray
     */
    public List<BpArray> getBpArray() {
        return bpArray;
    }

    /**
     * @param bpArray The bparray
     */
    public void setBpArray(List<BpArray> bpArray) {
        this.bpArray = bpArray;
    }

    public void addBp(Bp bp){
        BpArray bpArray = new BpArray(bp);
        getBpArray().add(bpArray);
    }

    public class BpArray {
        @SerializedName("businessname")
        @Expose
        private String businessname;
        @SerializedName("ownname")
        @Expose
        private String ownname;
        @SerializedName("hp")
        @Expose
        private String hp;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("city_code")
        @Expose
        private String cityCode;
        @SerializedName("coordinate")
        @Expose
        private String coordinate;

        public BpArray(Bp bp){
            setBusinessname(bp.getNamaPerusahaan());
            setOwnname(bp.getNamaPemilik());
            setHp(bp.getHp());
            setAddress(bp.getAlamat());
            setCityCode(bp.getKota());
            setCoordinate(bp.getGpsLocation());
        }

        /**
         * @return The businessname
         */
        public String getBusinessname() {
            return businessname;
        }

        /**
         * @param businessname The businessname
         */
        public void setBusinessname(String businessname) {
            this.businessname = businessname;
        }

        /**
         * @return The ownname
         */
        public String getOwnname() {
            return ownname;
        }

        /**
         * @param ownname The ownname
         */
        public void setOwnname(String ownname) {
            this.ownname = ownname;
        }

        /**
         * @return The hp
         */
        public String getHp() {
            return hp;
        }

        /**
         * @param hp The hp
         */
        public void setHp(String hp) {
            this.hp = hp;
        }

        /**
         * @return The address
         */
        public String getAddress() {
            return address;
        }

        /**
         * @param address The address
         */
        public void setAddress(String address) {
            this.address = address;
        }

        /**
         * @return The cityCode
         */
        public String getCityCode() {
            return cityCode;
        }

        /**
         * @param cityCode The city_code
         */
        public void setCityCode(String cityCode) {
            this.cityCode = cityCode;
        }

        /**
         * @return The coordinate
         */
        public String getCoordinate() {
            return coordinate;
        }

        /**
         * @param coordinate The coordinate
         */
        public void setCoordinate(String coordinate) {
            this.coordinate = coordinate;
        }

    }
}
