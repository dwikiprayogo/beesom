package com.bits.bee.so.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.model.Usr;
import com.bits.bee.so.service.ProfileService;
import com.bits.bee.so.util.BUtil;
import com.bits.bee.so.view.dialog.BDialog;
import com.bits.bee.so.view.fragment.MainDashboardFragment;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    public static final int MY_PERMISSION_ACCESS_COURSE_LOCATION = 12;

    private Toolbar mToolbar;
    private Drawer mDrawer;
    private MainDashboardFragment mMainDashboardFragment = MainDashboardFragment.newInstance();
    private HashMap<IProfile, Usr> mUsrMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

//    private void reqPermission(){
//        if (ActivityCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
//                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
//                        != PackageManager.PERMISSION_GRANTED) {
//
//            BDialog.showInfoDialog(this, "No GPS Permission", "Untuk menjalankan aplikasi ini, dibutuhkan persetujuan untuk menggunakan GPS.\n Silahkan tekan OK untuk menuju halaman persetujuan penggunaan GPS.", new MaterialDialog.ButtonCallback() {
//                @Override
//                public void onPositive(MaterialDialog dialog) {
//                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{
//                                    android.Manifest.permission.ACCESS_COARSE_LOCATION},
//                            MY_PERMISSION_ACCESS_COURSE_LOCATION);
//                }
//            });
//        }
//    }

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(mToolbar);

        getFragmentManager().beginTransaction().replace(R.id.main_flContent,
                mMainDashboardFragment).commit();

        initSlideMenu();
    }

    private void logOut() {
        Db.getInstance().clearAllDb();
        ProfileService.getInstance().load();

        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void initSlideMenu() {

        Usr usr = ProfileService.getInstance().getActiveUsr();

        PrimaryDrawerItem pdiDashboard = new PrimaryDrawerItem()
                .withName(R.string.mainactivity_menu_dashboard)
                .withIcon(R.drawable.ic_insert_chart);
        PrimaryDrawerItem pdiSalesOrder = new PrimaryDrawerItem()
                .withName(R.string.mainactivity_menu_salesorder)
                .withIcon(R.drawable.ic_assignment_black_24dp);
        PrimaryDrawerItem pdiSales = new PrimaryDrawerItem()
                .withName(R.string.mainactivity_menu_sales)
                .withIcon(R.drawable.ic_assignment_turned_in_black_24dp);
        PrimaryDrawerItem pdiCekitem = new PrimaryDrawerItem()
                .withName(R.string.mainactivity_menu_cekitem)
                .withIcon(R.drawable.ic_find_in_page_black_24dp);
        PrimaryDrawerItem pdiNewCustomer = new PrimaryDrawerItem()
                .withName(R.string.mainactivity_menu_newcustomer)
                .withIcon(R.drawable.ic_face_black_24dp);
        PrimaryDrawerItem pdiDownload = new PrimaryDrawerItem()
                .withName(R.string.mainactivity_menu_download)
                .withIcon(R.drawable.ic_file_download_black_24dp);
        PrimaryDrawerItem pdiUpload = new PrimaryDrawerItem()
                .withName(R.string.mainactivity_menu_upload)
                .withIcon(R.drawable.ic_file_upload_black_24dp);
        PrimaryDrawerItem pdiDownloadUpload = new PrimaryDrawerItem()
                .withName(R.string.mainactivity_menu_downloaddanupload)
                .withIcon(R.drawable.ic_swap_vertical_circle_black_24dp);
        PrimaryDrawerItem pdiLogout = new PrimaryDrawerItem()
                .withName(R.string.mainactivity_menu_logout)
                .withIcon(R.drawable.ic_power_settings_new_black_24dp);

        IProfile iProfile = new ProfileDrawerItem()
                .withName(usr.getUsername())
                .withIcon(ContextCompat.getDrawable(this, R.drawable.ic_launcher_white));

        final AccountHeader mAccountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.primary)
//                .addProfiles(profiles)
                .addProfiles(iProfile)
                .withSelectionListEnabled(false)
                .withProfileImagesClickable(false)
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile iProfile, boolean b) {
                        if (mUsrMap.containsKey(iProfile)) {
                            Usr usr = mUsrMap.get(iProfile);
                            ProfileService.getInstance().setActiveUsr(usr);
                        } else {
                            startActivity(new Intent(MainActivity.this, QrCodeRegActivity.class));

                        }
                        return true; //setEvent
                    }
                })
                .withSelectionListEnabled(false)
                .build();


//        mAccountHeader.setActiveProfile(profiles[indexOfActive]);

        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToolbar)
                .withAccountHeader(mAccountHeader)
                .addDrawerItems(
                        pdiDashboard,
                        pdiSalesOrder,
                        pdiSales,
                        pdiCekitem,
                        pdiNewCustomer,
                        new SectionDrawerItem()
                                .withName(R.string.mainactivity_menu_updatedata)
                                .setDivider(false),
                        pdiDownload,
                        pdiUpload,
                        pdiDownloadUpload,
                        pdiLogout
                )
                .withSelectedItem(-1)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> adapterView,
                                               View view,
                                               int i,
                                               long l,
                                               IDrawerItem iDrawerItem) {
                        switch (i) {
                            case 0:
                                getFragmentManager().beginTransaction().replace(R.id.main_flContent,
                                        mMainDashboardFragment).commit();
                                mDrawer.setSelection(-1);
                                break;
                            case 1:
                                startActivity(new Intent(MainActivity.this, SoActivity.class));
                                mDrawer.setSelection(-1);
                                break;
                            case 2:
                                startActivity(new Intent(MainActivity.this, SalesActivity.class));
                                mDrawer.setSelection(-1);
                                break;
                            case 3:
                                startActivity(new Intent(MainActivity.this, CheckItemActivity.class));
                                mDrawer.setSelection(-1);
                                break;
                            case 4:
                                startActivity(new Intent(MainActivity.this, BpActivity.class));
                                mDrawer.setSelection(-1);
                                break;
                            case 6:
                                startActivity(new Intent(MainActivity.this, ImportActivity.class));
                                mDrawer.setSelection(-1);
                                break;
                            case 7:
                                startActivity(new Intent(MainActivity.this, UploadActivity.class));
                                mDrawer.setSelection(-1);
                                break;
                            case 8:
                                BDialog.showInfoDialog(MainActivity.this, "Belum Tersedia", "Menu Belum Tersedia");
                                mDrawer.setSelection(-1);
                                break;
                            case 9:
                                BDialog.showChoiceDialog(MainActivity.this,
                                        "Log Out", "Yakin untuk log out?\nSemua data yang tersimpan akan dihapus.",
                                        new MaterialDialog.ButtonCallback() {
                                            @Override
                                            public void onPositive(MaterialDialog dialog) {
                                                logOut();
                                                dialog.dismiss();
                                            }
                                        });
                                mDrawer.setSelection(-1);
                                break;
                        }
                        return false;
                    }
                })
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!BUtil.gpsCheck(this)){
            BUtil.openGpsSetting(this);
        }
    }

    @Override
    public void onBackPressed() {
        if(mDrawer.isDrawerOpen()){
            mDrawer.closeDrawer();
        }else {
            super.onBackPressed();
        }
    }
}