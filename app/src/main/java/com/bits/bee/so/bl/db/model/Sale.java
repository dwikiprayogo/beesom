package com.bits.bee.so.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by basofi on 10/06/16.
 */
@DatabaseTable(tableName = Sale.TBL_NAME)
public class Sale implements Serializable{
    public static final String TBL_NAME = "sale";

    public static final String ID = "id";
    public static final String TRXNO = "trxno";
    public static final String TRXDATE = "trxdate";
    public static final String BP_OLID = "bp_olid";
    public static final String BP_ID = "bp_id";
    public static final String CREATED_AT = "created_at";
    public static final String TAXED = "taxed";
    public static final String TAXINC = "taxinc";
    public static final String BILLADDR = "billaddr";
    public static final String SHIPADDR = "shipaddr";
    public static final String SUBTOTAL = "subtotal";
    public static final String BASESUBTOTAL = "basesubtotal";
    public static final String DISCEXP = "discexp";
    public static final String DISCAMT = "discamt";
    public static final String BASEDISCAMT = "basediscamt";
    public static final String TAXAMT = "taxamt";
    public static final String BASETAXAMT = "basetaxamt";
    public static final String BASEFISTAXAMT = "basefistaxamt";
    public static final String TOTAL = "total";
    public static final String BASETOTAL = "basetotal";
    public static final String DPAMT = "dpamt";
    public static final String PAIDAMT = "paidamt";
    public static final String PAYSTATUS = "paystatus";
    public static final String DUEDAYS = "duedays";
    public static final String TERMTYPE = "termtype";
    public static final String ISUPLOADED = "isuploaded";
    public static final String TRXDATELONG = "trxdatelong";
    public static final String TRXCOORDINATE = "trxcoordinate";

    @DatabaseField(generatedId = true, columnName = ID)
    private int id;
    @DatabaseField(columnName = TRXNO)
    private String trxno;
    @DatabaseField(columnName = TRXDATE)
    private String trxdate;
    @DatabaseField(columnName = BP_OLID)
    private String bpolid;
    @DatabaseField(columnName = CREATED_AT)
    private String created_at;
    @DatabaseField(columnName = TAXED)
    private boolean taxed;
    @DatabaseField(columnName = TAXINC)
    private boolean taxinc;
    @DatabaseField(columnName = BILLADDR)
    private String billaddr;
    @DatabaseField(columnName = SHIPADDR)
    private String shipaddr;
    @DatabaseField(columnName = SUBTOTAL)
    private BigDecimal subtotal = new BigDecimal(0);
    @DatabaseField(columnName = BASESUBTOTAL)
    private BigDecimal basesubtotal = new BigDecimal(0);
    @DatabaseField(columnName = DISCEXP)
    private String discexp;
    @DatabaseField(columnName = DISCAMT)
    private BigDecimal discamt = new BigDecimal(0);
    @DatabaseField(columnName = BASEDISCAMT)
    private BigDecimal basediscamt = new BigDecimal(0);
    @DatabaseField(columnName = TAXAMT)
    private BigDecimal taxamt = new BigDecimal(0);
    @DatabaseField(columnName = BASETAXAMT)
    private BigDecimal basetaxamt = new BigDecimal(0);
    @DatabaseField(columnName = BASEFISTAXAMT)
    private BigDecimal basefistaxamt = new BigDecimal(0);
    @DatabaseField(columnName = TOTAL)
    private BigDecimal total = new BigDecimal(0);
    @DatabaseField(columnName = BASETOTAL)
    private BigDecimal basetotal = new BigDecimal(0);
    @DatabaseField(columnName = DPAMT)
    private BigDecimal dpamt = new BigDecimal(0);
    @DatabaseField(columnName = PAIDAMT)
    private BigDecimal paidamt = new BigDecimal(0);
    @DatabaseField(columnName = PAYSTATUS)
    private String paystatus;
    @DatabaseField(columnName = DUEDAYS)
    private int duedays;
    @DatabaseField(columnName = TERMTYPE)
    private String termtype;
    @DatabaseField(columnName = ISUPLOADED)
    private boolean isUploaded;
    @DatabaseField(columnName = TRXDATELONG)
    private long trxdateLong;
    @DatabaseField(columnName = BP_ID, foreign = true, foreignAutoRefresh = true)
    private Bp bpId;
    @DatabaseField(columnName = TRXCOORDINATE)
    private String trxCoordinate;

    public void updateSale(So so){
        setTotal(so.getTotal());
        setSubtotal(so.getSubtotal());
        setBpolid(so.getBpolid());
        setBpId(so.getBpId());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuedays() {
        return duedays;
    }

    public void setDuedays(int duedays) {
        this.duedays = duedays;
    }

    public String getTermtype() {
        return termtype;
    }

    public void setTermtype(String termtype) {
        this.termtype = termtype;
    }

    public String getPaystatus() {
        return paystatus;
    }

    public void setPaystatus(String paystatus) {
        this.paystatus = paystatus;
    }

    public BigDecimal getPaidamt() {
        return paidamt;
    }

    public void setPaidamt(BigDecimal paidamt) {
        this.paidamt = paidamt;
    }

    public BigDecimal getDpamt() {
        return dpamt;
    }

    public void setDpamt(BigDecimal dpamt) {
        this.dpamt = dpamt;
    }

    public BigDecimal getBasetotal() {
        return basetotal;
    }

    public void setBasetotal(BigDecimal basetotal) {
        this.basetotal = basetotal;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getBasefistaxamt() {
        return basefistaxamt;
    }

    public void setBasefistaxamt(BigDecimal basefistaxamt) {
        this.basefistaxamt = basefistaxamt;
    }

    public BigDecimal getBasetaxamt() {
        return basetaxamt;
    }

    public void setBasetaxamt(BigDecimal basetaxamt) {
        this.basetaxamt = basetaxamt;
    }

    public BigDecimal getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(BigDecimal taxamt) {
        this.taxamt = taxamt;
    }

    public BigDecimal getBasediscamt() {
        return basediscamt;
    }

    public void setBasediscamt(BigDecimal basediscamt) {
        this.basediscamt = basediscamt;
    }

    public BigDecimal getDiscamt() {
        return discamt;
    }

    public void setDiscamt(BigDecimal discamt) {
        this.discamt = discamt;
    }

    public String getDiscexp() {
        return discexp;
    }

    public void setDiscexp(String discexp) {
        this.discexp = discexp;
    }

    public BigDecimal getBasesubtotal() {
        return basesubtotal;
    }

    public void setBasesubtotal(BigDecimal basesubtotal) {
        this.basesubtotal = basesubtotal;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public String getShipaddr() {
        return shipaddr;
    }

    public void setShipaddr(String shipaddr) {
        this.shipaddr = shipaddr;
    }

    public String getBilladdr() {
        return billaddr;
    }

    public void setBilladdr(String billaddr) {
        this.billaddr = billaddr;
    }

    public boolean isTaxinc() {
        return taxinc;
    }

    public void setTaxinc(boolean taxinc) {
        this.taxinc = taxinc;
    }

    public boolean isTaxed() {
        return taxed;
    }

    public void setTaxed(boolean taxed) {
        this.taxed = taxed;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getBpolid() {
        return bpolid;
    }

    public void setBpolid(String bpolid) {
        this.bpolid = bpolid;
    }

    public String getTrxdate() {
        return trxdate;
    }

    public void setTrxdate(String trxdate) {
        this.trxdate = trxdate;
    }

    public String getTrxno() {
        return trxno;
    }

    public void setTrxno(String trxno) {
        this.trxno = trxno;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(boolean isUploaded) {
        this.isUploaded = isUploaded;
    }

    public long getTrxdateLong() {
        return trxdateLong;
    }

    public void setTrxdateLong(long trxdateLong) {
        this.trxdateLong = trxdateLong;
    }

    public Bp getBpId() {
        return bpId;
    }

    public void setBpId(Bp bpId) {
        this.bpId = bpId;
    }

    public String getTrxCoordinate() {
        return trxCoordinate;
    }

    public void setTrxCoordinate(String trxCoordinate) {
        this.trxCoordinate = trxCoordinate;
    }
}