package com.bits.bee.so.view.listener;

import com.bits.bee.so.bl.db.model.Sod;

/**
 * Created by basofi on 10/06/16.
 */
public interface NewSoItemAddItemDialogCallback {
    void onNewSodSet(Sod sod);
    void onSodDelete(Sod sod);
}