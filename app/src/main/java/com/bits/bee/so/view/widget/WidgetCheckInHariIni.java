package com.bits.bee.so.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.view.abstraction.WidgetAbstract;

/**
 * Created by basofi on 15/07/16.
 */
public class WidgetCheckInHariIni extends WidgetAbstract {

    private View mWidgetCheckInHariIni;
    private TextView mTvWidgetCheckInHariIni;

    public WidgetCheckInHariIni(Context context) {
        super(context);
        initViews();
    }

    public WidgetCheckInHariIni(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    private void initViews(){
        mWidgetCheckInHariIni = LayoutInflater.from(getContext())
                .inflate(R.layout.widget_checkinhariini, this, false);
        mTvWidgetCheckInHariIni = (TextView) mWidgetCheckInHariIni
                .findViewById(R.id.checkinhariini_tvCheckInHariIni);

        setContent(mWidgetCheckInHariIni);
    }

    @Override
    public void render() {
        loadData();
    }

    @Override
    protected void loadData() {
        mTvWidgetCheckInHariIni.setText("10");
    }
}