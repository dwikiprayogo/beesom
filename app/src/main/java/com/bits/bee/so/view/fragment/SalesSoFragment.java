package com.bits.bee.so.view.fragment;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.BpDao;
import com.bits.bee.so.bl.db.model.Bp;
import com.bits.bee.so.bl.trans.SaleSoTrans;
import com.bits.bee.so.view.activity.NewSoItemActivity;
import com.bits.bee.so.view.dialog.BDialog;

import java.util.ArrayList;
import java.util.List;

public class SalesSoFragment extends Fragment implements View.OnClickListener,
        OnItemClickListener {

    public static final String BUNDLEKEY_SOTRANS = "BUNDLEKEY_SOTRANS";

    public static final int MODE_SO = 0;
    public static final int MODE_SALES = 1;

    private int mMode = 0;

    private AutoCompleteTextView mActvNamaCust;
    private EditText mEtJangka;
    private TextView mTvDelNamaCust;

    private TextInputLayout mTilActvNamaCust;
    private TextInputLayout mTilEtJangka;

    private Spinner mSpTermin;

    private Button mBtnNext;

    private ImageView mIvJangka;

    private List<Bp> mBpList = new ArrayList<>();
    private String[] mSpTerminArray;

    private ArrayAdapter<String> mSpTerminAdapter;
    private ActvNamaCustAdapter mActvNamaCustAdapter;

    private Bp mBpSelected;

    private SaleSoTrans mSaleSoTrans;

    private boolean isTerminCash = true;

    private final static String KEY_PARAM_MODE = "KEY_PARAM_MODE";

    public static final int KEY_REQCODE = 1;

    public SalesSoFragment() {
    }

    public static SalesSoFragment newInstance(int mode) {
        Bundle args = new Bundle();
        args.putInt(KEY_PARAM_MODE, mode);

        SalesSoFragment fragment = new SalesSoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMode = getArguments().getInt(KEY_PARAM_MODE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_salesso, container, false);
        initViews(view);
        initListeners();

        loadData();

        return view;
    }

    private void initViews(View view) {

        mActvNamaCust = (AutoCompleteTextView) view.findViewById(R.id.salesso_actvNamaCust);
        mEtJangka = (EditText) view.findViewById(R.id.salesso_etJangka);

        mTilEtJangka = (TextInputLayout) view.findViewById(R.id.salesso_tilActvJangka);
        mTilActvNamaCust = (TextInputLayout) view.findViewById(R.id.salesso_tilEtNamaCust);

        mSpTermin = (Spinner) view.findViewById(R.id.salesso_spTermin);

        mBtnNext = (Button) view.findViewById(R.id.salesso_btnNext);

        mTvDelNamaCust = (TextView) view.findViewById(R.id.salesso_tvDelNamaCust);

        mIvJangka = (ImageView) view.findViewById(R.id.salesso_ivJangka);

        unlockActvNamaCust();
    }

    private void initListeners() {
        mBtnNext.setOnClickListener(this);
        mTvDelNamaCust.setOnClickListener(this);

        mActvNamaCust.setOnItemClickListener(this);

        mActvNamaCust.setOnClickListener(this);
        mEtJangka.setOnClickListener(this);

        mSpTermin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //cash
                if (position == 0) {
                    isTerminCash = true;
                    mEtJangka.setText("");
                    mEtJangka.setVisibility(View.INVISIBLE);
                    mTilEtJangka.setVisibility(View.INVISIBLE);
                    mIvJangka.setVisibility(View.INVISIBLE);
                    //credit
                } else if (position == 1) {
                    isTerminCash = false;
                    mEtJangka.setVisibility(View.VISIBLE);
                    mTilEtJangka.setVisibility(View.VISIBLE);
                    mIvJangka.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadData() {

        if(mMode == MODE_SO) {
            mSaleSoTrans = new SaleSoTrans(SaleSoTrans.MODE_SO);
        }else{
            mSaleSoTrans = new SaleSoTrans(SaleSoTrans.MODE_SALE);
        }

        mSpTerminArray = getActivity().getResources().getStringArray(R.array.mainso_array_spTermin);

        mSpTerminAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, mSpTerminArray);

        mSpTermin.setPromptId(R.string.mainso_prompt_spTermin);
        mSpTermin.setAdapter(mSpTerminAdapter);
        mSpTermin.setSelection(0);

        try {
            mBpList = BpDao.getInstance().read();

            mActvNamaCustAdapter = new ActvNamaCustAdapter(mBpList);
            mActvNamaCust.setAdapter(mActvNamaCustAdapter);
            mActvNamaCust.setThreshold(3);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if(mMode == MODE_SALES){
            mSpTermin.setSelection(0);
            mSpTermin.setEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mBtnNext) {
            if (validate()) {
                String termin = (String) mSpTermin.getSelectedItem();
                try {
                    int jangka = Integer.parseInt(mEtJangka.getText().toString());
                    mSaleSoTrans.setDueDays(jangka);
                }catch(Exception e){
                    e.printStackTrace();
                }

                mSaleSoTrans.setBp(mBpSelected);
                mSaleSoTrans.setTermType(termin);

                Bundle bundle = new Bundle();
                bundle.putSerializable(BUNDLEKEY_SOTRANS, mSaleSoTrans);

                Intent intent = new Intent(getActivity(), NewSoItemActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, KEY_REQCODE);
            }
        } else if (v == mTvDelNamaCust) {
            unlockActvNamaCust();
        } else if (v == mEtJangka) {
            mTilEtJangka.setErrorEnabled(false);
        } else if (v == mActvNamaCust) {
            mTilActvNamaCust.setErrorEnabled(false);
        }
    }

    private boolean validate() {
        boolean canContinue = true;

        if(!isTerminCash) {
            if (TextUtils.isEmpty(mEtJangka.getText().toString())) {
                canContinue = false;
                mTilEtJangka.setErrorEnabled(true);
                mTilEtJangka.setError(getActivity().getString(R.string.cantbeempty));
            } else {
                mTilEtJangka.setErrorEnabled(false);
            }
        }

        if (TextUtils.isEmpty(mActvNamaCust.getText().toString())) {
            canContinue = false;
            mTilActvNamaCust.setErrorEnabled(true);
            mTilActvNamaCust.setError(getActivity().getString(R.string.cantbeempty));
        } else {
            if(null == mBpSelected){
                canContinue = false;
                mTilActvNamaCust.setError(getActivity().getString(R.string.newso_error_nobpselected));
                mTilActvNamaCust.setErrorEnabled(true);
            }else {
                mTilActvNamaCust.setErrorEnabled(false);
            }
        }

        return canContinue;
    }

    private void lockActvNamaCust() {
        mActvNamaCust.setEnabled(false);
        mEtJangka.requestFocus();
    }

    private void unlockActvNamaCust() {
        mActvNamaCust.setText("");
        mBpSelected = null;
        mActvNamaCust.setEnabled(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bp bp = (Bp) parent.getItemAtPosition(position);
        if(!TextUtils.isEmpty(bp.getOlId())) {
            mActvNamaCust.setText(bp.getNamaPemilik());
            mBpSelected = bp;

            if(mMode == MODE_SO) {
                //cash
                if (bp.getPurctermtype().equalsIgnoreCase("c")) {
                    mSpTermin.setSelection(0);
                }
                //credit
                else if (bp.getPurctermtype().equalsIgnoreCase("r")) {
                    mSpTermin.setSelection(1);
                    mEtJangka.setText("7");
                }
            }

            lockActvNamaCust();
        }else{
            BDialog.showErrorDialog(getActivity(), R.string.salessofragment_error_bpnotsynced);
            mActvNamaCust.setText("");
        }
    }

    private class ActvNamaCustAdapter extends BaseAdapter implements Filterable {
        private List<Bp> mBpList;
        private List<Bp> mBpSuggestionList = new ArrayList<>();
        private Filter filter = new ActvNamaCustAdapterFilter();

        public ActvNamaCustAdapter(List<Bp> daftCustList) {
            this.mBpList = daftCustList;
        }

        @Override
        public int getCount() {
            return mBpSuggestionList.size();
        }

        @Override
        public Object getItem(int position) {
            return mBpSuggestionList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Bp bp = mBpSuggestionList.get(position);
            View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_mainso_actvnamacust, null);
            } else {
                view = convertView;
            }

            TextView tvNamaPerusahaan =
                    (TextView) view.findViewById(R.id.mainso_actvnamacust_tvNamaPerusahaan);
            TextView tvNamaPemilik =
                    (TextView) view.findViewById(R.id.mainso_actvnamacust_tvNamaPemilik);

            tvNamaPerusahaan.setText(bp.getNamaPerusahaan());
            tvNamaPemilik.setText(bp.getNamaPemilik());

            return view;
        }


        @Override
        public Filter getFilter() {
            return filter;
        }


        private class ActvNamaCustAdapterFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                mBpSuggestionList.clear();

                if (mBpList != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                    for (int i = 0; i < mBpList.size(); i++) {
                        Bp bp = mBpList.get(i);
                        bp.getNamaPerusahaan().toLowerCase().contains(constraint);
                        if (bp.getNamaPerusahaan().toLowerCase().contains(constraint)
                                ||
                                bp.getNamaPemilik().toLowerCase().contains(constraint)) { // Compare item in original list if it contains constraints.
                            mBpSuggestionList.add(bp); // If TRUE add item in Suggestions.
                        }
                    }
                }
                FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
                results.values = mBpSuggestionList;
                results.count = mBpSuggestionList.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == KEY_REQCODE){
            if(resultCode == NewSoItemActivity.RESULTCODE_FINISH){
                getActivity().finish();
            }
        }
    }
}