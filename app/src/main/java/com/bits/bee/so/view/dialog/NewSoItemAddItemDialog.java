package com.bits.bee.so.view.dialog;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.ItemDao;
import com.bits.bee.so.bl.db.dao.PriceDao;
import com.bits.bee.so.bl.db.dao.UnitDao;
import com.bits.bee.so.bl.db.model.Item;
import com.bits.bee.so.bl.db.model.Price;
import com.bits.bee.so.bl.db.model.Sod;
import com.bits.bee.so.bl.db.model.Unit;
import com.bits.bee.so.view.listener.NewSoItemAddItemDialogCallback;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 10/06/16.
 */
public class NewSoItemAddItemDialog extends MaterialDialog.Builder implements View.OnClickListener{

    private AutoCompleteTextView mActvNamaItem;
    private AutoCompleteTextView mActvNamaSubItem;
    private EditText mEtJumlah;
    private Spinner mSpUnit;

    private TextView mTvDelNamaItem;

    private TextInputLayout mTilActvNamaItem;
    private TextInputLayout mTilActvNamaSubItem;
    private TextInputLayout mTilEtJumlah;

    private NewSoItemAddItemDialogCallback callback;

    private ActvNamaItemAdapter mActvNamaItemAdapter;
    private ArrayAdapter<String> mSpUnitAdapter;

    private Item mItemSelected;
    private Item mSubItemSelected;

    private Sod mSod;

    private List<Item> mItemList = new ArrayList<>();
    private List<Unit> mUnitList = new ArrayList<>();
    private String[] mUnitListArray;

    public NewSoItemAddItemDialog(Context context) {
        super(context);

        mSod = new Sod();
        initViews();
        loadData();
        initListeners();
    }

    public MaterialDialog show(Sod sod) {
        title("Edit Item");
        loadSod(sod);
        return super.show();
    }

    public MaterialDialog show() {
        mSod = new Sod();
        title("Add New Item");
        return super.show();
    }

    private void loadSod(Sod sod) {
        this.mSod = sod;

        try {
            this.mItemSelected = ItemDao.getInstance().readByOlId(sod.getItemolid());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mEtJumlah.setText(sod.getQty().toPlainString());
        mActvNamaItem.setText(sod.getItemname());

        loadUnit();

        for(int i = 0 ; i<mUnitList.size() ; i++){
            Unit unit = mUnitList.get(i);
            if(unit.getOlId().equals(sod.getUnitolid())){
                mSpUnit.setSelection(i);
            }
        }

        lockActvNamaCust();
    }

    private void initViews() {
        customView(R.layout.dialog_newsoitemadditem, true);

        mActvNamaItem = (AutoCompleteTextView)customView.findViewById(R.id.newsoitemadditem_actvNamaItem);
        mActvNamaSubItem = (AutoCompleteTextView)customView.findViewById(R.id.newsoitemadditem_actvNamaSubItem);
        mEtJumlah = (EditText)customView.findViewById(R.id.newsoitemadditem_etJumlah);
        mSpUnit = (Spinner)customView.findViewById(R.id.newsoitemadditem_spUnit);

        mTvDelNamaItem = (TextView)customView.findViewById(R.id.newsoitemadditem_tvDelNamaItem);

        mTilActvNamaItem = (TextInputLayout)customView.findViewById(R.id.newsoitemadditem_tilAtcvNamaItem);
        mTilActvNamaSubItem = (TextInputLayout)customView.findViewById(R.id.newsoitemadditem_tilAtcvNamaSubItem);
        mTilEtJumlah = (TextInputLayout)customView.findViewById(R.id.newsoitemadditem_tilEtJumlah);

        positiveText(R.string.ok);
        negativeText(R.string.cancel);

        autoDismiss(false);
    }

    private void loadUnit(){
        if(null != mItemSelected){
            try {
                mUnitList = UnitDao.getInstance().readByItem(mItemSelected);
                mUnitListArray = new String[mUnitList.size()];
                for (int i = 0 ; i<mUnitList.size() ; i++){
                    mUnitListArray[i] = mUnitList.get(i).getUnit();
                }

                mSpUnitAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_dropdown_item, mUnitListArray);
                mSpUnit.setPrompt("Pilih Unit");
                mSpUnit.setAdapter(mSpUnitAdapter);
                mSpUnit.setSelection(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadData(){
        try {
            mItemList = ItemDao.getInstance().read();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mActvNamaItemAdapter = new ActvNamaItemAdapter(mItemList);

        mActvNamaItem.setAdapter(mActvNamaItemAdapter);
        mActvNamaItem.setThreshold(3);

        mActvNamaSubItem.setAdapter(mActvNamaItemAdapter);
        mActvNamaItem.setThreshold(3);
    }

    private void initListeners() {
        callback(mBtnCallback);
        mActvNamaItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item item = (Item) parent.getItemAtPosition(position);
                mActvNamaItem.setText(item.getName());
                mItemSelected = item;
                mEtJumlah.setText("1");
                loadUnit();
                lockActvNamaCust();
            }
        });
        mActvNamaSubItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item subItem = (Item) parent.getItemAtPosition(position);
                mActvNamaSubItem.setText(subItem.getName());
                mSubItemSelected = subItem;
            }
        });
        mTvDelNamaItem.setOnClickListener(this);
    }

    public void setNewSoItemAddItemDialogCallback(NewSoItemAddItemDialogCallback callback) {
        this.callback = callback;
    }

    private boolean validate() {
        boolean canContinue = true;

        if (TextUtils.isEmpty(mEtJumlah.getText().toString())) {
            canContinue = false;
            mTilEtJumlah.setErrorEnabled(true);
            mTilEtJumlah.setError(context.getString(R.string.cantbeempty));
        }

        if (TextUtils.isEmpty(mActvNamaItem.getText().toString())) {
            canContinue = false;
            mTilActvNamaItem.setErrorEnabled(true);
            mTilActvNamaItem.setError(context.getString(R.string.cantbeempty));
        }

        if(null == mItemSelected){
            canContinue = false;
            mTilActvNamaItem.setErrorEnabled(true);
            mTilActvNamaItem.setError(
                    context.getString(R.string.newsoitemadditem_error_itemnotselected));
        }

        return canContinue;
    }
    private void lockActvNamaCust(){
        mActvNamaItem.setEnabled(false);
        mEtJumlah.requestFocus();
    }

    private void unlockActvNamaCust(){
        mActvNamaItem.setText("");
        mItemSelected = null;
        mActvNamaItem.setEnabled(true);
    }


    private MaterialDialog.ButtonCallback mBtnCallback = new MaterialDialog.ButtonCallback() {
        @Override
        public void onPositive(MaterialDialog dialog) {
            if (validate()) {
                if (null != callback) {
                    BigDecimal jumlah = new BigDecimal(mEtJumlah.getText().toString());
                    Item subItem = mItemSelected;
                    Price price = new Price();

                    try {
                        Unit unit = mUnitList.get(mSpUnit.getSelectedItemPosition());
                        mSod.setUnit(unit);
                        mSod.setUnitolid(unit.getOlId());
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try {
                        price = PriceDao.getInstance().readByItem(mItemSelected);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mSod.setItemolid(mItemSelected.getOlId());
                    mSod.setQty(jumlah);
                    mSod.setItemname(mItemSelected.getName());
                    mSod.setSubtotal(price.getPrice1());
                    mSod.setSubitemolid(subItem.getOlId());

                    callback.onNewSodSet(mSod);

                    dialog.dismiss();
                }
            }

        }

        @Override
        public void onNegative(MaterialDialog dialog) {
            dialog.dismiss();
        }
    };

    @Override
    public void onClick(View v) {
        if(v == mTvDelNamaItem){
            unlockActvNamaCust();
        }
    }


    private class ActvNamaItemAdapter extends BaseAdapter implements Filterable {
        private List<Item> mItemList;
        private List<Item> mItemSuggestionList = new ArrayList<>();
        private Filter filter = new ActvNamaItemAdapterFilter();

        public ActvNamaItemAdapter(List<Item> itemStringList) {
            this.mItemList = itemStringList;
        }

        @Override
        public int getCount() {
            return mItemSuggestionList.size();
        }

        @Override
        public Object getItem(int position) {
            return mItemSuggestionList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Item item = mItemSuggestionList.get(position);
            View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_dialog_newsoitemadditem_actvnamaitem, null);
            } else {
                view = convertView;
            }

            TextView tvNamaItem =
                    (TextView) view.findViewById(R.id.dialog_newsoitemadditem_tvNamaItem);

            tvNamaItem.setText(item.getName());
            return view;
        }

        @Override
        public Filter getFilter() {
            return filter;
        }

        private class ActvNamaItemAdapterFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                mItemSuggestionList.clear();

                if (mItemList != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                    for (int i = 0; i < mItemList.size(); i++) {
                        Item item = mItemList.get(i);
                        if (item.getName().toLowerCase().contains(constraint)) { // Compare item in original list if it contains constraints.
                            mItemSuggestionList.add(item); // If TRUE add item in Suggestions.
                        }
                    }
                }
                FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
                results.values = mItemSuggestionList;
                results.count = mItemSuggestionList.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        }
    }
}