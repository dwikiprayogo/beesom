package com.bits.bee.so.view.widget;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.SaleDao;
import com.bits.bee.so.bl.db.dao.SoDao;
import com.bits.bee.so.util.BUtil;
import com.bits.bee.so.view.abstraction.WidgetAbstract;
import com.bits.bee.so.view.activity.OrderSalesActivity;

/**
 * Created by basofi on 15/07/16.
 */
public class WidgetOrderSalesHariIni extends WidgetAbstract implements View.OnClickListener{

    private View mOrderSalesHariIni;
    private TextView mTvOrderHariIni;
    private TextView mTvSalesHariIni;
    private LinearLayout mLlSo;
    private LinearLayout mLlSales;

    public WidgetOrderSalesHariIni(Context context) {
        super(context);
        initViews();
        initListeners();
    }

    public WidgetOrderSalesHariIni(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
        initListeners();
    }

    private void initViews(){
        mOrderSalesHariIni = LayoutInflater.from(getContext()).inflate(
                R.layout.widget_ordersaleshariini, this, false);

        mTvOrderHariIni = (TextView)
                mOrderSalesHariIni.findViewById(R.id.ordersaleshariini_tvOrderHariIni);
        mTvSalesHariIni = (TextView)
                mOrderSalesHariIni.findViewById(R.id.ordersaleshariini_tvSalesHariIni);
        mLlSales = (LinearLayout)mOrderSalesHariIni.findViewById(R.id.ordersaleshariini_llSales);
        mLlSo = (LinearLayout)mOrderSalesHariIni.findViewById(R.id.ordersaleshariini_llSo);

        setContent(mOrderSalesHariIni);
    }

    private void initListeners(){
        mLlSales.setOnClickListener(this);
        mLlSo.setOnClickListener(this);
    }

    @Override
    public void render() {
        loadData();
    }

    @Override
    protected void loadData() {
        long soToday = 0;
        long salesToday = 0;

        try {
            soToday = SoDao.getInstance().countSoByDate(
                    BUtil.getStartOfDay().getTimeInMillis(), BUtil.getEndOfDay().getTimeInMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            salesToday = SaleDao.getInstance().countSaleByDate(
                    BUtil.getStartOfDay().getTimeInMillis(), BUtil.getEndOfDay().getTimeInMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mTvOrderHariIni.setText(""+soToday);
        mTvSalesHariIni.setText(""+salesToday);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        if(v == mLlSales){
            bundle.putInt(OrderSalesActivity.MODE_KEY, OrderSalesActivity.MODE_SALES);
        }else if(v == mLlSo){
            bundle.putInt(OrderSalesActivity.MODE_KEY, OrderSalesActivity.MODE_SO);
        }

        Intent intent = new Intent(getContext(), OrderSalesActivity.class);
        intent.putExtras(bundle);

        getContext().startActivity(intent);
    }
}