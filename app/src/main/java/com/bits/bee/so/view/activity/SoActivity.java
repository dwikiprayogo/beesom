package com.bits.bee.so.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.bits.bee.so.R;
import com.bits.bee.so.view.fragment.SalesSoFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SoActivity extends AppCompatActivity {

    @BindView(R.id.so_toolbar) Toolbar mToolbar;

    private SalesSoFragment mSalesSoFragment = SalesSoFragment.newInstance(SalesSoFragment.MODE_SO);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_so);
        ButterKnife.bind(this);

        initViews();
    }

    private void initViews(){
        mToolbar = (Toolbar)findViewById(R.id.so_toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getFragmentManager().beginTransaction().replace(R.id.so_flContent,
                mSalesSoFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
