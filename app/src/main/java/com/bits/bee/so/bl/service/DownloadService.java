package com.bits.bee.so.bl.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.BpDao;
import com.bits.bee.so.bl.db.dao.CityDao;
import com.bits.bee.so.bl.db.dao.ItemDao;
import com.bits.bee.so.bl.db.dao.PriceDao;
import com.bits.bee.so.bl.db.dao.RegDao;
import com.bits.bee.so.bl.db.dao.StockDao;
import com.bits.bee.so.bl.db.dao.UnitDao;
import com.bits.bee.so.bl.db.model.Bp;
import com.bits.bee.so.bl.db.model.City;
import com.bits.bee.so.bl.db.model.Item;
import com.bits.bee.so.bl.db.model.Price;
import com.bits.bee.so.bl.db.model.Reg;
import com.bits.bee.so.bl.db.model.Stock;
import com.bits.bee.so.bl.db.model.Unit;
import com.bits.bee.so.bl.db.model.Usr;
import com.bits.bee.so.bl.net.api.BApi;
import com.bits.bee.so.bl.net.api.BApiHelper;
import com.bits.bee.so.bl.net.model.BpGet;
import com.bits.bee.so.bl.net.model.CityGet;
import com.bits.bee.so.bl.net.model.ItemGet;
import com.bits.bee.so.bl.net.model.PriceGet;
import com.bits.bee.so.bl.net.model.RegGet;
import com.bits.bee.so.bl.net.model.StockGet;
import com.bits.bee.so.bl.net.model.UnitGet;
import com.bits.bee.so.service.ProfileService;
import com.bits.bee.so.view.dialog.BDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by basofi on 30/06/16.
 */
public class DownloadService {
    private Context mContext;
    private ProgressDialog mPd;

    private BApi mBApi;

    private BpGet mBpGet;
    private ItemGet mItemGet;
    private PriceGet mPriceGet;
    private StockGet mStockGet;
    private UnitGet mUnitGet;
    private CityGet mCityGet;
    private RegGet mRegGet;

    private Call<BpGet> mBpGetCall;
    private Call<ItemGet> mItemGetCall;
    private Call<PriceGet> mPriceGetCall;
    private Call<StockGet> mStockGetCall;
    private Call<UnitGet> mUnitGetCall;
    private Call<CityGet> mCityGetCall;
    private Call<RegGet> mRegGetCall;

    public DownloadService(Context context) {
        this.mContext = context;

        Usr usr = ProfileService.getInstance().getActiveUsr();
        mBApi = BApiHelper.build(mContext, "https://app.beecloud.id", usr.getApikey());

        mPd = new ProgressDialog(mContext);
        mPd.setTitle(mContext.getString(R.string.downloadservice_title_mpd));
        mPd.setMessage(mContext.getString(R.string.downloadservice_msg_mpd));
        mPd.setCancelable(false);
        mPd.setButton(DialogInterface.BUTTON_NEGATIVE,
                mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cancelDownload();
                        mPd.dismiss();
                    }
                });
    }

    private void saveToDb() {
        saveCityToDb();
        saveBpToDb();
        saveItemToDb();
        savePriceToDb();
        saveStockToDb();
        saveUnitToDb();
        saveRegToDb();

        mPd.dismiss();
        BDialog.showInfoDialog(mContext, "Information", "Importing data completed");
    }

    private void cancelDownload() {
        try {
            mCityGetCall.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            mItemGetCall.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mUnitGetCall.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mStockGetCall.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mPriceGetCall.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mBpGetCall.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mPd.dismiss();
        BDialog.showInfoDialog(mContext, "Information", "Importing data cancelled");
    }

    public void download() {
        mPd.show();
//        downloadReg();
        downloadCity();
//        downloadBp();
    }

    private void downloadCity() {
        mCityGetCall = mBApi.getCity();
        mCityGetCall.enqueue(new Callback<CityGet>() {
            @Override
            public void onResponse(Call<CityGet> call, Response<CityGet> response) {
                if (response.isSuccessful()) {
                    mCityGet = response.body();
                    downloadBp();
                } else {
                    cancelDownload();
                }
            }

            @Override
            public void onFailure(Call<CityGet> call, Throwable t) {
                if (call.isCanceled()) {

                } else {
                    BDialog.showErrorDialog(mContext, t.getMessage());
                }
            }
        });
    }

    private void downloadBp() {
        mBpGetCall = mBApi.getBp();
        mBpGetCall.enqueue(new Callback<BpGet>() {
            @Override
            public void onResponse(Call<BpGet> call, Response<BpGet> response) {
                if (response.isSuccessful()) {
                    mBpGet = response.body();
                    downloadItem();
                } else {
                    cancelDownload();
                }
            }

            @Override
            public void onFailure(Call<BpGet> call, Throwable t) {
                if (call.isCanceled()) {

                } else {
                    BDialog.showErrorDialog(mContext, t.getMessage());
                }
            }
        });
    }

    private void downloadItem() {
        mItemGetCall = mBApi.getItem();
        mItemGetCall.enqueue(new Callback<ItemGet>() {
            @Override
            public void onResponse(Call<ItemGet> call, Response<ItemGet> response) {
                if (response.isSuccessful()) {
                    mItemGet = response.body();
                    downloadPrice();
                } else {
                    cancelDownload();
                }
            }

            @Override
            public void onFailure(Call<ItemGet> call, Throwable t) {
                if (call.isCanceled()) {

                } else {
                    BDialog.showErrorDialog(mContext, t.getMessage());
                }
            }
        });
    }

    private void downloadPrice() {
        mPriceGetCall = mBApi.getPrice();
        mPriceGetCall.enqueue(new Callback<PriceGet>() {
            @Override
            public void onResponse(Call<PriceGet> call, Response<PriceGet> response) {
                if (response.isSuccessful()) {
                    mPriceGet = response.body();
                    downloadStock();
                } else {
                    cancelDownload();
                }
            }

            @Override
            public void onFailure(Call<PriceGet> call, Throwable t) {
                if (call.isCanceled()) {

                } else {
                    BDialog.showErrorDialog(mContext, t.getMessage());
                }
            }
        });
    }

    private void downloadStock() {
        mStockGetCall = mBApi.getStock();
        mStockGetCall.enqueue(new Callback<StockGet>() {
            @Override
            public void onResponse(Call<StockGet> call, Response<StockGet> response) {
                if (response.isSuccessful()) {
                    mStockGet = response.body();
                    downloadUnit();
                } else {
                    cancelDownload();
                }
            }

            @Override
            public void onFailure(Call<StockGet> call, Throwable t) {
                if (call.isCanceled()) {

                } else {
                    BDialog.showErrorDialog(mContext, t.getMessage());
                }
            }
        });
    }

    private void downloadUnit() {
        mUnitGetCall = mBApi.getUnit();
        mUnitGetCall.enqueue(new Callback<UnitGet>() {
            @Override
            public void onResponse(Call<UnitGet> call, Response<UnitGet> response) {
                if (response.isSuccessful()) {
                    mUnitGet = response.body();
                    downloadReg();
                } else {

                }
            }

            @Override
            public void onFailure(Call<UnitGet> call, Throwable t) {
                if (call.isCanceled()) {

                } else {
                    BDialog.showErrorDialog(mContext, t.getMessage());
                }
            }
        });
    }

    private void downloadReg(){
        mRegGetCall = mBApi.getReg();
        mRegGetCall.enqueue(new Callback<RegGet>() {
            @Override
            public void onResponse(Call<RegGet> call, Response<RegGet> response) {
                if(response.isSuccessful()){
                    mRegGet = response.body();
                    saveToDb();
                } else{

                }
            }

            @Override
            public void onFailure(Call<RegGet> call, Throwable t) {
                if(call.isCanceled()){

                }else{
                    BDialog.showErrorDialog(mContext, t.getMessage());
                }
            }
        });
    }

    private void saveCityToDb() {
        try {
            CityDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < mCityGet.getData().size(); i++) {
            CityGet.Datum cityGetDatum = mCityGet.getData().get(i);
            City city = new City(cityGetDatum);

            try {
                CityDao.getInstance().createOrUpdate(city);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void saveBpToDb() {
        try {
            BpDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < mBpGet.getData().size(); i++) {
            BpGet.Datum bpGetDatum = mBpGet.getData().get(i);
            Bp bp = new Bp(bpGetDatum);

            try {
                BpDao.getInstance().createOrUpdate(bp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void saveItemToDb() {
        try {
            ItemDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < mItemGet.getData().size(); i++) {
            ItemGet.Datum itemGetDatum = mItemGet.getData().get(i);
            Item item = new Item(itemGetDatum);

            try {
                ItemDao.getInstance().createOrUpdate(item);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void savePriceToDb() {
        try {
            PriceDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < mPriceGet.getData().size(); i++) {
            try {
                Price price = new Price(mPriceGet.getData().get(i));
                PriceDao.getInstance().createOrUpdate(price);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void saveStockToDb() {
        try {
            StockDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < mStockGet.getData().size(); i++) {
            StockGet.Datum stockGetDatum = mStockGet.getData().get(i);
            Stock stock = new Stock(stockGetDatum);

            try {
                StockDao.getInstance().createOrUpdate(stock);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void saveUnitToDb() {
        try {
            UnitDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < mUnitGet.getData().size(); i++) {
            UnitGet.Datum unitGetDatum = mUnitGet.getData().get(i);

            Unit unit = new Unit(unitGetDatum);
            try {
                UnitDao.getInstance().createOrUpdate(unit);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void saveRegToDb() {
        try {
            RegDao.getInstance().deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        RegGet.Data regGetData = mRegGet.getData();
        Reg reg = new Reg(regGetData);
        try {
            RegDao.getInstance().createOrUpdate(reg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
