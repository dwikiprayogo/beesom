package com.bits.bee.so.bl.net.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 01/06/16.
 */
public class PriceGet {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The data
     */
    public List<Datum> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("item_id")
        @Expose
        private String itemId;
        @SerializedName("pricelvl_id")
        @Expose
        private String pricelvlId;
        @SerializedName("crc_id")
        @Expose
        private String crcId;
        @SerializedName("price1")
        @Expose
        private String price1;
        @SerializedName("disc1exp")
        @Expose
        private String disc1exp;
        @SerializedName("price2")
        @Expose
        private String price2;
        @SerializedName("disc2exp")
        @Expose
        private String disc2exp;
        @SerializedName("price3")
        @Expose
        private String price3;
        @SerializedName("disc3exp")
        @Expose
        private String disc3exp;
        @SerializedName("pricedate")
        @Expose
        private Object pricedate;
        @SerializedName("usr_id")
        @Expose
        private String usrId;
        @SerializedName("lastpurcprice")
        @Expose
        private String lastpurcprice;
        @SerializedName("lastavgcost")
        @Expose
        private String lastavgcost;

        /**
         * @return The itemId
         */
        public String getItemId() {
            return itemId;
        }

        /**
         * @param itemId The item_id
         */
        public void setItemId(String itemId) {
            this.itemId = itemId;
        }

        /**
         * @return The pricelvlId
         */
        public String getPricelvlId() {
            return pricelvlId;
        }

        /**
         * @param pricelvlId The pricelvl_id
         */
        public void setPricelvlId(String pricelvlId) {
            this.pricelvlId = pricelvlId;
        }

        /**
         * @return The crcId
         */
        public String getCrcId() {
            return crcId;
        }

        /**
         * @param crcId The crc_id
         */
        public void setCrcId(String crcId) {
            this.crcId = crcId;
        }

        /**
         * @return The price1
         */
        public String getPrice1() {
            return price1;
        }

        /**
         * @param price1 The price1
         */
        public void setPrice1(String price1) {
            this.price1 = price1;
        }

        /**
         * @return The disc1exp
         */
        public String getDisc1exp() {
            return disc1exp;
        }

        /**
         * @param disc1exp The disc1exp
         */
        public void setDisc1exp(String disc1exp) {
            this.disc1exp = disc1exp;
        }

        /**
         * @return The price2
         */
        public String getPrice2() {
            return price2;
        }

        /**
         * @param price2 The price2
         */
        public void setPrice2(String price2) {
            this.price2 = price2;
        }

        /**
         * @return The disc2exp
         */
        public String getDisc2exp() {
            return disc2exp;
        }

        /**
         * @param disc2exp The disc2exp
         */
        public void setDisc2exp(String disc2exp) {
            this.disc2exp = disc2exp;
        }

        /**
         * @return The price3
         */
        public String getPrice3() {
            return price3;
        }

        /**
         * @param price3 The price3
         */
        public void setPrice3(String price3) {
            this.price3 = price3;
        }

        /**
         * @return The disc3exp
         */
        public String getDisc3exp() {
            return disc3exp;
        }

        /**
         * @param disc3exp The disc3exp
         */
        public void setDisc3exp(String disc3exp) {
            this.disc3exp = disc3exp;
        }

        /**
         * @return The pricedate
         */
        public Object getPricedate() {
            return pricedate;
        }

        /**
         * @param pricedate The pricedate
         */
        public void setPricedate(Object pricedate) {
            this.pricedate = pricedate;
        }

        /**
         * @return The usrId
         */
        public String getUsrId() {
            return usrId;
        }

        /**
         * @param usrId The usr_id
         */
        public void setUsrId(String usrId) {
            this.usrId = usrId;
        }

        /**
         * @return The lastpurcprice
         */
        public String getLastpurcprice() {
            return lastpurcprice;
        }

        /**
         * @param lastpurcprice The lastpurcprice
         */
        public void setLastpurcprice(String lastpurcprice) {
            this.lastpurcprice = lastpurcprice;
        }

        /**
         * @return The lastavgcost
         */
        public String getLastavgcost() {
            return lastavgcost;
        }

        /**
         * @param lastavgcost The lastavgcost
         */
        public void setLastavgcost(String lastavgcost) {
            this.lastavgcost = lastavgcost;
        }

    }

}
