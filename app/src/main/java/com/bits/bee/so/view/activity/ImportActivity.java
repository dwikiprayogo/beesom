package com.bits.bee.so.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.service.DownloadService;
import com.bits.bee.so.util.BPrefs;
import com.bits.bee.so.util.BUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImportActivity extends AppCompatActivity{

    @BindView(R.id.import_ivDownload) 
    ImageView mIvDownload;

    @BindView(R.id.import_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.import_tvLastSynced)
    TextView mTvLastSynced;

    private Animation mIvDownloadAnimation;

    private DownloadService mDownloadService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import);
        ButterKnife.bind(this);
        initViews();
        loadData();

        mDownloadService = new DownloadService(this);
    }

    private void initViews() {
        mIvDownloadAnimation = AnimationUtils.loadAnimation(
                this, R.anim.mainimport_ivdownloadanim);
        mIvDownload.startAnimation(mIvDownloadAnimation);

        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void loadData(){
        String lastDownloadTime = BPrefs.getInstance().getLastDownloadTime();

        if(!TextUtils.isEmpty(lastDownloadTime)){
            mTvLastSynced.setVisibility(View.VISIBLE);

            String lastSynced = getString(R.string.importupload_lastsynced);
            lastSynced = String.format(lastSynced, lastDownloadTime);
            mTvLastSynced.setText(lastSynced);
        }else{
            mTvLastSynced.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.import_ivDownload)
    void doDownload(){
        mDownloadService.download();

        BPrefs.getInstance().setLastDownloadTime(BUtil.getCompleteCurrentDateAndTime());

        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}