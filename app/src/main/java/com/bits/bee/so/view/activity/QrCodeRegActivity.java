package com.bits.bee.so.view.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bits.bee.so.R;
import com.bits.bee.so.bl.service.model.RegQrCode;
import com.bits.bee.so.service.RegistrationService;
import com.bits.bee.so.view.dialog.BDialog;
import com.google.gson.Gson;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QrCodeRegActivity extends AppCompatActivity implements BarcodeCallback {

    @BindView(R.id.registration_cbScanner)
    CompoundBarcodeView mBarcodeScannerView;
    @BindView(R.id.registration_toolbar)
    Toolbar mToolbar;

    private ProgressDialog mPd;
    private boolean mIsAllowQrRead = true;

    private RegistrationService mRegistrationService = new RegistrationService();

    private RegQrCode mRegQrCode;

    private Gson mGson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcodereg);
        ButterKnife.bind(this);

        initView();
        initListener();
    }

    private void initView() {
        mPd = new ProgressDialog(this);
        mPd.setMessage(getResources().getString(R.string.qrcodereg_message_pd));
        mPd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mPd.setCancelable(false);

        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initListener() {
        mBarcodeScannerView.decodeContinuous(this);
    }

    private void decodeQr(String barcodeResult) {
        mRegQrCode = mGson.fromJson(barcodeResult, RegQrCode.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBarcodeScannerView.resume();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mBarcodeScannerView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void barcodeResult(BarcodeResult barcodeResult) {
        if (mIsAllowQrRead) {
            mBarcodeScannerView.pause();
            mIsAllowQrRead = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showMPd();
                }
            });

            decodeQr(barcodeResult.getText());
            try {
                mRegistrationService.register(mRegQrCode);
                dismissMPd();
                BDialog.showInfoDialog(QrCodeRegActivity.this,
                        "Info",
                        "Successfully registered",
                        new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onAny(MaterialDialog dialog) {
                                setResult(RESULT_OK);
                                finish();
//                                mBarcodeScannerView.resume();
                                super.onAny(dialog);
                            }
                        });
            } catch (Exception e) {
                dismissMPd();
                BDialog.showErrorDialog(QrCodeRegActivity.this,
                        e.getMessage(),
                        new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onAny(MaterialDialog dialog) {
                                mBarcodeScannerView.resume();
                                super.onAny(dialog);
                            }
                        });
                e.printStackTrace();
            }
        }
    }

    private void showMPd() {
        if (!mPd.isShowing()) {
            mPd.show();
        }
    }

    private void dismissMPd() {
        if (mPd.isShowing()) {
            mPd.dismiss();
        }
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> list) {

    }
}