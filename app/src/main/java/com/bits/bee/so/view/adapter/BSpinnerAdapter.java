package com.bits.bee.so.view.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by basofi on 23/06/16.
 */
public class BSpinnerAdapter extends ArrayAdapter<String>  {
    public BSpinnerAdapter(Context context, int resource) {
        super(context, resource);
    }

    public BSpinnerAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public BSpinnerAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
    }

    public BSpinnerAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public BSpinnerAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
    }

    public BSpinnerAdapter(Context context, int resource, int textViewResourceId, List objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public void setHint(String hint){
        add(hint);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = super.getView(position, convertView, parent);
        if (position == getCount()) {
            ((TextView)v.findViewById(android.R.id.text1)).setText("");
            ((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
        }

        return v;
    }

    @Override
    public int getCount() {
        return super.getCount()-1; // you dont display last item. It is used as hint.
    }
}
