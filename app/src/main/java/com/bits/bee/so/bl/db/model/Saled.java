package com.bits.bee.so.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by basofi on 13/06/16.
 */
@DatabaseTable(tableName = Saled.TBL_NAME)
public class Saled implements Serializable {
    public static final String TBL_NAME = "saled";

    public static final String ID = "id";
    public static final String SALE_ID = "sale_id";
    public static final String DNO = "dno";
    public static final String ITEM_OLID = "item_olid";
    public static final String ITEMNAME = "itemname";
    public static final String QTY = "qty";
    public static final String UNIT_OLID = "unit_olid";
    public static final String BASEPRICE = "baseprice";
    public static final String LISTPRICE = "listprice";
    public static final String DISCEXP = "discexp";
    public static final String DISCAMT = "discamt";
    public static final String TOTALDISCAMT = "totaldiscamt";
    public static final String TAXABLEAMT = "taxableamt";
    public static final String TAXAMT = "taxamt";
    public static final String TOTALTAXAMT = "totaltaxamt";
    public static final String BASETOTALTAXAMT = "basetotaltaxamt";
    public static final String BASEFISTOTALTAXAMT = "basefistotaltaxamt";
    public static final String SUBTOTAL = "subtotal";
    public static final String BASESUBTOTAL = "basesubtotal";
    public static final String TAX_CODE = "tax_code";
    public static final String SO_ID = "so_id";
    public static final String SO_DNO = "so_dno";
    public static final String WH_ID = "wh_id";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = SALE_ID, foreign = true, foreignAutoRefresh = true)
    private Sale saleid;
    @DatabaseField(columnName = DNO)
    private int dno;
    @DatabaseField(columnName = ITEM_OLID)
    private String itemolid;
    @DatabaseField(columnName = ITEMNAME)
    private String itemname;
    @DatabaseField(columnName = QTY)
    private BigDecimal qty = new BigDecimal(0);
    @DatabaseField(columnName = UNIT_OLID)
    private String unit_olid;
    @DatabaseField(columnName = BASEPRICE)
    private BigDecimal baseprice = new BigDecimal(0);
    @DatabaseField(columnName = LISTPRICE)
    private BigDecimal listprice = new BigDecimal(0);
    @DatabaseField(columnName = DISCEXP)
    private String discexp;
    @DatabaseField(columnName = DISCAMT)
    private BigDecimal discamt = new BigDecimal(0);
    @DatabaseField(columnName = TOTALDISCAMT)
    private BigDecimal totaldiscamt = new BigDecimal(0);
    @DatabaseField(columnName = TAXABLEAMT)
    private BigDecimal taxableamt = new BigDecimal(0);
    @DatabaseField(columnName = TAXAMT)
    private BigDecimal taxamt = new BigDecimal(0);
    @DatabaseField(columnName = TOTALTAXAMT)
    private BigDecimal totaltaxamt = new BigDecimal(0);
    @DatabaseField(columnName = BASETOTALTAXAMT)
    private BigDecimal basetotaltaxamt = new BigDecimal(0);
    @DatabaseField(columnName = BASEFISTOTALTAXAMT)
    private BigDecimal basefistotaltaxamt = new BigDecimal(0);
    @DatabaseField(columnName = SUBTOTAL)
    private BigDecimal subtotal = new BigDecimal(0);
    @DatabaseField(columnName = BASESUBTOTAL)
    private BigDecimal basesubtotal = new BigDecimal(0);
    @DatabaseField(columnName = TAX_CODE)
    private String tax_code;
    @DatabaseField(columnName = SO_ID, foreign = true, foreignAutoRefresh = true)
    private So so_id;
    @DatabaseField(columnName = SO_DNO)
    private int so_dno;
    @DatabaseField(columnName = WH_ID)
    private String whId;

    public int getSo_dno() {
        return so_dno;
    }

    public void setSo_dno(int so_dno) {
        this.so_dno = so_dno;
    }

    public So getSo_id() {
        return so_id;
    }

    public void setSo_id(So so_id) {
        this.so_id = so_id;
    }

    public String getTax_code() {
        return tax_code;
    }

    public void setTax_code(String tax_code) {
        this.tax_code = tax_code;
    }

    public BigDecimal getBasesubtotal() {
        return basesubtotal;
    }

    public void setBasesubtotal(BigDecimal basesubtotal) {
        this.basesubtotal = basesubtotal;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getBasefistotaltaxamt() {
        return basefistotaltaxamt;
    }

    public void setBasefistotaltaxamt(BigDecimal basefistotaltaxamt) {
        this.basefistotaltaxamt = basefistotaltaxamt;
    }

    public BigDecimal getBasetotaltaxamt() {
        return basetotaltaxamt;
    }

    public void setBasetotaltaxamt(BigDecimal basetotaltaxamt) {
        this.basetotaltaxamt = basetotaltaxamt;
    }

    public BigDecimal getTotaltaxamt() {
        return totaltaxamt;
    }

    public void setTotaltaxamt(BigDecimal totaltaxamt) {
        this.totaltaxamt = totaltaxamt;
    }

    public BigDecimal getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(BigDecimal taxamt) {
        this.taxamt = taxamt;
    }

    public BigDecimal getTaxableamt() {
        return taxableamt;
    }

    public void setTaxableamt(BigDecimal taxableamt) {
        this.taxableamt = taxableamt;
    }

    public BigDecimal getTotaldiscamt() {
        return totaldiscamt;
    }

    public void setTotaldiscamt(BigDecimal totaldiscamt) {
        this.totaldiscamt = totaldiscamt;
    }

    public BigDecimal getDiscamt() {
        return discamt;
    }

    public void setDiscamt(BigDecimal discamt) {
        this.discamt = discamt;
    }

    public String getDiscexp() {
        return discexp;
    }

    public void setDiscexp(String discexp) {
        this.discexp = discexp;
    }

    public BigDecimal getListprice() {
        return listprice;
    }

    public void setListprice(BigDecimal listprice) {
        this.listprice = listprice;
    }

    public BigDecimal getBaseprice() {
        return baseprice;
    }

    public void setBaseprice(BigDecimal baseprice) {
        this.baseprice = baseprice;
    }

    public String getUnit_olid() {
        return unit_olid;
    }

    public void setUnit_olid(String unit_olid) {
        this.unit_olid = unit_olid;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemolid() {
        return itemolid;
    }

    public void setItemolid(String itemolid) {
        this.itemolid = itemolid;
    }

    public int getDno() {
        return dno;
    }

    public void setDno(int dno) {
        this.dno = dno;
    }

    public Sale getSaleid() {
        return saleid;
    }

    public void setSaleid(Sale saleid) {
        this.saleid = saleid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWhId() {
        return whId;
    }

    public void setWhId(String whId) {
        this.whId = whId;
    }
}
