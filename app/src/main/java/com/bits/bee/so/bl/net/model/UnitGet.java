package com.bits.bee.so.bl.net.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 01/06/16.
 */
public class UnitGet {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The data
     */
    public List<Datum> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("item_id")
        @Expose
        private String itemId;
        @SerializedName("idx")
        @Expose
        private String idx;
        @SerializedName("unit")
        @Expose
        private String unit;
        @SerializedName("conv")
        @Expose
        private String conv;

        /**
         * @return The id
         */
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return The itemId
         */
        public String getItemId() {
            return itemId;
        }

        /**
         * @param itemId The item_id
         */
        public void setItemId(String itemId) {
            this.itemId = itemId;
        }

        /**
         * @return The idx
         */
        public String getIdx() {
            return idx;
        }

        /**
         * @param idx The idx
         */
        public void setIdx(String idx) {
            this.idx = idx;
        }

        /**
         * @return The unit
         */
        public String getUnit() {
            return unit;
        }

        /**
         * @param unit The unit
         */
        public void setUnit(String unit) {
            this.unit = unit;
        }

        /**
         * @return The conv
         */
        public String getConv() {
            return conv;
        }

        /**
         * @param conv The conv
         */
        public void setConv(String conv) {
            this.conv = conv;
        }
    }
}