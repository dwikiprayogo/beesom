package com.bits.bee.so.bl.db.model;

import com.bits.bee.so.bl.net.model.CityGet;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by basofi on 30/06/16.
 */
@DatabaseTable(tableName = City.TBL_NAME)
public class City implements Serializable {
    public static final String TBL_NAME = "city";

    public static final String ID = "id";
    public static final String CODE = "code";
    public static final String NAME = "name";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = CODE)
    private String code;
    @DatabaseField(columnName = NAME)
    private String name;

    public City(){

    }

    public City(CityGet.Datum cityGetDatum){
        setCode(cityGetDatum.getCode());
        setName(cityGetDatum.getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
