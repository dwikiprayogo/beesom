package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.Sale;
import com.bits.bee.so.bl.db.model.Saled;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.List;

/**
 * Created by basofi on 14/06/16.
 */
public class SaledDao implements DaoCrud<Saled, Integer> {

    private static SaledDao singleton;

    public static SaledDao getInstance() {
        if (singleton == null) {
            singleton = new SaledDao();
        }
        return singleton;
    }

    @Override
    public void create(Saled model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Saled model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Saled model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Saled model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<Saled> read() throws Exception {
        return getDao().queryForAll();
    }

    public List<Saled> readBySale(Sale sale) throws Exception{
        return getDao().queryForEq(Saled.SALE_ID, sale);
    }

    @Override
    public Dao<Saled, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Saled.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Saled.class);
    }
}
