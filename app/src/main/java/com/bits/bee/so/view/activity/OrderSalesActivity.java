package com.bits.bee.so.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.SaleDao;
import com.bits.bee.so.bl.db.dao.SoDao;
import com.bits.bee.so.bl.db.model.Sale;
import com.bits.bee.so.bl.db.model.So;
import com.bits.bee.so.util.BUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderSalesActivity extends AppCompatActivity {

    @BindView(R.id.ordersales_rvContent)
    RecyclerView mRvContent;

    @BindView(R.id.ordersales_toolbar)
    Toolbar mToolbar;

    public static final int MODE_SO = 1;
    public static final int MODE_SALES = 2;
    public static final String MODE_KEY = "modekey";

    private int mode;

    private RvContentAdapter mRvContentAdapter;

    private List<Sale> mSaleList = new ArrayList<>();
    private List<So> mSoList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordersales);

        ButterKnife.bind(this);

        initViews();
        loadData();
    }

    private void initViews() {
        mRvContentAdapter = new RvContentAdapter();

        mRvContent.setLayoutManager(new LinearLayoutManager(this));
        mRvContent.setHasFixedSize(true);
        mRvContent.setAdapter(mRvContentAdapter);

        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void loadData() {
        Bundle bundle = getIntent().getExtras();
        mode = bundle.getInt(MODE_KEY);

        if (mode == MODE_SO) {
            try {
                mSoList = SoDao.getInstance().read();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mRvContentAdapter.generate(mSoList);
            setTitle("Sales Order Today");
        } else if (mode == MODE_SALES) {
            try {
                mSaleList = SaleDao.getInstance().read();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mRvContentAdapter.generate(mSaleList);
            setTitle("Sales Today");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class RvContentAdapter extends RecyclerView.Adapter<RvContentAdapter.ViewHolder> {

        private List mList;

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.ordersales_rvContent_tvBpName)
            TextView tvBpName;

            @BindView(R.id.ordersales_rvContent_tvDelete)
            TextView tvDelete;

            @BindView(R.id.ordersales_rvContent_tvEdit)
            TextView tvEdit;

            @BindView(R.id.ordersales_rvContent_tvNo)
            TextView tvNo;

            @BindView(R.id.ordersales_rvContent_tvTime)
            TextView tvTime;

            @BindView(R.id.ordersales_rvContent_tvTotal)
            TextView tvTotal;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        public void generate(List list) {
            this.mList = list;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(OrderSalesActivity.this)
                    .inflate(R.layout.row_ordersales_rvcontent, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (mode == MODE_SALES) {
                Sale sale = (Sale) mList.get(position);
                String saleNo = String.format("Sales #%s", sale.getId());

                holder.tvBpName.setText(sale.getBpId().getNamaPerusahaan());
                holder.tvNo.setText(saleNo);
                holder.tvTime.setText(BUtil.getHHmm(sale.getTrxdateLong()));
                holder.tvTotal.setText(BUtil.addCurr(sale.getTotal().toPlainString()));
            } else if (mode == MODE_SO) {
                So so = (So) mList.get(position);
                String soNo = String.format("Sales Order #%s", so.getId());

                holder.tvBpName.setText(so.getBpId().getNamaPerusahaan());
                holder.tvNo.setText(soNo);
                holder.tvTime.setText(BUtil.getHHmm(so.getTrxdateLong()));
                holder.tvTotal.setText(BUtil.addCurr(so.getTotal().toPlainString()));
            }
        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

    }
}