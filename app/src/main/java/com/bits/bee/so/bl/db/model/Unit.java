package com.bits.bee.so.bl.db.model;

import com.bits.bee.so.bl.db.dao.ItemDao;
import com.bits.bee.so.bl.net.model.UnitGet;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by basofi on 25/05/16.
 */
@DatabaseTable(tableName = Unit.TBL_NAME)
public class Unit implements Serializable{
    public static final String TBL_NAME = "unit";

    public static final String ID = "id";
    public static final String OL_ID = "ol_id";
    public static final String ITEM_ID = "item_id";
    public static final String ITEM_OL_ID = "item_ol_id";
    public static final String UNIT = "unit";
    public static final String CONV = "conv";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = OL_ID)
    private String olId;
    @DatabaseField(columnName = ITEM_ID, foreign = true, foreignAutoRefresh = true)
    private Item item;
    @DatabaseField(columnName = ITEM_OL_ID)
    private String itemolid;
    @DatabaseField(columnName = UNIT)
    private String unit;
    @DatabaseField(columnName = CONV)
    private BigDecimal conv;

    public Unit(){

    }

    public Unit(UnitGet.Datum unitGetDatum){

        try {
            Item item = ItemDao.getInstance().readByOlId(unitGetDatum.getItemId());
            setItem(item);
            setItemolid(item.getOlId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        setOlId(unitGetDatum.getId());
        setConv(new BigDecimal(unitGetDatum.getConv()));
        setUnit(unitGetDatum.getUnit());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getConv() {
        return conv;
    }

    public void setConv(BigDecimal conv1) {
        this.conv = conv;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getOlId() {
        return olId;
    }

    public void setOlId(String olId) {
        this.olId = olId;
    }

    public String getItemolid() {
        return itemolid;
    }

    public void setItemolid(String itemolid) {
        this.itemolid = itemolid;
    }
}