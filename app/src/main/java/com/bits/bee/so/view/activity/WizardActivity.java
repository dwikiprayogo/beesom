package com.bits.bee.so.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bits.bee.so.R;

public class WizardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard);
    }
}
