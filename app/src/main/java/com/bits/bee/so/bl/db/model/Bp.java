package com.bits.bee.so.bl.db.model;

import com.bits.bee.so.bl.net.model.BpGet;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by basofi on 25/05/16.
 */

@DatabaseTable(tableName = Bp.TBL_NAME)
public class Bp implements Serializable{
    public static final String TBL_NAME = "bp";

    public static final String ID = "id";
    public static final String OL_ID = "ol_id";
    public static final String CODE = "code";
    public static final String NAMA_PERUSAHAAN = "nama_perusahaan";
    public static final String NAMA_PEMILIK = "nama_pemilik";
    public static final String HP = "hp";
    public static final String ALAMAT = "alamat";
    public static final String KOTA = "kota";
    public static final String GPSLOCATION = "gpslocation";
    public static final String PURCTERMTYPE = "purctermtype";
    public static final String ISUPLOADED = "isuploaded";
    public static final String CREATED_AT = "created_at";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = OL_ID)
    private String olId;
    @DatabaseField(columnName = CODE)
    private String code;
    @DatabaseField(columnName = NAMA_PERUSAHAAN)
    private String namaPerusahaan;
    @DatabaseField(columnName = NAMA_PEMILIK)
    private String namaPemilik;
    @DatabaseField(columnName = HP)
    private String hp;
    @DatabaseField(columnName = ALAMAT)
    private String alamat;
    @DatabaseField(columnName = KOTA)
    private String kota;
    @DatabaseField(columnName = GPSLOCATION)
    private String gpsLocation;
    @DatabaseField(columnName = PURCTERMTYPE)
    private String purctermtype;
    @DatabaseField(columnName = ISUPLOADED)
    private boolean isUploaded;
    @DatabaseField(columnName = CREATED_AT)
    private long createdAt;

    public Bp(){

    }

    public Bp(BpGet.Datum bpGetDatum){
        setOlId(bpGetDatum.getId());
        setCode(bpGetDatum.getCode());
        setNamaPemilik(bpGetDatum.getName());
        setNamaPerusahaan(bpGetDatum.getName());
        setGpsLocation(bpGetDatum.getCoordinate());
        setPurctermtype(bpGetDatum.getBpaccount().getPurctermtype());
        setIsUploaded(true);
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getNamaPemilik() {
        return namaPemilik;
    }

    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }

    public String getNamaPerusahaan() {
        return namaPerusahaan;
    }

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOlId() {
        return olId;
    }

    public void setOlId(String olId) {
        this.olId = olId;
    }

    public String getPurctermtype() {
        return purctermtype;
    }

    public void setPurctermtype(String purctermtype) {
        this.purctermtype = purctermtype;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(boolean isUploaded) {
        this.isUploaded = isUploaded;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}