package com.bits.bee.so.service;

import com.bits.bee.so.bl.db.dao.UsrDao;
import com.bits.bee.so.bl.db.model.Usr;
import com.bits.bee.so.bl.service.model.RegQrCode;

/**
 * Created by basofi on 27/06/16.
 */
public class RegistrationService {

    public void register(RegQrCode regQrCode) throws Exception{
        //save to db, check if record exist
        Usr usr = new Usr(regQrCode);
        UsrDao.getInstance().create(usr);
        ProfileService.getInstance().setActiveUsr(usr);

    }
}
