package com.bits.bee.so.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.SaleDao;
import com.bits.bee.so.util.BUtil;
import com.bits.bee.so.view.abstraction.WidgetAbstract;

import java.math.BigDecimal;

/**
 * Created by basofi on 15/07/16.
 */
public class WidgetTotalPenjualan extends WidgetAbstract {

    private View mTotalPenjualan;
    private TextView mTvTotal;

    public WidgetTotalPenjualan(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public WidgetTotalPenjualan(Context context) {
        super(context);
        initViews();
    }

    private void initViews(){
        mTotalPenjualan = LayoutInflater.from(getContext())
                .inflate(R.layout.widget_totalpenjualan, this, false);
        mTvTotal = (TextView) mTotalPenjualan.findViewById(R.id.totalpenjualan_tvTotal);

        setContent(mTotalPenjualan);
    }

    @Override
    public void render() {
        loadData();
    }

    @Override
    protected void loadData(){
        BigDecimal bigDecimal = new BigDecimal(0);
        try {
            bigDecimal = SaleDao.getInstance().sumTotalByDate(
                    BUtil.getStartOfDay().getTimeInMillis(),
                    BUtil.getEndOfDay().getTimeInMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mTvTotal.setText(BUtil.addCurr(bigDecimal.toPlainString()));
    }
}