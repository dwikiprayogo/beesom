package com.bits.bee.so.view.activity;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.BpDao;
import com.bits.bee.so.bl.db.dao.CityDao;
import com.bits.bee.so.bl.db.model.Bp;
import com.bits.bee.so.bl.db.model.City;
import com.bits.bee.so.util.BUtil;
import com.bits.bee.so.view.dialog.BDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import ru.solodovnikov.rxlocationmanager.LocationRequestBuilder;
import ru.solodovnikov.rxlocationmanager.LocationTime;
import rx.Subscriber;

public class BpActivity extends AppCompatActivity implements OnItemClickListener, LocationListener {

    @BindView(R.id.bp_etNamaUsaha)
    EditText mEtNamaUsaha;
    @BindView(R.id.bp_etNamaPemilik)
    EditText mEtNamaPemilik;
    @BindView(R.id.bp_etAlamat)
    EditText mEtAlamat;

    @BindView(R.id.bp_actvKota)
    AutoCompleteTextView mActvKota;
    @BindView(R.id.bp_tvDelKota)
    TextView mTvDelKota;

    @BindView(R.id.bp_etNoHp)
    EditText mEtNoHp;
    @BindView(R.id.bp_etLaLo)
    EditText mEtLaLo;

    @BindView(R.id.bp_tilEtNamaUsaha)
    TextInputLayout mTilEtNamaUsaha;
    @BindView(R.id.bp_tilEtNamaPemilik)
    TextInputLayout mTilEtNamaPemilik;
    @BindView(R.id.bp_tilEtAlamat)
    TextInputLayout mTilEtAlamat;
    @BindView(R.id.bp_tilEtKota)
    TextInputLayout mTilActvKota;
    @BindView(R.id.bp_tilEtNoHp)
    TextInputLayout mTilEtNoHp;
    @BindView(R.id.bp_tilEtLaLo)
    TextInputLayout mTilEtLaLo;

    @BindView(R.id.bp_toolbar)
    Toolbar mToolbar;

    private double mLat = 0;
    private double mLong = 0;

    private City mCitySelected;
    private ActvKotaAdapter mActvKotaAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bp);
        ButterKnife.bind(this);

        initViews();
        loadData();
        initListeners();
    }

    private void initViews() {
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        unlockActvKota();
    }

    private void getCoordinate() {
        BUtil.gpsCheck(this);
        new LocationRequestBuilder(this)
                .addRequestLocation(LocationManager.NETWORK_PROVIDER, new LocationTime(10, TimeUnit.SECONDS))
                .addRequestLocation(LocationManager.GPS_PROVIDER, new LocationTime(10, TimeUnit.SECONDS))
                .setDefaultLocation(new Location(LocationManager.PASSIVE_PROVIDER))
                .create().subscribe(new Subscriber<Location>() {
            @Override
            public void onCompleted() {
                mEtLaLo.setText(BUtil.genCoordinate(mLat, mLong));
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Location location) {
                mLat = location.getLatitude();
                mLong = location.getLongitude();
            }
        });
    }

    private void loadData() {

        try {
            List<City> cityList = CityDao.getInstance().read();

            mActvKotaAdapter = new ActvKotaAdapter(cityList);
            mActvKota.setAdapter(mActvKotaAdapter);
            mActvKota.setThreshold(3);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initListeners() {
        mActvKota.setOnItemClickListener(this);
    }

    private void lockActvKota() {
        mActvKota.setEnabled(false);
    }

    private void unlockActvKota() {
        mActvKota.setText("");
        mCitySelected = null;
        mActvKota.setEnabled(true);
    }

    public void save() {
        if (validate()) {
            String alamat = mEtAlamat.getText().toString();
            String kota = mCitySelected.getCode();
            String lalo = mEtLaLo.getText().toString();
            String namaPemilik = mEtNamaPemilik.getText().toString();
            String namaUsaha = mEtNamaUsaha.getText().toString();
            String noHp = mEtNoHp.getText().toString();

            Bp bp = new Bp();
            bp.setGpsLocation(lalo);
            bp.setKota(kota);
            bp.setAlamat(alamat);
            bp.setHp(noHp);
            bp.setNamaPemilik(namaPemilik);
            bp.setNamaPerusahaan(namaUsaha);
            bp.setPurctermtype("C");
            bp.setGpsLocation(BUtil.genCoordinate(mLat, mLong));

            try {
                BpDao.getInstance().create(bp);
                BDialog.showCustomChoiceDialog(this,
                        "Data Saved",
                        "Data saved successfully",
                        "OK",
                        "Add New",
                        new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                finish();
                                super.onPositive(dialog);
                            }


                            @Override
                            public void onNeutral(MaterialDialog dialog) {
                                cleanUp();
                                super.onNeutral(dialog);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void cleanUp() {
        mEtNamaUsaha.setText("");
        mEtNamaPemilik.setText("");
        mEtAlamat.setText("");
        mActvKota.setText("");
        mEtNoHp.setText("");
        mEtLaLo.setText("");
    }

    private boolean validate() {
        boolean canContinue = true;

        if (TextUtils.isEmpty(mEtAlamat.getText().toString())) {
            canContinue = false;
            mTilEtAlamat.setErrorEnabled(true);
            mTilEtAlamat.setError("Alamat harus diisi");
        }

        if (TextUtils.isEmpty(mEtNoHp.getText().toString())) {
            canContinue = false;
            mTilEtNoHp.setErrorEnabled(true);
            mTilEtNoHp.setError("No Hp harus diisi");
        }

        if (TextUtils.isEmpty(mEtNamaUsaha.getText().toString())) {
            canContinue = false;
            mTilEtNamaUsaha.setErrorEnabled(true);
            mTilEtNamaUsaha.setError("Nama Usaha harus diisi");
        }

        if (TextUtils.isEmpty(mEtNamaPemilik.getText().toString())) {
            canContinue = false;
            mTilEtNamaPemilik.setErrorEnabled(true);
            mTilEtNamaPemilik.setError("Nama Pemilik harus diisi");
        }

        if (TextUtils.isEmpty(mEtLaLo.getText().toString())) {
            canContinue = false;
            mTilEtLaLo.setErrorEnabled(true);
            mTilEtLaLo.setError("Latitude Longitude harus diisi");
        }

        if (TextUtils.isEmpty(mEtNamaPemilik.getText().toString())) {
            canContinue = false;
            mTilEtNamaPemilik.setErrorEnabled(true);
            mTilEtNamaPemilik.setError("Nama Pemilik harus diisi");
        }

        return canContinue;
    }

    @OnTouch(R.id.bp_etAlamat)
    boolean etAlamatTouched() {
        mTilEtAlamat.setErrorEnabled(false);
        return false;
    }

    @OnTouch(R.id.bp_actvKota)
    boolean actvKotaTouched() {
        mTilActvKota.setErrorEnabled(false);
        return false;
    }

    @OnTouch(R.id.bp_etLaLo)
    boolean etLaLoTouched() {
        mTilEtLaLo.setErrorEnabled(false);
        return false;
    }

    @OnTouch(R.id.bp_etNamaPemilik)
    boolean etNamaPemilikTouched() {
        mTilEtNamaPemilik.setErrorEnabled(false);
        return false;
    }

    @OnTouch(R.id.bp_etNamaUsaha)
    boolean etNamaUsahaTouched() {
        mTilEtNamaUsaha.setErrorEnabled(false);
        return false;
    }

    @OnTouch(R.id.bp_etNoHp)
    boolean etNoHpTouched() {
        mTilEtNoHp.setErrorEnabled(false);
        return false;
    }

    @OnClick(R.id.bp_tvDelKota)
    void tvDelKotaClicked() {
        unlockActvKota();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bp, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_bp_save:
                if(BUtil.gpsCheck(this)) {
                    save();
                }else{
                    BUtil.openGpsSetting(this);
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        City city = (City) parent.getItemAtPosition(position);
        mActvKota.setText(city.getName());
        mCitySelected = city;

        lockActvKota();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("", "");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private class ActvKotaAdapter extends BaseAdapter implements Filterable {
        private List<City> mCityList;
        private List<City> mCitySuggestionList = new ArrayList<>();
        private Filter filter = new ActvKotaAdapterFilter();

        public ActvKotaAdapter(List<City> daftCustList) {
            this.mCityList = daftCustList;
        }

        @Override
        public int getCount() {
            return mCitySuggestionList.size();
        }

        @Override
        public Object getItem(int position) {
            return mCitySuggestionList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            City city = mCitySuggestionList.get(position);
            View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) BpActivity.this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.row_mainso_actvnamacust, null);
            } else {
                view = convertView;
            }

            TextView tvNamaPerusahaan =
                    (TextView) view.findViewById(R.id.mainso_actvnamacust_tvNamaPerusahaan);
            TextView tvNamaPemilik =
                    (TextView) view.findViewById(R.id.mainso_actvnamacust_tvNamaPemilik);

            tvNamaPerusahaan.setText(city.getName());
            tvNamaPemilik.setText(city.getName());

            return view;
        }


        @Override
        public Filter getFilter() {
            return filter;
        }


        private class ActvKotaAdapterFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                mCitySuggestionList.clear();

                if (mCityList != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                    for (int i = 0; i < mCityList.size(); i++) {
                        City city = mCityList.get(i);
                        city.getName().toLowerCase().contains(constraint);
                        if (city.getName().toLowerCase().contains(constraint)) { // Compare item in original list if it contains constraints.
                            mCitySuggestionList.add(city); // If TRUE add item in Suggestions.
                        }
                    }
                }
                FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
                results.values = mCitySuggestionList;
                results.count = mCitySuggestionList.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!BUtil.gpsCheck(this)) {
            BUtil.openGpsSetting(this);
        }
        getCoordinate();
    }
}
