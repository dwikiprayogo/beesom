package com.bits.bee.so.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by basofi on 10/06/16.
 */
@DatabaseTable(tableName = Sod.TBL_NAME)
public class Sod implements Serializable {
    public static final String TBL_NAME = "sod";

    public static final String ID = "id";
    public static final String SO_ID = "so_id";
    public static final String DNO = "dno";
    public static final String ITEM_ID = "item_id";
    public static final String ITEM_OLID = "item_olid";
    public static final String ITEMNAME = "itemname";
    public static final String QTY = "qty";
    public static final String BASEPRICE = "baseprice";
    public static final String LISTPRICE = "listprice";
    public static final String DISCEXP = "discexp";
    public static final String DISCAMT = "discamt";
    public static final String TOTALDISCAMT = "totaldiscamt";
    public static final String TAXABLEAMT = "taxableamt";
    public static final String TAXAMT = "taxamt";
    public static final String TOTALTAXAMT = "totaltaxamt";
    public static final String BASETOTALTAXAMT = "basetotaltaxamt";
    public static final String BASEFISTOTALTAXAMT = "basefistotaltaxamt";
    public static final String SUBTOTAL = "subtotal";
    public static final String BASESUBTOTAL = "basesubtotal";
    public static final String HARGA = "harga";
    public static final String UNIT_ID = "unit_id";
    public static final String UNIT_OLID = "unit_olid";
    public static final String SUBITEM_OLID = "subitem_olid";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = SO_ID, foreign = true, foreignAutoRefresh = true)
    private So soid;
    @DatabaseField(columnName = DNO)
    private int dno;
    @DatabaseField(columnName = ITEM_ID, foreign = true, foreignAutoRefresh = true)
    private Item item;
    @DatabaseField(columnName = ITEM_OLID)
    private String itemolid;
    @DatabaseField(columnName = ITEMNAME)
    private String itemname;
    @DatabaseField(columnName = QTY)
    private BigDecimal qty = new BigDecimal(0);
    @DatabaseField(columnName = BASEPRICE)
    private BigDecimal baseprice = new BigDecimal(0);
    @DatabaseField(columnName = LISTPRICE)
    private BigDecimal listprice = new BigDecimal(0);
    @DatabaseField(columnName = DISCEXP)
    private String discexp;
    @DatabaseField(columnName = DISCAMT)
    private BigDecimal discamt = new BigDecimal(0);
    @DatabaseField(columnName = TOTALDISCAMT)
    private BigDecimal totaldiscamt = new BigDecimal(0);
    @DatabaseField(columnName = TAXABLEAMT)
    private BigDecimal taxableamt = new BigDecimal(0);
    @DatabaseField(columnName = TAXAMT)
    private BigDecimal taxamt = new BigDecimal(0);
    @DatabaseField(columnName = TOTALTAXAMT)
    private BigDecimal totaltaxamt = new BigDecimal(0);
    @DatabaseField(columnName = BASETOTALTAXAMT)
    private BigDecimal basetotaltaxamt = new BigDecimal(0);
    @DatabaseField(columnName = BASEFISTOTALTAXAMT)
    private BigDecimal basefistotaltaxamt = new BigDecimal(0);
    @DatabaseField(columnName = SUBTOTAL)
    private BigDecimal subtotal = new BigDecimal(0);
    @DatabaseField(columnName = BASESUBTOTAL)
    private BigDecimal basesubtotal = new BigDecimal(0);
    @DatabaseField(columnName = HARGA)
    private BigDecimal harga = new BigDecimal(0);
    @DatabaseField(columnName = UNIT_ID, foreign = true, foreignAutoRefresh = true)
    private Unit unit;
    @DatabaseField(columnName = UNIT_OLID)
    private String unitolid;
    @DatabaseField(columnName = SUBITEM_OLID)
    private String subitemolid;

    public String getSubitemolid() {
        return subitemolid;
    }

    public void setSubitemolid(String subitemolid) {
        this.subitemolid = subitemolid;
    }

    public String getUnitolid() {
        return unitolid;
    }

    public void setUnitolid(String unitolid) {
        this.unitolid = unitolid;
    }

    public String getItemolid() {
        return itemolid;
    }

    public void setItemolid(String itemolid) {
        this.itemolid = itemolid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }

    public BigDecimal getBasesubtotal() {
        return basesubtotal;
    }

    public void setBasesubtotal(BigDecimal basesubtotal) {
        this.basesubtotal = basesubtotal;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getBasefistotaltaxamt() {
        return basefistotaltaxamt;
    }

    public void setBasefistotaltaxamt(BigDecimal basefistotaltaxamt) {
        this.basefistotaltaxamt = basefistotaltaxamt;
    }

    public BigDecimal getBasetotaltaxamt() {
        return basetotaltaxamt;
    }

    public void setBasetotaltaxamt(BigDecimal basetotaltaxamt) {
        this.basetotaltaxamt = basetotaltaxamt;
    }

    public BigDecimal getTotaltaxamt() {
        return totaltaxamt;
    }

    public void setTotaltaxamt(BigDecimal totaltaxamt) {
        this.totaltaxamt = totaltaxamt;
    }

    public BigDecimal getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(BigDecimal taxamt) {
        this.taxamt = taxamt;
    }

    public BigDecimal getTaxableamt() {
        return taxableamt;
    }

    public void setTaxableamt(BigDecimal taxableamt) {
        this.taxableamt = taxableamt;
    }

    public BigDecimal getTotaldiscamt() {
        return totaldiscamt;
    }

    public void setTotaldiscamt(BigDecimal totaldiscamt) {
        this.totaldiscamt = totaldiscamt;
    }

    public BigDecimal getDiscamt() {
        return discamt;
    }

    public void setDiscamt(BigDecimal discamt) {
        this.discamt = discamt;
    }

    public String getDiscexp() {
        return discexp;
    }

    public void setDiscexp(String discexp) {
        this.discexp = discexp;
    }

    public BigDecimal getListprice() {
        return listprice;
    }

    public void setListprice(BigDecimal listprice) {
        this.listprice = listprice;
    }

    public BigDecimal getBaseprice() {
        return baseprice;
    }

    public void setBaseprice(BigDecimal baseprice) {
        this.baseprice = baseprice;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public int getDno() {
        return dno;
    }

    public void setDno(int dno) {
        this.dno = dno;
    }

    public So getSoid() {
        return soid;
    }

    public void setSoid(So soid) {
        this.soid = soid;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
