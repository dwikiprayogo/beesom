package com.bits.bee.so.view.fragment;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bits.bee.so.R;
import com.bits.bee.so.view.activity.TotalSalesActivity;
import com.bits.bee.so.view.widget.WidgetCheckInHariIni;
import com.bits.bee.so.view.widget.WidgetCheckInSelanjutnya;
import com.bits.bee.so.view.widget.WidgetCustomerBaru;
import com.bits.bee.so.view.widget.WidgetOrderSalesHariIni;
import com.bits.bee.so.view.widget.WidgetTargetCheckIn;
import com.bits.bee.so.view.widget.WidgetTotalPenjualan;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainDashboardFragment extends Fragment {

    @BindView(R.id.maindashboard_widgetTotalPenjualan)
    WidgetTotalPenjualan mWidgetTotalPenjualan;

    @BindView(R.id.maindashboard_widgetOrderSalesHariIni)
    WidgetOrderSalesHariIni mWidgetOrderSalesHariIni;

    @BindView(R.id.maindashboard_widgetTargetCheckIn)
    WidgetTargetCheckIn mWidgetTargetCheckIn;

    @BindView(R.id.maindashboard_widgetCheckInHariIni)
    WidgetCheckInHariIni mWidgetCheckInHariIni;

    @BindView(R.id.maindashboard_widgetCheckInSelanjutnya)
    WidgetCheckInSelanjutnya mWidgetCheckInSelanjutnya;

    @BindView(R.id.maindashboard_widgetCustomerBaru)
    WidgetCustomerBaru mWidgetCustomerBaru;

    public MainDashboardFragment() {
        // Required empty public constructor
    }

    public static MainDashboardFragment newInstance() {
        MainDashboardFragment fragment = new MainDashboardFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maindashboard, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        renderWidgets();
    }

    private void renderWidgets(){
        mWidgetTotalPenjualan.render();
        mWidgetOrderSalesHariIni.render();

        mWidgetCheckInHariIni.setVisibility(View.GONE);
        mWidgetTargetCheckIn.setVisibility(View.GONE);
        mWidgetCheckInSelanjutnya.setVisibility(View.GONE);
        mWidgetCustomerBaru.setVisibility(View.GONE);

//        mWidgetCheckInHariIni.render();
//        mWidgetTargetCheckIn.render();
//        mWidgetCheckInSelanjutnya.render();
//        mWidgetCustomerBaru.render();
    }

    @OnClick(R.id.maindashboard_widgetTotalPenjualan)
    void onTotalPenjualanClicked(){
        startActivity(new Intent(getActivity(), TotalSalesActivity.class));
    }
}