package com.bits.bee.so.bl.net.model;

import com.bits.bee.so.bl.db.dao.SodDao;
import com.bits.bee.so.bl.db.model.So;
import com.bits.bee.so.bl.db.model.Sod;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 01/07/16.
 */
public class SoPost {
    @SerializedName("soarray")
    @Expose
    private List<SoArray> soArray = new ArrayList<SoArray>();

    /**
     * @return The soarray
     */
    public List<SoArray> getSoArray() {
        return soArray;
    }

    /**
     * @param soArray The soarray
     */
    public void setSoArray(List<SoArray> soArray) {
        this.soArray = soArray;
    }

    public void addSo(So so) {
        SoArray soArray = new SoArray(so);
        getSoArray().add(soArray);
    }

    public class SoArray {

        @SerializedName("trxdate")
        @Expose
        private String trxdate;
        @SerializedName("bp_id")
        @Expose
        private String bpId;
        @SerializedName("crc_id")
        @Expose
        private String crcId;
        @SerializedName("subtotal")
        @Expose
        private String subtotal;
        @SerializedName("discexp")
        @Expose
        private String discexp;
        @SerializedName("taxamt")
        @Expose
        private String taxamt;
        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("coordinate")
        @Expose
        private String coordinate;
        @SerializedName("sods")
        @Expose
        private List<SodPost> sodPosts = new ArrayList<SodPost>();

        public SoArray(So so) {
            setBpId(so.getBpolid());
            setDiscexp(so.getDiscexp());
            setCrcId("1");
            setSubtotal(so.getSubtotal().toPlainString());
            setTaxamt(so.getTaxamt().toPlainString());
            setTotal(so.getTotal().toPlainString());
            setTrxdate(so.getTrxdate());
            setCoordinate(so.getTrxCoordinate());
            try {
                List<Sod> sodList = SodDao.getInstance().readBySo(so);
                for(int i = 0 ; i < sodList.size() ; i++){
                    Sod sod = sodList.get(i);
                    SodPost sodPost = new SodPost(sod);
                    getSodPosts().add(sodPost);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void setTrxdate(String trxdate) {
            this.trxdate = trxdate;
        }

        public String getBpId() {
            return bpId;
        }

        public void setBpId(String bpId) {
            this.bpId = bpId;
        }

        public String getCrcId() {
            return crcId;
        }

        public void setCrcId(String crcId) {
            this.crcId = crcId;
        }

        public String getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(String subtotal) {
            this.subtotal = subtotal;
        }

        public String getDiscexp() {
            return discexp;
        }

        public void setDiscexp(String discexp) {
            this.discexp = discexp;
        }

        public String getTaxamt() {
            return taxamt;
        }

        public void setTaxamt(String taxamt) {
            this.taxamt = taxamt;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public List<SodPost> getSodPosts() {
            return sodPosts;
        }

        public void setSodPosts(List<SodPost> sodPosts) {
            this.sodPosts = sodPosts;
        }

        public String getTrxdate() {
            return trxdate;
        }

        public String getCoordinate() {
            return coordinate;
        }

        public void setCoordinate(String coordinate) {
            this.coordinate = coordinate;
        }

        public class SodPost {

            @SerializedName("dno")
            @Expose
            private String dno;
            @SerializedName("item_id")
            @Expose
            private String itemId;
            @SerializedName("itemname")
            @Expose
            private String itemname;
            @SerializedName("qty")
            @Expose
            private String qty;
            @SerializedName("listprice")
            @Expose
            private String listprice;
            @SerializedName("conv")
            @Expose
            private String conv;
            @SerializedName("unit")
            @Expose
            private String unit;
            @SerializedName("baseprice")
            @Expose
            private String baseprice;
            @SerializedName("discexp")
            @Expose
            private String discexp;
            @SerializedName("discamt")
            @Expose
            private String discamt;
            @SerializedName("totaldiscamt")
            @Expose
            private String totaldiscamt;
            @SerializedName("disc2amt")
            @Expose
            private String disc2amt;
            @SerializedName("totaldisc2amt")
            @Expose
            private String totaldisc2amt;
            @SerializedName("subtotal")
            @Expose
            private String subtotal;
            @SerializedName("basesubtotal")
            @Expose
            private String basesubtotal;
            @SerializedName("tax_code")
            @Expose
            private String taxCode;
            @SerializedName("taxableamt")
            @Expose
            private String taxableamt;
            @SerializedName("taxamt")
            @Expose
            private String taxamt;
            @SerializedName("totaltaxamt")
            @Expose
            private String totaltaxamt;
            @SerializedName("basetotaltaxamt")
            @Expose
            private String basetotaltaxamt;
            @SerializedName("basefistotaltaxamt")
            @Expose
            private String basefistotaltaxamt;

            public SodPost(Sod sod){
                this.setTaxamt(sod.getTaxamt().toPlainString());
                this.setSubtotal(sod.getSubtotal().toPlainString());
                this.setDiscexp(sod.getDiscexp());
                this.setBasefistotaltaxamt(sod.getBasefistotaltaxamt().toPlainString());
                this.setBaseprice(sod.getBaseprice().toPlainString());
                this.setBasesubtotal(sod.getBasesubtotal().toPlainString());
                this.setBasetotaltaxamt(sod.getBasetotaltaxamt().toPlainString());
                this.setConv("1");
//                this.setDisc2amt(sod.getDisc2amt());
                this.setDiscamt(sod.getDiscamt().toPlainString());
                this.setDno(""+sod.getDno());
                this.setItemId(sod.getItemolid());
                this.setItemname(sod.getItemname());
                this.setListprice(sod.getListprice().toPlainString());
                this.setQty(sod.getQty().toPlainString());
                this.setSubtotal(sod.getSubtotal().toPlainString());
                this.setTaxableamt(sod.getTaxableamt().toPlainString());
                this.setTaxamt(sod.getTaxamt().toPlainString());
                this.setTaxCode("PPN");
//                this.setTotaldisc2amt();
                this.setTotaldiscamt(sod.getTotaldiscamt().toPlainString());
                this.setUnit(sod.getUnitolid());
            }

            /**
             * @return The dno
             */
            public String getDno() {
                return dno;
            }

            /**
             * @param dno The dno
             */
            public void setDno(String dno) {
                this.dno = dno;
            }

            /**
             * @return The itemId
             */
            public String getItemId() {
                return itemId;
            }

            /**
             * @param itemId The item_id
             */
            public void setItemId(String itemId) {
                this.itemId = itemId;
            }

            /**
             * @return The itemname
             */
            public String getItemname() {
                return itemname;
            }

            /**
             * @param itemname The itemname
             */
            public void setItemname(String itemname) {
                this.itemname = itemname;
            }

            /**
             * @return The qty
             */
            public String getQty() {
                return qty;
            }

            /**
             * @param qty The qty
             */
            public void setQty(String qty) {
                this.qty = qty;
            }

            /**
             * @return The listprice
             */
            public String getListprice() {
                return listprice;
            }

            /**
             * @param listprice The listprice
             */
            public void setListprice(String listprice) {
                this.listprice = listprice;
            }

            /**
             * @return The conv
             */
            public String getConv() {
                return conv;
            }

            /**
             * @param conv The conv
             */
            public void setConv(String conv) {
                this.conv = conv;
            }

            /**
             * @return The unit
             */
            public String getUnit() {
                return unit;
            }

            /**
             * @param unit The unit
             */
            public void setUnit(String unit) {
                this.unit = unit;
            }

            /**
             * @return The baseprice
             */
            public String getBaseprice() {
                return baseprice;
            }

            /**
             * @param baseprice The baseprice
             */
            public void setBaseprice(String baseprice) {
                this.baseprice = baseprice;
            }

            /**
             * @return The discexp
             */
            public String getDiscexp() {
                return discexp;
            }

            /**
             * @param discexp The discexp
             */
            public void setDiscexp(String discexp) {
                this.discexp = discexp;
            }

            /**
             * @return The discamt
             */
            public String getDiscamt() {
                return discamt;
            }

            /**
             * @param discamt The discamt
             */
            public void setDiscamt(String discamt) {
                this.discamt = discamt;
            }

            /**
             * @return The totaldiscamt
             */
            public String getTotaldiscamt() {
                return totaldiscamt;
            }

            /**
             * @param totaldiscamt The totaldiscamt
             */
            public void setTotaldiscamt(String totaldiscamt) {
                this.totaldiscamt = totaldiscamt;
            }

            /**
             * @return The disc2amt
             */
            public String getDisc2amt() {
                return disc2amt;
            }

            /**
             * @param disc2amt The disc2amt
             */
            public void setDisc2amt(String disc2amt) {
                this.disc2amt = disc2amt;
            }

            /**
             * @return The totaldisc2amt
             */
            public String getTotaldisc2amt() {
                return totaldisc2amt;
            }

            /**
             * @param totaldisc2amt The totaldisc2amt
             */
            public void setTotaldisc2amt(String totaldisc2amt) {
                this.totaldisc2amt = totaldisc2amt;
            }

            /**
             * @return The subtotal
             */
            public String getSubtotal() {
                return subtotal;
            }

            /**
             * @param subtotal The subtotal
             */
            public void setSubtotal(String subtotal) {
                this.subtotal = subtotal;
            }

            /**
             * @return The basesubtotal
             */
            public String getBasesubtotal() {
                return basesubtotal;
            }

            /**
             * @param basesubtotal The basesubtotal
             */
            public void setBasesubtotal(String basesubtotal) {
                this.basesubtotal = basesubtotal;
            }

            /**
             * @return The taxCode
             */
            public String getTaxCode() {
                return taxCode;
            }

            /**
             * @param taxCode The tax_code
             */
            public void setTaxCode(String taxCode) {
                this.taxCode = taxCode;
            }

            /**
             * @return The taxableamt
             */
            public String getTaxableamt() {
                return taxableamt;
            }

            /**
             * @param taxableamt The taxableamt
             */
            public void setTaxableamt(String taxableamt) {
                this.taxableamt = taxableamt;
            }

            /**
             * @return The taxamt
             */
            public String getTaxamt() {
                return taxamt;
            }

            /**
             * @param taxamt The taxamt
             */
            public void setTaxamt(String taxamt) {
                this.taxamt = taxamt;
            }

            /**
             * @return The totaltaxamt
             */
            public String getTotaltaxamt() {
                return totaltaxamt;
            }

            /**
             * @param totaltaxamt The totaltaxamt
             */
            public void setTotaltaxamt(String totaltaxamt) {
                this.totaltaxamt = totaltaxamt;
            }

            /**
             * @return The basetotaltaxamt
             */
            public String getBasetotaltaxamt() {
                return basetotaltaxamt;
            }

            /**
             * @param basetotaltaxamt The basetotaltaxamt
             */
            public void setBasetotaltaxamt(String basetotaltaxamt) {
                this.basetotaltaxamt = basetotaltaxamt;
            }

            /**
             * @return The basefistotaltaxamt
             */
            public String getBasefistotaltaxamt() {
                return basefistotaltaxamt;
            }

            /**
             * @param basefistotaltaxamt The basefistotaltaxamt
             */
            public void setBasefistotaltaxamt(String basefistotaltaxamt) {
                this.basefistotaltaxamt = basefistotaltaxamt;
            }
        }
    }
}