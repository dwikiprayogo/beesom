package com.bits.bee.so.bl.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.BatchDao;
import com.bits.bee.so.bl.db.dao.BpDao;
import com.bits.bee.so.bl.db.dao.SaleDao;
import com.bits.bee.so.bl.db.dao.SoDao;
import com.bits.bee.so.bl.db.model.Batch;
import com.bits.bee.so.bl.db.model.Bp;
import com.bits.bee.so.bl.db.model.Sale;
import com.bits.bee.so.bl.db.model.So;
import com.bits.bee.so.bl.db.model.Usr;
import com.bits.bee.so.bl.net.api.BApi;
import com.bits.bee.so.bl.net.api.BApiHelper;
import com.bits.bee.so.bl.net.model.BpPost;
import com.bits.bee.so.bl.net.model.PostReturn;
import com.bits.bee.so.bl.net.model.SalePost;
import com.bits.bee.so.bl.net.model.SaleReturn;
import com.bits.bee.so.bl.net.model.SoPost;
import com.bits.bee.so.service.ProfileService;
import com.bits.bee.so.view.dialog.BDialog;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;

/**
 * Created by basofi on 30/06/16.
 */
public class UploadService {

    private Context mContext;
    private ProgressDialog mPd;
    private BApi mBApi;

    private Call<PostReturn> mBpPostReturn;
    private Call<PostReturn> mSoPostReturn;
    private Call<SaleReturn> mSaleReturn;

    private UploadTask mUploadTask;

    public UploadService(Context context) {
        this.mContext = context;

        Usr usr = ProfileService.getInstance().getActiveUsr();
        mBApi = BApiHelper.build(mContext, "https://app.beecloud.id", usr.getApikey());

        mPd = new ProgressDialog(mContext);
        mPd.setTitle(mContext.getString(R.string.uploadservice_title_mpd));
        mPd.setMessage(mContext.getString(R.string.uploadservice_msg_mpd));
        mPd.setCancelable(false);
        mPd.setButton(DialogInterface.BUTTON_NEGATIVE,
                mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cancelUpload();
                        mPd.dismiss();
                    }
                });
    }

    private void cancelUpload() {
        mUploadTask.cancel(true);
//        mBpPostReturn.cancel();
//        mSaleReturn.cancel();
//        mSoPostReturn.cancel();
//
//        BDialog.showInfoDialog(mContext, "Information", "Uploading data cancelled");
    }

    private void dismissMPd() {
        if (mPd.isShowing()) {
            mPd.dismiss();
        }
    }

    private void showMPd() {
        if (!mPd.isShowing()) {
            mPd.show();
        }
    }

    public void upload() {
        mUploadTask = new UploadTask();
        mUploadTask.execute(new Void[0]);

//        showMPd();
//
//        uploadBp();
//        uploadSo();
//        uploadSale();
//
//        dismissMPd();
//
//        BDialog.showInfoDialog(mContext, "Information", "Uploading data completed");
    }

    private void uploadBp() {
        try {
            BpPost bpPost = new BpPost();
            final List<Bp> bpListNotUploaded = BpDao.getInstance().readNotUploaded();
            if (!(bpListNotUploaded.size() == 0)) {
                for (int i = 0; i < bpListNotUploaded.size(); i++) {
                    Bp bp = bpListNotUploaded.get(i);
                    bpPost.addBp(bp);
                }
                mBpPostReturn = mBApi.postBp(bpPost);

                PostReturn postReturn = mBpPostReturn.execute().body();
                if (postReturn.getStatus()) {
                    for (int i = 0; i < bpListNotUploaded.size(); i++) {
                        Bp bp = bpListNotUploaded.get(i);
                        bp.setIsUploaded(true);
                        try {
                            BpDao.getInstance().update(bp);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadSo() {
        try {
            SoPost soPost = new SoPost();
            final List<So> soListNotUploaded = SoDao.getInstance().readNotUploaded();
            if (!(soListNotUploaded.size() == 0)) {
                for (int i = 0; i < soListNotUploaded.size(); i++) {
                    So so = soListNotUploaded.get(i);
                    soPost.addSo(so);
                }
                mSoPostReturn = mBApi.postSo(soPost);
                PostReturn postReturn = mSoPostReturn.execute().body();
                if (postReturn.getStatus()) {
                    for (int i = 0; i < soListNotUploaded.size(); i++) {
                        So so = soListNotUploaded.get(i);
                        so.setIsUploaded(true);
                        try {
                            SoDao.getInstance().update(so);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadSale() {
        try {
            SalePost salePost = new SalePost();
            final List<Sale> saleListNotUploaded = SaleDao.getInstance().readNotUploaded();
            if (!(saleListNotUploaded.size() == 0)) {
                for (int i = 0; i < saleListNotUploaded.size(); i++) {
                    Sale sale = saleListNotUploaded.get(i);
                    salePost.addSale(sale);
                }
                mSaleReturn = mBApi.postSale(salePost);
                SaleReturn saleReturn = mSaleReturn.execute().body();
                if (saleReturn.getStatus()) {
                    List<SaleReturn.Data.BatchContent> batchContentList =
                            saleReturn.getData().getBatchContent();

                    int lastBatchNo = 0;
                    try{
                        lastBatchNo = BatchDao.getInstance().lastBatchId();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    lastBatchNo++;

                    long trxDateLong = Calendar.getInstance().getTimeInMillis();

                    for(int i = 0 ; i<batchContentList.size() ; i++){
                        SaleReturn.Data.BatchContent batchContent = batchContentList.get(i);
                        try {
                            Sale sale = SaleDao.getInstance().readById(batchContent.getClientId());
                            sale.setIsUploaded(true);
                            SaleDao.getInstance().update(sale);

                            Batch batch = new Batch();
                            batch.setBatchId(lastBatchNo);
                            batch.setSaleId(sale);
                            batch.setTrxDateLong(trxDateLong);

                            BatchDao.getInstance().create(batch);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

//                    for (int i = 0; i < saleListNotUploaded.size(); i++) {
//                        Sale sale = saleListNotUploaded.get(i);
//                        sale.setIsUploaded(true);
//                        try {
//                            SaleDao.getInstance().update(sale);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class UploadTask extends AsyncTask<Void, Void, Void> {

        private void cancelUpload() {
            try {
                mBpPostReturn.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                mSaleReturn.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                mSoPostReturn.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }

            BDialog.showInfoDialog(mContext, "Information", "Uploading data cancelled");
        }

        @Override
        protected void onPreExecute() {
            showMPd();
        }

        @Override
        protected Void doInBackground(Void... params) {
            uploadBp();
            uploadSale();
            uploadSo();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dismissMPd();
            BDialog.showInfoDialog(mContext, "Information", "Uploading data completed");
        }

        @Override
        protected void onCancelled() {
            cancelUpload();
        }
    }
}