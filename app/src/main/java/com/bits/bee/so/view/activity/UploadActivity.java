package com.bits.bee.so.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.service.UploadService;
import com.bits.bee.so.util.BPrefs;
import com.bits.bee.so.util.BUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UploadActivity extends AppCompatActivity {

    @BindView(R.id.upload_ivUpload)
    ImageView mIvDownload;

    @BindView(R.id.upload_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.upload_tvLastSynced)
    TextView mTvLastSynced;

    private Animation mIvUploadAnimation;

//    private UploadService mUploadService;
    private UploadService mUploadService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        ButterKnife.bind(this);
        initViews();
        loadData();

//        mUploadService = new UploadService(this);
        mUploadService = new UploadService(this);
    }

    private void loadData(){
        String lastUploadTime = BPrefs.getInstance().getLastUploadTime();

        if(!TextUtils.isEmpty(lastUploadTime)){
            mTvLastSynced.setVisibility(View.VISIBLE);

            String lastSynced = getString(R.string.importupload_lastsynced);
            lastSynced = String.format(lastSynced, lastUploadTime);
            mTvLastSynced.setText(lastSynced);
        }else{
            mTvLastSynced.setVisibility(View.GONE);
        }

    }

    private void initViews() {
        mIvUploadAnimation = AnimationUtils.loadAnimation(
                this, R.anim.mainimport_ivdownloadanim);
        mIvDownload.startAnimation(mIvUploadAnimation);

        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @OnClick(R.id.upload_ivUpload)
    void doUpload(){
        mUploadService.upload();

        BPrefs.getInstance().setLastUploadTime(BUtil.getCompleteCurrentDateAndTime());

        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
