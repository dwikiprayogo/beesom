package com.bits.bee.so.service;

import android.content.Context;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.util.BGps;
import com.bits.bee.so.util.BPrefs;

/**
 * Created by basofi on 20/07/16.
 */
public class AppInitializer {
    private static AppInitializer singleton;

    public static AppInitializer getInstance(){
        if(singleton == null){
            singleton = new AppInitializer();
        }
        return singleton;
    }

    public void initApp(Context context){
        Db.getInstance().init(context);
//        BUtil.exportDB(context);
        ProfileService.getInstance().init(context);
        BGps.getInstance().init(context);
        BPrefs.getInstance().init(context);
    }
}