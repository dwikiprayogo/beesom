package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.Item;
import com.bits.bee.so.bl.db.model.Unit;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 27/05/16.
 */
public class UnitDao implements DaoCrud<Unit, Integer> {
    private static UnitDao singleton;

    public static UnitDao getInstance() {
        if (singleton == null) {
            singleton = new UnitDao();
        }
        return singleton;
    }

    @Override
    public void create(Unit model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Unit model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Unit model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Unit model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<Unit> read() throws Exception {
        return getDao().queryForAll();
    }

    public List<Unit> readByItem(Item item) throws Exception{
        List<Unit> unitList = new ArrayList<>();

        QueryBuilder queryBuilder = getDao().queryBuilder();
        queryBuilder.where()
                .eq(Unit.ITEM_ID, item)
                .and()
                .isNotNull(Unit.UNIT)
                .and()
                .not()
                .eq(Unit.UNIT, "");

        PreparedQuery preparedQuery = queryBuilder.prepare();

        unitList = getDao().query(preparedQuery);
        if(unitList.size() > 0){
            return unitList;
        }else {
            throw new Exception("Unit not found");
        }
    }

    public Unit readByOlId(String olId) throws Exception{
        List<Unit> unitList = new ArrayList<>();
        unitList = getDao().queryForEq(Unit.OL_ID, olId);
        if(unitList.size() > 0){
            return unitList.get(0);
        }else {
            throw new Exception("Unit not found");
        }
    }

    @Override
    public Dao<Unit, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Unit.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Unit.class);
    }
}
