package com.bits.bee.so.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.LocationManager;
import android.os.Environment;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bits.bee.so.bl.db.DbHelper;
import com.bits.bee.so.view.dialog.BDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by basofi on 15/06/16.
 */
public class BUtil {

    public static Calendar getStartOfDay() {
        Calendar calendarStart = Calendar.getInstance();
        calendarStart.set(Calendar.HOUR_OF_DAY, calendarStart.getActualMinimum(Calendar.HOUR_OF_DAY));
        calendarStart.set(Calendar.MINUTE, calendarStart.getActualMinimum(Calendar.MINUTE));
        calendarStart.set(Calendar.SECOND, calendarStart.getActualMinimum(Calendar.SECOND));

        return calendarStart;
    }

    public static Calendar getEndOfDay() {
        Calendar calendarEnd = Calendar.getInstance();

        calendarEnd.set(Calendar.HOUR_OF_DAY, calendarEnd.getActualMaximum(Calendar.HOUR_OF_DAY));
        calendarEnd.set(Calendar.MINUTE, calendarEnd.getActualMaximum(Calendar.MINUTE));
        calendarEnd.set(Calendar.SECOND, calendarEnd.getActualMaximum(Calendar.SECOND));

        return calendarEnd;
    }

    public static String getCompleteCurrentDateAndTime(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy | HH:mm");

        Calendar calendar = Calendar.getInstance();
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String getCurrentDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");

        Calendar calendar = Calendar.getInstance();
        return simpleDateFormat.format(calendar.getTime());
    }

    public static void exportDB(Context context) {
        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source = null;
        FileChannel destination = null;
        String currentDBPath = "/data/" + "com.bits.bee.so"
                + "/databases/" + DbHelper.DBNAME;
        String backupDBPath = DbHelper.DBNAME;
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            Toast.makeText(context, "DB Exported!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String addCurr(String total) {
        String totalVal = getDefNumberFormat().format(Double.valueOf(total));
        String retVal = String.format("Rp %s", totalVal);
        return retVal;
    }

    public static NumberFormat getDefNumberFormat() {
        DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance(Locale.getDefault());
        String decimalSeparator = "" + ds.getDecimalSeparator();
        String thousandSeparator = "" + ds.getGroupingSeparator();

        String format = String.format("#%s###%s##", thousandSeparator, decimalSeparator);
        DecimalFormat df = new DecimalFormat();
        try {
            df.applyPattern(format);
            df.setDecimalFormatSymbols(ds);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return df;
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static String getHHmm(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        return simpleDateFormat.format(new Date(time));
    }

    public static boolean isGpsAvailable(Context context) {
        PackageManager pm = context.getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
    }

    public static boolean isGpsEnabled(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean gpsCheck(final Context context) {
        if (BUtil.isGpsAvailable(context)) {
            if (BUtil.isGpsEnabled(context)) {
                return true;
            }
        }
        return false;
    }

    public static void openGpsSetting(final Context context) {
        BDialog.showInfoDialog(context,
                "GPS Tidak Aktif", "Tekan OK untuk membuka menu setting GPS",
                new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onAny(MaterialDialog dialog) {
                        Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(i);
                    }
                });
    }

    public static String genCoordinate(double lat, double lo) {
        return String.format("%s,%s", lat, lo);
    }

}
