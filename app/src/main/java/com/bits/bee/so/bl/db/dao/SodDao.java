package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.So;
import com.bits.bee.so.bl.db.model.Sod;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.List;

/**
 * Created by basofi on 13/06/16.
 */
public class SodDao implements DaoCrud<Sod, Integer> {

    private static SodDao singleton;

    public static SodDao getInstance() {
        if (singleton == null) {
            singleton = new SodDao();
        }
        return singleton;
    }

    @Override
    public void create(Sod model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Sod model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Sod model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Sod model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<Sod> read() throws Exception {
        return getDao().queryForAll();
    }

    public List<Sod> readBySo(So so) throws Exception{
        return getDao().queryForEq(Sod.SO_ID, so.getId());
    }

    @Override
    public Dao<Sod, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Sod.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Sod.class);
    }
}
