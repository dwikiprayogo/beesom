package com.bits.bee.so.util;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * Created by basofi on 29/07/16.
 */
public class BGps implements LocationListener {

    private LocationManager mLocationManager;
    private String mProvider;
    private Context mContext;
    private Location mLocation;

    private static BGps singleton;

    private double mLat = 0;
    private double mLong = 0;

    public static BGps getInstance() {
        if (singleton == null) {
            singleton = new BGps();
        }
        return singleton;
    }

    public void init(Context context) {
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        mProvider = mLocationManager.getBestProvider(criteria, false);
    }

    @SuppressWarnings({"MissingPermission", "ResourceType"})
    public void refreshLocation() {
        mLocationManager.requestLocationUpdates(mProvider, 400, 1, this);

        if (mLocationManager != null) {
            mLocation = mLocationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (mLocation != null) {
                mLat = mLocation.getLatitude();
                mLong = mLocation.getLongitude();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLat = location.getLatitude();
        mLong = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public double getLat() {
        return mLat;
    }

    public void setLat(double mLat) {
        this.mLat = mLat;
    }

    public double getLong() {
        return mLong;
    }

    public void setmLong(double mLong) {
        this.mLong = mLong;
    }
}
