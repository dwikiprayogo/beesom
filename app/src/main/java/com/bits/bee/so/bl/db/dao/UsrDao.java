package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.Usr;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.List;

/**
 * Created by basofi on 28/06/16.
 */
public class UsrDao implements DaoCrud<Usr, Integer> {

    private static UsrDao singleton;

    public static UsrDao getInstance() {
        if (singleton == null) {
            singleton = new UsrDao();
        }
        return singleton;
    }

    @Override
    public void create(Usr model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Usr model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Usr model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Usr model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<Usr> read() throws Exception {
        return getDao().queryForAll();
    }

    public boolean isNameAndKeyExist(String username, String apikey){

        QueryBuilder queryBuilder = null;
        try {
            queryBuilder = getDao().queryBuilder();

            queryBuilder.where()
                    .eq(Usr.USERNAME, username)
                    .and()
                    .eq(Usr.APIKEY,apikey);

            PreparedQuery preparedQuery = queryBuilder.prepare();

            List<Usr> usrList = getDao().query(preparedQuery);

            return (usrList.size() > 0);

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Dao<Usr, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Usr.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    public void setAllIsActive(boolean allIsActive) throws Exception{
        List<Usr> usrList = read();
        for(int i = 0; i<usrList.size() ; i++){
            Usr registeredDb = usrList.get(i);
            registeredDb.setIsactive(allIsActive);
            update(registeredDb);
        }
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Usr.class);
    }
}
