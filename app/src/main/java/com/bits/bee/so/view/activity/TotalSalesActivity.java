package com.bits.bee.so.view.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.SaleDao;
import com.bits.bee.so.bl.db.model.Bp;
import com.bits.bee.so.bl.db.model.Sale;
import com.bits.bee.so.util.BUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TotalSalesActivity extends AppCompatActivity implements
        AppBarLayout.OnOffsetChangedListener{

    @BindView(R.id.totalsales_appbar)
    AppBarLayout mAppBar;
    @BindView(R.id.totalsales_ctlToolbar)
    CollapsingToolbarLayout mCtlToolbar;
    @BindView(R.id.totalsales_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.totalsales_rvSales)
    RecyclerView mRvSales;

    @BindView(R.id.totalsales_tvTotal)
    TextView mTvTotal;

    private RvSalesAdapter mRvSalesAdapter;
    private BigDecimal mTotalToday = new BigDecimal(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_totalsales);
        ButterKnife.bind(this);

        initViews();
        loadData();
        initListeners();
    }

    private void initListeners(){
        mAppBar.addOnOffsetChangedListener(this);
    }

    private void initViews(){
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.setTitle("Total Sales Hari Ini");

        mRvSales.setLayoutManager(new LinearLayoutManager(this));
        mRvSales.setHasFixedSize(true);

        mRvSalesAdapter = new RvSalesAdapter();

        mRvSales.setAdapter(mRvSalesAdapter);
    }

    private void loadData(){
        BigDecimal totalToday = new BigDecimal(0);
        List<Sale> saleList = new ArrayList<>();

        try {
            totalToday =
                    SaleDao.getInstance().sumTotalByDate(
                            BUtil.getStartOfDay().getTimeInMillis(),
                            BUtil.getEndOfDay().getTimeInMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            saleList = SaleDao.getInstance().readSalesByDate(
                    BUtil.getStartOfDay().getTimeInMillis(),
                    BUtil.getEndOfDay().getTimeInMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mTotalToday = totalToday;

        mRvSalesAdapter.generate(saleList);
        mTvTotal.setText(BUtil.addCurr(mTotalToday.toPlainString()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if(verticalOffset < 0){
            mToolbar.setTitle(BUtil.addCurr(mTotalToday.toPlainString()));
        }else{
            mToolbar.setTitle("Total Sales Hari Ini");
        }
    }

    class RvSalesAdapter extends RecyclerView.Adapter<RvSalesAdapter.ViewHolder>{

        private List<Sale> mSaleList = new ArrayList<>();

        public void generate(List<Sale> saleList){
            this.mSaleList = saleList;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(TotalSalesActivity.this)
                    .inflate(R.layout.row_totalsales_rvsales, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Sale sale = mSaleList.get(position);
            Bp bp = sale.getBpId();

            try {
                holder.mTvNamaPerusahaan.setText(bp.getNamaPerusahaan());
            }catch (Exception e){
                e.printStackTrace();
            }

            holder.mTvTotal.setText(BUtil.addCurr(sale.getTotal().toPlainString()));
            holder.mTvWaktu.setText(BUtil.getHHmm(sale.getTrxdateLong()));
            holder.mTvCoordinate.setText(sale.getTrxCoordinate());
        }

        @Override
        public int getItemCount() {
            return mSaleList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{

            @BindView(R.id.totalsales_rvsales_tvNamaPerusahaan)
            TextView mTvNamaPerusahaan;

            @BindView(R.id.totalsales_rvsales_tvTotal)
            TextView mTvTotal;

            @BindView(R.id.totalsales_rvsales_tvWaktu)
            TextView mTvWaktu;

            @BindView(R.id.totalsales_rvsales_tvCoordinate)
            TextView mTvCoordinate;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
