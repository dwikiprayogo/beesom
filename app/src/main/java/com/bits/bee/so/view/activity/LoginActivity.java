package com.bits.bee.so.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.service.AppInitializer;
import com.bits.bee.so.service.ProfileService;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    public static final int REQCODE_LOGIN = 11524;

    private Button mBtnLogin;
    private TextView mTvKlikDiSini;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        AppInitializer.getInstance().initApp(this);

        if(null != ProfileService.getInstance().getActiveUsr()){
//            startFirstActivities();
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        //cek apa profile ada

        initViews();
        initListeners();
    }


    private void initViews() {
        mBtnLogin = (Button) findViewById(R.id.login_btnLogin);
        mTvKlikDiSini = (TextView) findViewById(R.id.login_tvKlikDiSini);
    }

    private void initListeners() {
        mBtnLogin.setOnClickListener(this);
        mTvKlikDiSini.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mBtnLogin) {
            Intent intent = new Intent(this, QrCodeRegActivity.class);
            startActivityForResult(intent, REQCODE_LOGIN);
        } else if (v == mTvKlikDiSini) {
            startActivity(
                    new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.beecloud.id/z/android")));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQCODE_LOGIN){
            if(resultCode == RESULT_OK){
                startActivity(new Intent(this, MainActivity.class));
                startActivity(new Intent(this, ImportActivity.class));
                finish();
            }
        }
    }
}