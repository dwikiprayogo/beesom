package com.bits.bee.so.bl.net.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 27/07/16.
 */
public class SaleReturn {
    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }


    public class Data {

        @SerializedName("batch")
        @Expose
        private String batch;

        @SerializedName("batchcontent")
        @Expose
        private List<BatchContent> batchContent = new ArrayList<BatchContent>();


        public List<BatchContent> getBatchContent() {
            return batchContent;
        }

        public void setBatchContent(List<BatchContent> batchContent) {
            this.batchContent = batchContent;
        }

        public String getBatch() {
            return batch;
        }

        public void setBatch(String batch) {
            this.batch = batch;
        }

        public class BatchContent {
            @SerializedName("client_id")
            @Expose
            private String clientId;

            public String getSaleId() {
                return saleId;
            }

            public void setSaleId(String saleId) {
                this.saleId = saleId;
            }

            public String getClientId() {
                return clientId;
            }

            public void setClientId(String clientId) {
                this.clientId = clientId;
            }

            @SerializedName("sale_id")
            @Expose
            private String saleId;
        }
    }
}
