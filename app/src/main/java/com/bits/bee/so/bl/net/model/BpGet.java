package com.bits.bee.so.bl.net.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 25/05/16.
 */
public class BpGet {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("version")
        @Expose
        private String version;
        @SerializedName("saletax_code")
        @Expose
        private String saletaxCode;
        @SerializedName("purctax_code")
        @Expose
        private String purctaxCode;
        @SerializedName("srep_id")
        @Expose
        private String srepId;
        @SerializedName("anniversary")
        @Expose
        private String anniversary;
        @SerializedName("taxedonpurc")
        @Expose
        private Boolean taxedonpurc;
        @SerializedName("taxinconpurc")
        @Expose
        private Boolean taxinconpurc;
        @SerializedName("taxedonsale")
        @Expose
        private Boolean taxedonsale;
        @SerializedName("taxinconsale")
        @Expose
        private Boolean taxinconsale;
        @SerializedName("taxregno")
        @Expose
        private String taxregno;
        @SerializedName("vatregno")
        @Expose
        private String vatregno;
        @SerializedName("vatregdate")
        @Expose
        private String vatregdate;
        @SerializedName("bankname")
        @Expose
        private String bankname;
        @SerializedName("bankaccno")
        @Expose
        private String bankaccno;
        @SerializedName("bankaccname")
        @Expose
        private String bankaccname;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("website")
        @Expose
        private String website;
        @SerializedName("note")
        @Expose
        private String note;
        @SerializedName("bpgrp1_id")
        @Expose
        private String bpgrp1Id;
        @SerializedName("bpgrp2_id")
        @Expose
        private String bpgrp2Id;
        @SerializedName("bpgrp3_id")
        @Expose
        private String bpgrp3Id;
        @SerializedName("pricelvl_id")
        @Expose
        private String pricelvlId;
        @SerializedName("active")
        @Expose
        private Boolean active;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("created_by")
        @Expose
        private String createdBy;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("updated_by")
        @Expose
        private String updatedBy;
        @SerializedName("collector_id")
        @Expose
        private String collectorId;
        @SerializedName("isimport")
        @Expose
        private Boolean isimport;
        @SerializedName("coordinate")
        @Expose
        public String coordinate;
        @SerializedName("bpaccount")
        @Expose
        private Bpaccount bpaccount;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getSaletaxCode() {
            return saletaxCode;
        }

        public void setSaletaxCode(String saletaxCode) {
            this.saletaxCode = saletaxCode;
        }

        public String getPurctaxCode() {
            return purctaxCode;
        }

        public void setPurctaxCode(String purctaxCode) {
            this.purctaxCode = purctaxCode;
        }

        public String getSrepId() {
            return srepId;
        }

        public void setSrepId(String srepId) {
            this.srepId = srepId;
        }

        public String getAnniversary() {
            return anniversary;
        }

        public void setAnniversary(String anniversary) {
            this.anniversary = anniversary;
        }

        public Boolean getTaxedonpurc() {
            return taxedonpurc;
        }

        public void setTaxedonpurc(Boolean taxedonpurc) {
            this.taxedonpurc = taxedonpurc;
        }

        public Boolean getTaxinconpurc() {
            return taxinconpurc;
        }

        public void setTaxinconpurc(Boolean taxinconpurc) {
            this.taxinconpurc = taxinconpurc;
        }

        public Boolean getTaxedonsale() {
            return taxedonsale;
        }

        public void setTaxedonsale(Boolean taxedonsale) {
            this.taxedonsale = taxedonsale;
        }

        public Boolean getTaxinconsale() {
            return taxinconsale;
        }

        public void setTaxinconsale(Boolean taxinconsale) {
            this.taxinconsale = taxinconsale;
        }

        public String getTaxregno() {
            return taxregno;
        }

        public void setTaxregno(String taxregno) {
            this.taxregno = taxregno;
        }

        public String getVatregno() {
            return vatregno;
        }

        public void setVatregno(String vatregno) {
            this.vatregno = vatregno;
        }

        public String getVatregdate() {
            return vatregdate;
        }

        public void setVatregdate(String vatregdate) {
            this.vatregdate = vatregdate;
        }

        public String getBankname() {
            return bankname;
        }

        public void setBankname(String bankname) {
            this.bankname = bankname;
        }

        public String getBankaccno() {
            return bankaccno;
        }

        public void setBankaccno(String bankaccno) {
            this.bankaccno = bankaccno;
        }

        public String getBankaccname() {
            return bankaccname;
        }

        public void setBankaccname(String bankaccname) {
            this.bankaccname = bankaccname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getBpgrp1Id() {
            return bpgrp1Id;
        }

        public void setBpgrp1Id(String bpgrp1Id) {
            this.bpgrp1Id = bpgrp1Id;
        }

        public String getBpgrp2Id() {
            return bpgrp2Id;
        }

        public void setBpgrp2Id(String bpgrp2Id) {
            this.bpgrp2Id = bpgrp2Id;
        }

        public String getBpgrp3Id() {
            return bpgrp3Id;
        }

        public void setBpgrp3Id(String bpgrp3Id) {
            this.bpgrp3Id = bpgrp3Id;
        }

        public String getPricelvlId() {
            return pricelvlId;
        }

        public void setPricelvlId(String pricelvlId) {
            this.pricelvlId = pricelvlId;
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(String updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getCollectorId() {
            return collectorId;
        }

        public void setCollectorId(String collectorId) {
            this.collectorId = collectorId;
        }

        public Boolean getIsimport() {
            return isimport;
        }

        public void setIsimport(Boolean isimport) {
            this.isimport = isimport;
        }

        public String getCoordinate() {
            return coordinate;
        }

        public void setCoordinate(String coordinate) {
            this.coordinate = coordinate;
        }

        public Bpaccount getBpaccount() {
            return bpaccount;
        }

        public void setBpaccount(Bpaccount bpaccount) {
            this.bpaccount = bpaccount;
        }
    }

    public class Bpaccount {

        @SerializedName("bp_id")
        @Expose
        private String bpId;
        @SerializedName("crc_id")
        @Expose
        private String crcId;
        @SerializedName("purctermtype")
        @Expose
        private String purctermtype;
        @SerializedName("saletermtype")
        @Expose
        private String saletermtype;
        @SerializedName("accap")
        @Expose
        private String accap;
        @SerializedName("accar")
        @Expose
        private String accar;
        @SerializedName("apduedays")
        @Expose
        private String apduedays;
        @SerializedName("arduedays")
        @Expose
        private String arduedays;
        @SerializedName("apdiscdays")
        @Expose
        private Object apdiscdays;
        @SerializedName("ardiscdays")
        @Expose
        private Object ardiscdays;
        @SerializedName("apearlydisc")
        @Expose
        private String apearlydisc;
        @SerializedName("arearlydisc")
        @Expose
        private String arearlydisc;
        @SerializedName("aplatecharge")
        @Expose
        private Object aplatecharge;
        @SerializedName("arlatecharge")
        @Expose
        private Object arlatecharge;
        @SerializedName("aplimitamt")
        @Expose
        private String aplimitamt;
        @SerializedName("arlimitamt")
        @Expose
        private String arlimitamt;
        @SerializedName("aplimitfreq")
        @Expose
        private String aplimitfreq;
        @SerializedName("arlimitfreq")
        @Expose
        private String arlimitfreq;
        @SerializedName("apbalance")
        @Expose
        private String apbalance;
        @SerializedName("arbalance")
        @Expose
        private String arbalance;
        @SerializedName("apbasebalance")
        @Expose
        private String apbasebalance;
        @SerializedName("arbasebalance")
        @Expose
        private String arbasebalance;
        @SerializedName("custdeposit")
        @Expose
        private String custdeposit;
        @SerializedName("vendordeposit")
        @Expose
        private String vendordeposit;
        @SerializedName("isdefault")
        @Expose
        private Boolean isdefault;

        /**
         * @return The bpId
         */
        public String getBpId() {
            return bpId;
        }

        /**
         * @param bpId The bp_id
         */
        public void setBpId(String bpId) {
            this.bpId = bpId;
        }

        /**
         * @return The crcId
         */
        public String getCrcId() {
            return crcId;
        }

        /**
         * @param crcId The crc_id
         */
        public void setCrcId(String crcId) {
            this.crcId = crcId;
        }

        /**
         * @return The purctermtype
         */
        public String getPurctermtype() {
            return purctermtype;
        }

        /**
         * @param purctermtype The purctermtype
         */
        public void setPurctermtype(String purctermtype) {
            this.purctermtype = purctermtype;
        }

        /**
         * @return The saletermtype
         */
        public String getSaletermtype() {
            return saletermtype;
        }

        /**
         * @param saletermtype The saletermtype
         */
        public void setSaletermtype(String saletermtype) {
            this.saletermtype = saletermtype;
        }

        /**
         * @return The accap
         */
        public String getAccap() {
            return accap;
        }

        /**
         * @param accap The accap
         */
        public void setAccap(String accap) {
            this.accap = accap;
        }

        /**
         * @return The accar
         */
        public String getAccar() {
            return accar;
        }

        /**
         * @param accar The accar
         */
        public void setAccar(String accar) {
            this.accar = accar;
        }

        /**
         * @return The apduedays
         */
        public String getApduedays() {
            return apduedays;
        }

        /**
         * @param apduedays The apduedays
         */
        public void setApduedays(String apduedays) {
            this.apduedays = apduedays;
        }

        /**
         * @return The arduedays
         */
        public String getArduedays() {
            return arduedays;
        }

        /**
         * @param arduedays The arduedays
         */
        public void setArduedays(String arduedays) {
            this.arduedays = arduedays;
        }

        /**
         * @return The apdiscdays
         */
        public Object getApdiscdays() {
            return apdiscdays;
        }

        /**
         * @param apdiscdays The apdiscdays
         */
        public void setApdiscdays(Object apdiscdays) {
            this.apdiscdays = apdiscdays;
        }

        /**
         * @return The ardiscdays
         */
        public Object getArdiscdays() {
            return ardiscdays;
        }

        /**
         * @param ardiscdays The ardiscdays
         */
        public void setArdiscdays(Object ardiscdays) {
            this.ardiscdays = ardiscdays;
        }

        /**
         * @return The apearlydisc
         */
        public String getApearlydisc() {
            return apearlydisc;
        }

        /**
         * @param apearlydisc The apearlydisc
         */
        public void setApearlydisc(String apearlydisc) {
            this.apearlydisc = apearlydisc;
        }

        /**
         * @return The arearlydisc
         */
        public String getArearlydisc() {
            return arearlydisc;
        }

        /**
         * @param arearlydisc The arearlydisc
         */
        public void setArearlydisc(String arearlydisc) {
            this.arearlydisc = arearlydisc;
        }

        /**
         * @return The aplatecharge
         */
        public Object getAplatecharge() {
            return aplatecharge;
        }

        /**
         * @param aplatecharge The aplatecharge
         */
        public void setAplatecharge(Object aplatecharge) {
            this.aplatecharge = aplatecharge;
        }

        /**
         * @return The arlatecharge
         */
        public Object getArlatecharge() {
            return arlatecharge;
        }

        /**
         * @param arlatecharge The arlatecharge
         */
        public void setArlatecharge(Object arlatecharge) {
            this.arlatecharge = arlatecharge;
        }

        /**
         * @return The aplimitamt
         */
        public String getAplimitamt() {
            return aplimitamt;
        }

        /**
         * @param aplimitamt The aplimitamt
         */
        public void setAplimitamt(String aplimitamt) {
            this.aplimitamt = aplimitamt;
        }

        /**
         * @return The arlimitamt
         */
        public String getArlimitamt() {
            return arlimitamt;
        }

        /**
         * @param arlimitamt The arlimitamt
         */
        public void setArlimitamt(String arlimitamt) {
            this.arlimitamt = arlimitamt;
        }

        /**
         * @return The aplimitfreq
         */
        public String getAplimitfreq() {
            return aplimitfreq;
        }

        /**
         * @param aplimitfreq The aplimitfreq
         */
        public void setAplimitfreq(String aplimitfreq) {
            this.aplimitfreq = aplimitfreq;
        }

        /**
         * @return The arlimitfreq
         */
        public String getArlimitfreq() {
            return arlimitfreq;
        }

        /**
         * @param arlimitfreq The arlimitfreq
         */
        public void setArlimitfreq(String arlimitfreq) {
            this.arlimitfreq = arlimitfreq;
        }

        /**
         * @return The apbalance
         */
        public String getApbalance() {
            return apbalance;
        }

        /**
         * @param apbalance The apbalance
         */
        public void setApbalance(String apbalance) {
            this.apbalance = apbalance;
        }

        /**
         * @return The arbalance
         */
        public String getArbalance() {
            return arbalance;
        }

        /**
         * @param arbalance The arbalance
         */
        public void setArbalance(String arbalance) {
            this.arbalance = arbalance;
        }

        /**
         * @return The apbasebalance
         */
        public String getApbasebalance() {
            return apbasebalance;
        }

        /**
         * @param apbasebalance The apbasebalance
         */
        public void setApbasebalance(String apbasebalance) {
            this.apbasebalance = apbasebalance;
        }

        /**
         * @return The arbasebalance
         */
        public String getArbasebalance() {
            return arbasebalance;
        }

        /**
         * @param arbasebalance The arbasebalance
         */
        public void setArbasebalance(String arbasebalance) {
            this.arbasebalance = arbasebalance;
        }

        /**
         * @return The custdeposit
         */
        public String getCustdeposit() {
            return custdeposit;
        }

        /**
         * @param custdeposit The custdeposit
         */
        public void setCustdeposit(String custdeposit) {
            this.custdeposit = custdeposit;
        }

        /**
         * @return The vendordeposit
         */
        public String getVendordeposit() {
            return vendordeposit;
        }

        /**
         * @param vendordeposit The vendordeposit
         */
        public void setVendordeposit(String vendordeposit) {
            this.vendordeposit = vendordeposit;
        }

        /**
         * @return The isdefault
         */
        public Boolean getIsdefault() {
            return isdefault;
        }

        /**
         * @param isdefault The isdefault
         */
        public void setIsdefault(Boolean isdefault) {
            this.isdefault = isdefault;
        }

    }
}
