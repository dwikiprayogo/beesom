
package com.bits.bee.so.view.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bits.bee.so.R;
import com.bits.bee.so.bl.db.dao.BpDao;
import com.bits.bee.so.bl.db.model.Bp;
import com.bits.bee.so.util.BUtil;
import com.bits.bee.so.view.abstraction.WidgetAbstract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 15/07/16.
 */
public class WidgetCustomerBaru extends WidgetAbstract {

    private View mWidgetCustomerBaru;
    private RecyclerView mRvNewCust;

    private RvNewCustAdapter mRvNewCustAdapter;

    public WidgetCustomerBaru(Context context) {
        super(context);
        initViews();
    }

    public WidgetCustomerBaru(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    private void initViews(){
        mWidgetCustomerBaru = LayoutInflater.from(getContext())
                .inflate(R.layout.widget_customerbaru, this, false);
        setContent(mWidgetCustomerBaru);

        mRvNewCust = (RecyclerView) mWidgetCustomerBaru.findViewById(R.id.customerbaru_rvNewCust);
        mRvNewCust.setHasFixedSize(true);
        mRvNewCust.setLayoutManager(new LinearLayoutManager(getContext()));

        mRvNewCustAdapter = new RvNewCustAdapter();
        mRvNewCust.setAdapter(mRvNewCustAdapter);
    }

    @Override
    public void render() {
        loadData();
    }

    @Override
    protected void loadData() {
        List<Bp> bpList = new ArrayList<>();
        try {
            bpList = BpDao.getInstance().readBpByDate(
                    BUtil.getStartOfDay().getTimeInMillis(), BUtil.getEndOfDay().getTimeInMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }


//        List<Bp> bpList = new ArrayList<>();
//        for(int i = 0 ; i<15 ; i++){
//            Bp bp = new Bp();
//            bp.setNamaPerusahaan("Jack Dummy "+i);
//            bpList.add(bp);
//        }

        mRvNewCustAdapter.generate(bpList);
    }

    private class RvNewCustAdapter
            extends RecyclerView.Adapter<RvNewCustAdapter.ViewHolder>{

        private List<Bp> mBpList = new ArrayList<>();

        public void generate(List<Bp> bpList){
            this.mBpList = bpList;
            notifyDataSetChanged();
        }

        class ViewHolder extends RecyclerView.ViewHolder{

            TextView tvIdx;
            TextView tvCustName;

            public ViewHolder(View itemView) {
                super(itemView);

                tvIdx = (TextView) itemView.findViewById(R.id.customerbaru_rvNewCust_tvIdx);
                tvCustName = (TextView) itemView.findViewById(R.id.customerbaru_rvNewCust_tvCustName);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_customerbaru_rvnewcust, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Bp bp = mBpList.get(position);

            holder.tvIdx.setText(""+(position + 1));
            holder.tvCustName.setText(bp.getNamaPerusahaan());
        }

        @Override
        public int getItemCount() {
            return mBpList.size();
        }
    }
}
