package com.bits.bee.so.bl.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by basofi on 27/06/16.
 */
public class RegQrCode {
    @SerializedName("username")
    @Expose
    private String username;
//    @SerializedName("cmpname")
//    @Expose
//    private String cmpname;
    @SerializedName("apikey")
    @Expose
    private String apikey;

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

//    /**
//     * @return The cmpname
//     */
//    public String getCmpname() {
//        return cmpname;
//    }
//
//    /**
//     * @param cmpname The cmpname
//     */
//    public void setCmpname(String cmpname) {
//        this.cmpname = cmpname;
//    }

    /**
     * @return The apikey
     */
    public String getApikey() {
        return apikey;
    }

    /**
     * @param apikey The apikey
     */
    public void setApikey(String apikey) {
        this.apikey = apikey;
    }
}
