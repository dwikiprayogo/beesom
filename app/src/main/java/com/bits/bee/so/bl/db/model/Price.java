package com.bits.bee.so.bl.db.model;

import com.bits.bee.so.bl.db.dao.ItemDao;
import com.bits.bee.so.bl.net.model.PriceGet;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by basofi on 25/05/16.
 */
@DatabaseTable(tableName = Price.TBL_NAME)
public class Price implements Serializable{
    public static final String TBL_NAME = "price";

    public static final String ID = "id";
    public static final String ITEM_ID = "item_id";
    public static final String PRICE1 = "price1";
    public static final String DISC1_EXP = "disc1_exp";
    public static final String PRICE2 = "price2";
    public static final String DISC2_EXP = "disc2_exp";
    public static final String PRICE3 = "price3";
    public static final String DISC3_EXP = "disc3_exp";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = ITEM_ID, foreign = true, foreignAutoRefresh = true)
    private Item itemId;
    @DatabaseField(columnName = PRICE1)
    private BigDecimal price1  = new BigDecimal(0);
    @DatabaseField(columnName = DISC1_EXP)
    private String disc1_exp;
    @DatabaseField(columnName = PRICE2)
    private BigDecimal price2  = new BigDecimal(0);
    @DatabaseField(columnName = DISC2_EXP)
    private String disc2_exp;
    @DatabaseField(columnName = PRICE3)
    private BigDecimal price3  = new BigDecimal(0);
    @DatabaseField(columnName = DISC3_EXP)
    private String disc3_exp;

    public Price(){

    }

    public Price(PriceGet.Datum priceGetDatum) throws Exception{
        Item item = null;
        try {
            item = ItemDao.getInstance().readByOlId(priceGetDatum.getItemId());
            setItemId(item);
        } catch (Exception e) {
            throw new Exception("No item found, price not generated");
        }
        setPrice1(new BigDecimal(priceGetDatum.getPrice1()));
        setPrice2(new BigDecimal(priceGetDatum.getPrice2()));
        setPrice3(new BigDecimal(priceGetDatum.getPrice3()));
        setDisc1_exp(priceGetDatum.getDisc1exp());
        setDisc2_exp(priceGetDatum.getDisc2exp());
        setDisc3_exp(priceGetDatum.getDisc3exp());
//        setItemId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Item getItemId() {
        return itemId;
    }

    public void setItemId(Item itemId) {
        this.itemId = itemId;
    }

    public BigDecimal getPrice1() {
        return price1;
    }

    public void setPrice1(BigDecimal price1) {
        this.price1 = price1;
    }

    public String getDisc1_exp() {
        return disc1_exp;
    }

    public void setDisc1_exp(String disc1_exp) {
        this.disc1_exp = disc1_exp;
    }

    public BigDecimal getPrice2() {
        return price2;
    }

    public void setPrice2(BigDecimal price2) {
        this.price2 = price2;
    }

    public String getDisc2_exp() {
        return disc2_exp;
    }

    public void setDisc2_exp(String disc2_exp) {
        this.disc2_exp = disc2_exp;
    }

    public BigDecimal getPrice3() {
        return price3;
    }

    public void setPrice3(BigDecimal price3) {
        this.price3 = price3;
    }

    public String getDisc3_exp() {
        return disc3_exp;
    }

    public void setDisc3_exp(String disc3_exp) {
        this.disc3_exp = disc3_exp;
    }
}
