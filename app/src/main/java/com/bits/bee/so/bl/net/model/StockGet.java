package com.bits.bee.so.bl.net.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 01/06/16.
 */
public class StockGet {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The data
     */
    public List<Datum> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("item_id")
        @Expose
        private String itemId;
        @SerializedName("wh_id")
        @Expose
        private String whId;
        @SerializedName("pid")
        @Expose
        private String pid;
        @SerializedName("qty")
        @Expose
        private String qty;
        @SerializedName("qtyx")
        @Expose
        private String qtyx;
        @SerializedName("qtypo")
        @Expose
        private String qtypo;
        @SerializedName("qtyxpo")
        @Expose
        private Object qtyxpo;
        @SerializedName("qtyso")
        @Expose
        private String qtyso;
        @SerializedName("qtyxso")
        @Expose
        private String qtyxso;
        @SerializedName("qtydefect")
        @Expose
        private String qtydefect;
        @SerializedName("qtyxdefect")
        @Expose
        private String qtyxdefect;
        @SerializedName("expdate")
        @Expose
        private Object expdate;
        @SerializedName("location")
        @Expose
        private Object location;
        @SerializedName("qtymin")
        @Expose
        private String qtymin;
        @SerializedName("qtymax")
        @Expose
        private String qtymax;
        @SerializedName("qtyreorder")
        @Expose
        private String qtyreorder;
        @SerializedName("avgcost")
        @Expose
        private String avgcost;
        @SerializedName("totcost")
        @Expose
        private String totcost;
        @SerializedName("active")
        @Expose
        private Boolean active;
        @SerializedName("opname_emp_id")
        @Expose
        private Object opnameEmpId;
        @SerializedName("opname_date")
        @Expose
        private Object opnameDate;
        @SerializedName("opname_usr_id")
        @Expose
        private Object opnameUsrId;
        @SerializedName("_xt")
        @Expose
        private Boolean xt;
        @SerializedName("subtotal")
        @Expose
        private String subtotal;

        /**
         * @return The itemId
         */
        public String getItemId() {
            return itemId;
        }

        /**
         * @param itemId The item_id
         */
        public void setItemId(String itemId) {
            this.itemId = itemId;
        }

        /**
         * @return The whId
         */
        public String getWhId() {
            return whId;
        }

        /**
         * @param whId The wh_id
         */
        public void setWhId(String whId) {
            this.whId = whId;
        }

        /**
         * @return The pid
         */
        public String getPid() {
            return pid;
        }

        /**
         * @param pid The pid
         */
        public void setPid(String pid) {
            this.pid = pid;
        }

        /**
         * @return The qty
         */
        public String getQty() {
            return qty;
        }

        /**
         * @param qty The qty
         */
        public void setQty(String qty) {
            this.qty = qty;
        }

        /**
         * @return The qtyx
         */
        public String getQtyx() {
            return qtyx;
        }

        /**
         * @param qtyx The qtyx
         */
        public void setQtyx(String qtyx) {
            this.qtyx = qtyx;
        }

        /**
         * @return The qtypo
         */
        public String getQtypo() {
            return qtypo;
        }

        /**
         * @param qtypo The qtypo
         */
        public void setQtypo(String qtypo) {
            this.qtypo = qtypo;
        }

        /**
         * @return The qtyxpo
         */
        public Object getQtyxpo() {
            return qtyxpo;
        }

        /**
         * @param qtyxpo The qtyxpo
         */
        public void setQtyxpo(Object qtyxpo) {
            this.qtyxpo = qtyxpo;
        }

        /**
         * @return The qtyso
         */
        public String getQtyso() {
            return qtyso;
        }

        /**
         * @param qtyso The qtyso
         */
        public void setQtyso(String qtyso) {
            this.qtyso = qtyso;
        }

        /**
         * @return The qtyxso
         */
        public String getQtyxso() {
            return qtyxso;
        }

        /**
         * @param qtyxso The qtyxso
         */
        public void setQtyxso(String qtyxso) {
            this.qtyxso = qtyxso;
        }

        /**
         * @return The qtydefect
         */
        public String getQtydefect() {
            return qtydefect;
        }

        /**
         * @param qtydefect The qtydefect
         */
        public void setQtydefect(String qtydefect) {
            this.qtydefect = qtydefect;
        }

        /**
         * @return The qtyxdefect
         */
        public String getQtyxdefect() {
            return qtyxdefect;
        }

        /**
         * @param qtyxdefect The qtyxdefect
         */
        public void setQtyxdefect(String qtyxdefect) {
            this.qtyxdefect = qtyxdefect;
        }

        /**
         * @return The expdate
         */
        public Object getExpdate() {
            return expdate;
        }

        /**
         * @param expdate The expdate
         */
        public void setExpdate(Object expdate) {
            this.expdate = expdate;
        }

        /**
         * @return The location
         */
        public Object getLocation() {
            return location;
        }

        /**
         * @param location The location
         */
        public void setLocation(Object location) {
            this.location = location;
        }

        /**
         * @return The qtymin
         */
        public String getQtymin() {
            return qtymin;
        }

        /**
         * @param qtymin The qtymin
         */
        public void setQtymin(String qtymin) {
            this.qtymin = qtymin;
        }

        /**
         * @return The qtymax
         */
        public String getQtymax() {
            return qtymax;
        }

        /**
         * @param qtymax The qtymax
         */
        public void setQtymax(String qtymax) {
            this.qtymax = qtymax;
        }

        /**
         * @return The qtyreorder
         */
        public String getQtyreorder() {
            return qtyreorder;
        }

        /**
         * @param qtyreorder The qtyreorder
         */
        public void setQtyreorder(String qtyreorder) {
            this.qtyreorder = qtyreorder;
        }

        /**
         * @return The avgcost
         */
        public String getAvgcost() {
            return avgcost;
        }

        /**
         * @param avgcost The avgcost
         */
        public void setAvgcost(String avgcost) {
            this.avgcost = avgcost;
        }

        /**
         * @return The totcost
         */
        public String getTotcost() {
            return totcost;
        }

        /**
         * @param totcost The totcost
         */
        public void setTotcost(String totcost) {
            this.totcost = totcost;
        }

        /**
         * @return The active
         */
        public Boolean getActive() {
            return active;
        }

        /**
         * @param active The active
         */
        public void setActive(Boolean active) {
            this.active = active;
        }

        /**
         * @return The opnameEmpId
         */
        public Object getOpnameEmpId() {
            return opnameEmpId;
        }

        /**
         * @param opnameEmpId The opname_emp_id
         */
        public void setOpnameEmpId(Object opnameEmpId) {
            this.opnameEmpId = opnameEmpId;
        }

        /**
         * @return The opnameDate
         */
        public Object getOpnameDate() {
            return opnameDate;
        }

        /**
         * @param opnameDate The opname_date
         */
        public void setOpnameDate(Object opnameDate) {
            this.opnameDate = opnameDate;
        }

        /**
         * @return The opnameUsrId
         */
        public Object getOpnameUsrId() {
            return opnameUsrId;
        }

        /**
         * @param opnameUsrId The opname_usr_id
         */
        public void setOpnameUsrId(Object opnameUsrId) {
            this.opnameUsrId = opnameUsrId;
        }

        /**
         * @return The xt
         */
        public Boolean getXt() {
            return xt;
        }

        /**
         * @param xt The _xt
         */
        public void setXt(Boolean xt) {
            this.xt = xt;
        }

        /**
         * @return The subtotal
         */
        public String getSubtotal() {
            return subtotal;
        }

        /**
         * @param subtotal The subtotal
         */
        public void setSubtotal(String subtotal) {
            this.subtotal = subtotal;
        }

    }
}
