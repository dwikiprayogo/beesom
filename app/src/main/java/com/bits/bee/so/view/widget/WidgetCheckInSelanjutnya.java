package com.bits.bee.so.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.bits.bee.so.R;
import com.bits.bee.so.view.abstraction.WidgetAbstract;

/**
 * Created by basofi on 15/07/16.
 */
public class WidgetCheckInSelanjutnya extends WidgetAbstract {

    private View mWidgetCheckInSelanjutnya;


    public WidgetCheckInSelanjutnya(Context context) {
        super(context);
        initViews();
    }

    public WidgetCheckInSelanjutnya(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    private void initViews(){
        mWidgetCheckInSelanjutnya = LayoutInflater.from(getContext())
                .inflate(R.layout.widget_checkinselanjutnya, this, false);

        setContent(mWidgetCheckInSelanjutnya);
    }

    @Override
    public void render() {

    }

    @Override
    protected void loadData() {

    }
}
