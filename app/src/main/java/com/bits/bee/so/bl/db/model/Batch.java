package com.bits.bee.so.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by basofi on 27/07/16.
 */
@DatabaseTable(tableName = Batch.TBL_NAME)
public class Batch implements Serializable {

    public static final String TBL_NAME = "batch";

    public static final String ID = "id";
    public static final String BATCH_ID = "batch_id";
    public static final String SALE_ID = "sale_id";
    public static final String TRXDATELONG = "trxdatelong";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = BATCH_ID)
    private int batchId;
    @DatabaseField(columnName = SALE_ID, foreign = true, foreignAutoRefresh = true)
    private Sale saleId;
    @DatabaseField(columnName = TRXDATELONG)
    private long trxDateLong;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBatchId() {
        return batchId;
    }

    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }

    public Sale getSaleId() {
        return saleId;
    }

    public void setSaleId(Sale saleId) {
        this.saleId = saleId;
    }

    public long getTrxDateLong() {
        return trxDateLong;
    }

    public void setTrxDateLong(long trxDateLong) {
        this.trxDateLong = trxDateLong;
    }
}
