package com.bits.bee.so.bl.trans;

import com.bits.bee.so.bl.db.model.So;

import java.io.Serializable;

/**
 * Created by basofi on 02/08/16.
 */
public class SoTrans implements Serializable {

    private So mSo;

    public So getSo() {
        return mSo;
    }

    public void setSo(So mSo) {
        this.mSo = mSo;
    }
}
