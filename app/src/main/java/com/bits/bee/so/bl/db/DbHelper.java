package com.bits.bee.so.bl.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.bits.bee.so.bl.db.model.Batch;
import com.bits.bee.so.bl.db.model.Bp;
import com.bits.bee.so.bl.db.model.City;
import com.bits.bee.so.bl.db.model.Item;
import com.bits.bee.so.bl.db.model.Price;
import com.bits.bee.so.bl.db.model.Reg;
import com.bits.bee.so.bl.db.model.Sale;
import com.bits.bee.so.bl.db.model.Saled;
import com.bits.bee.so.bl.db.model.So;
import com.bits.bee.so.bl.db.model.Sod;
import com.bits.bee.so.bl.db.model.Stock;
import com.bits.bee.so.bl.db.model.Unit;
import com.bits.bee.so.bl.db.model.Usr;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by basofi on 24/05/16.
 */
public class DbHelper extends OrmLiteSqliteOpenHelper{

    private static final int DBVER = 1;
    public static final String DBNAME = "beesomobile.db";
    private Context mContext;

    public DbHelper(Context context) {
        super(context, DBNAME, null, DBVER);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Bp.class);
            TableUtils.createTable(connectionSource, Item.class);
            TableUtils.createTable(connectionSource, Price.class);
            TableUtils.createTable(connectionSource, Stock.class);
            TableUtils.createTable(connectionSource, Unit.class);
            TableUtils.createTable(connectionSource, So.class);
            TableUtils.createTable(connectionSource, Sod.class);
            TableUtils.createTable(connectionSource, Sale.class);
            TableUtils.createTable(connectionSource, Saled.class);
            TableUtils.createTable(connectionSource, Usr.class);
            TableUtils.createTable(connectionSource, City.class);
            TableUtils.createTable(connectionSource, Reg.class);
            TableUtils.createTable(connectionSource, Batch.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database,
                          ConnectionSource connectionSource,
                          int oldVersion,
                          int newVersion) {
    }

    public ConnectionSource getConnectionSource(){
        return connectionSource;
    }
}