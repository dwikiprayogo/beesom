package com.bits.bee.so.service;

import android.content.Context;

import com.bits.bee.so.bl.db.dao.UsrDao;
import com.bits.bee.so.bl.db.model.Usr;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 28/06/16.
 */
public class ProfileService {

    private static ProfileService singleton;

    private Usr mUsr;

    private List<Usr> mUsrList = new ArrayList<>();

    private Context mContext;

    public static ProfileService getInstance(){
        if(singleton == null){
            singleton = new ProfileService();
        }
        return singleton;
    }

    public void init(Context context){
        this.mContext = context;

        load();
    }

    public void load(){
        Usr usr = null;
        try {
            mUsrList = UsrDao.getInstance().read();
            if(mUsrList.size() == 0){
                mUsr = null;
            }
            for(int i = 0 ; i< mUsrList.size() ; i++){
                usr = mUsrList.get(i);
                if(usr.isactive()){
                    setActiveUsr(usr);
                    break;
                }else{
                    mUsr = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Usr getActiveUsr() {
        return mUsr;
    }

    public void setActiveUsr(Usr usr) {

        try {
            UsrDao.getInstance().setAllIsActive(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        usr.setIsactive(true);

        try {
            UsrDao.getInstance().createOrUpdate(usr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mUsr = usr;

        try {
            mUsrList = UsrDao.getInstance().read();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Usr> getUsrList(){
        return mUsrList;
    }
}