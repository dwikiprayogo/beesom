package com.bits.bee.so.bl.trans;

import com.bits.bee.so.bl.db.dao.RegDao;
import com.bits.bee.so.bl.db.dao.SaleDao;
import com.bits.bee.so.bl.db.dao.SaledDao;
import com.bits.bee.so.bl.db.dao.SoDao;
import com.bits.bee.so.bl.db.dao.SodDao;
import com.bits.bee.so.bl.db.model.Bp;
import com.bits.bee.so.bl.db.model.Reg;
import com.bits.bee.so.bl.db.model.Sale;
import com.bits.bee.so.bl.db.model.Saled;
import com.bits.bee.so.bl.db.model.So;
import com.bits.bee.so.bl.db.model.Sod;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by basofi on 10/06/16.
 */
public class SaleSoTrans implements Serializable {

    public static final int MODE_SO = 0;
    public static final int MODE_SALE = 1;

    private int mode = MODE_SO;

    private Sale mSale;
    private So mSo;
    private List<Sod> mSodList;
    private Bp mBp;
    private Reg mReg;

    public SaleSoTrans(int mode){
        this.mode = mode;

        mSale = new Sale();
        mSo = new So();
        mSodList = new ArrayList<>();
        try {
            mReg = RegDao.getInstance().read().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void load(Sale sale){
        this.mSale = sale;
    }

    public void load(So so){
        this.mSo = so;
    }

    public void setBp(Bp bp){
        this.mBp = bp;
        mSo.setBpId(bp);
        mSale.setBpId(bp);
        mSo.setBpolid(bp.getOlId());
        mSale.setBpolid(bp.getOlId());
    }

    public Bp getBp(){
        return mBp;
    }

    public Sale getSale(){
        return mSale;
    }

    public void setDueDays(int dueDays){
        mSale.setDuedays(dueDays);
    }

    public void setTermType(String termType){
        mSale.setTermtype(termType);
    }

    public String getTermType(){
        return mSale.getTermtype();
    }

    public List<Sod> getSodList(){
        return mSodList;
    }

    public void putSodToSo(Sod sod){
        if(mSodList.contains(sod)){
            for(int i = 0; i<mSodList.size() ; i++){
                if(mSodList.get(i) == sod){
                    mSodList.set(i, sod);
                }
            }
        }else{
            boolean isNew = true;
            for(int i = 0 ; i < mSodList.size() ; i++){
                Sod sodFromList = mSodList.get(i);
                if(sod.getItemolid().equals(sodFromList.getItemolid())){
                    isNew = false;
                    sodFromList.setQty(sodFromList.getQty().add(sod.getQty()));
                }
            }
            if(isNew) {
                mSodList.add(sod);
            }
        }

        calculate();
    }

    private void calculate(){
        BigDecimal total = new BigDecimal(0);
        for(int i = 0 ; i < mSodList.size() ; i++){
            Sod sod = mSodList.get(i);
            total = total.add((sod.getSubtotal().multiply(sod.getQty())));
        }
        mSo.setSubtotal(total);
        mSo.setTotal(total);

        mSale.setSubtotal(mSo.getSubtotal());
        mSale.setTotal(mSo.getTotal());
    }

    public void deleteSod(Sod sod){
        mSodList.remove(sod);

        calculate();
    }

    public void save(String coordinate){
        if(getTermType().equalsIgnoreCase("credit")){
            mSale.setTermtype("R");
        }else{
            mSale.setTermtype("C");
        }

        if(mode == MODE_SO){
            saveSo(coordinate);
            if(mSale.getTermtype().equalsIgnoreCase("c")){
                saveSales(mSo, coordinate);
            }
        }else{
            saveSales(null, coordinate);
        }
    }

    private void saveSo(String coordinate){
        Calendar calendar = Calendar.getInstance();
        mSo.setTrxdateLong(calendar.getTimeInMillis());
        mSo.setTrxCoordinate(coordinate);
        try {
            SoDao.getInstance().create(mSo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for(int i = 0 ; i<mSodList.size() ; i++) {
            Sod sod = mSodList.get(i);
            sod.setSoid(mSo);
            sod.setDno(i + 1);

            try {
                SodDao.getInstance().create(sod);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void saveSales(So so, String coordinate){
        Calendar calendar = Calendar.getInstance();

        //create sale
        mSale.setTrxdateLong(calendar.getTimeInMillis());
        mSale.setTrxCoordinate(coordinate);
        try {
            SaleDao.getInstance().create(mSale);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for(int i = 0 ; i<mSodList.size() ; i++){
            Sod sod = mSodList.get(i);

            Saled saled = new Saled();
            saled.setBasefistotaltaxamt(sod.getBasefistotaltaxamt());
            saled.setBaseprice(sod.getBaseprice());
            saled.setBasesubtotal(sod.getBasesubtotal());
            saled.setBasetotaltaxamt(sod.getBasetotaltaxamt());
            saled.setSaleid(mSale);
            saled.setSo_id(so);
            saled.setSo_dno(sod.getDno());
            saled.setDno(i + 1);
            saled.setSubtotal(sod.getSubtotal());
//            saled.setTax_code();
            saled.setTaxableamt(sod.getTaxableamt());
            saled.setTaxamt(sod.getTaxamt());
            saled.setTotaldiscamt(sod.getTotaldiscamt());
            saled.setTotaltaxamt(sod.getTotaltaxamt());
            saled.setUnit_olid(sod.getUnitolid());
            saled.setListprice(sod.getListprice());
            saled.setQty(sod.getQty());
            saled.setDiscamt(sod.getDiscamt());
            saled.setDiscexp(sod.getDiscexp());
            saled.setItemname(sod.getItemname());
            saled.setItemolid(sod.getItemolid());
            saled.setWhId(mReg.getValue());

            try {
                SaledDao.getInstance().create(saled);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}