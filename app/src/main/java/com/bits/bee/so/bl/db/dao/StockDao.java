package com.bits.bee.so.bl.db.dao;

import com.bits.bee.so.bl.db.Db;
import com.bits.bee.so.bl.db.abstraction.DaoCrud;
import com.bits.bee.so.bl.db.model.Item;
import com.bits.bee.so.bl.db.model.Stock;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by basofi on 27/05/16.
 */
public class StockDao implements DaoCrud<Stock, Integer> {
    private static StockDao singleton;

    public static StockDao getInstance(){
        if(singleton == null){
            singleton = new StockDao();
        }
        return singleton;
    }

    @Override
    public void create(Stock model) throws Exception {
        getDao().create(model);
    }

    @Override
    public void update(Stock model) throws Exception {
        getDao().update(model);
    }

    @Override
    public void delete(Stock model) throws Exception {
        getDao().delete(model);
    }

    @Override
    public void createOrUpdate(Stock model) throws Exception {
        getDao().createOrUpdate(model);
    }

    @Override
    public List<Stock> read() throws Exception {
        return getDao().queryForAll();
    }

    public Stock readByItemWhPid(String itemId, String whId, String pid) throws Exception{
        QueryBuilder<Stock, Integer> builder = getDao().queryBuilder();
        builder.where()
                .eq(Stock.ITEM_ID, itemId)
                .and()
                .eq(Stock.WH_ID, whId)
                .and()
                .eq(Stock.PID, pid);
        PreparedQuery preparedQuery = builder.prepare();

        List<Stock> stockList = new ArrayList<>();
        stockList = getDao().query(preparedQuery);
        if(stockList.size() > 0){
            return stockList.get(0);
        }else {
            throw new Exception("Stock not found");
        }
    }

    public Stock readByItemId(Item itemId) throws Exception{
        List<Stock> stockList = new ArrayList<>();
        stockList = getDao().queryForEq(Stock.ITEM_ID, itemId);
        return stockList.get(0);
    }

    @Override
    public Dao<Stock, Integer> getDao() throws Exception {
        return Db.getInstance().getDbHelper().getDao(Stock.class);
    }

    @Override
    public long count() throws Exception {
        return read().size();
    }

    @Override
    public void deleteAll() throws Exception {
        ConnectionSource connectionSource = Db.getInstance().getDbHelper().getConnectionSource();
        TableUtils.clearTable(connectionSource, Stock.class);
    }
}
