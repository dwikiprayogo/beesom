package com.bits.bee.so.bl.db.model;

import com.bits.bee.so.bl.db.dao.UsrDao;
import com.bits.bee.so.bl.service.model.RegQrCode;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by basofi on 27/06/16.
 */
@DatabaseTable(tableName = Usr.TBL_NAME)
public class Usr implements Serializable {

    public static final String TBL_NAME = "usr";

    public static final String ID = "id";
    public static final String USERNAME = "username";
    public static final String ISACTIVE = "isactive";
    public static final String CMPNAME = "cmpname";
    public static final String APIKEY = "apikey";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = USERNAME)
    private String username;
    @DatabaseField(columnName = ISACTIVE)
    private boolean isactive = true;
    @DatabaseField(columnName = CMPNAME)
    private String cmpname;
    @DatabaseField(columnName = APIKEY)
    private String apikey;

    public Usr(){

    }

    public Usr(RegQrCode regQrCode) throws Exception{

        setUsername(regQrCode.getUsername());
        setApikey(regQrCode.getApikey());

        boolean isUsrExist = UsrDao.getInstance().isNameAndKeyExist(getUsername(), getApikey());
        if(isUsrExist){
            throw new Exception("User already exists");
        }
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getCmpname() {
        return cmpname;
    }

    public void setCmpname(String cmpname) {
        this.cmpname = cmpname;
    }

    public boolean isactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
